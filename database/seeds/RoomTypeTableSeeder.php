<?php

use Illuminate\Database\Seeder;
use App\HotelRoomType;
class RoomTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$datas = [
    		['name' => 'Single', 'description' => 'A room assigned to one person. May have one or more beds.'],
    		['name' => 'Double', 'description' => 'A room assigned to two people. May have one or more beds.'],
    		['name' => 'Triple', 'description' => 'A room assigned to three people. May have two or more beds.'],
    		['name' => 'Quad', 'description' => 'A room assigned to four people. May have two or more beds.'],
    		['name' => 'Queen', 'description' => 'A room with a queen-sized bed. May be occupied by one or more people.'],
    		['name' => 'King', 'description' => 'A room with a king-sized bed. May be occupied by one or more people.'],
    		['name' => 'Twin', 'description' => 'A room with two beds. May be occupied by one or more people.'],
    		['name' => 'Double-double', 'description' => 'A room with two double (or perhaps queen) beds. May be occupied by one or more people.'],
    		['name' => 'Studio', 'description' => 'A room with a studio bed – a couch that can be converted into a bed. May also have an additional bed.']
    	];
    	foreach ($datas as $data) {
    		HotelRoomType::create($data);
    	}
    }
}