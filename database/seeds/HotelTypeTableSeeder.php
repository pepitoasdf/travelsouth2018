<?php

use Illuminate\Database\Seeder;
use App\HotelType;
class HotelTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
        	['name' => 'One Star'],
        	['name' => 'Two Star'],
        	['name' => 'Three Star'],
        	['name' => 'Four Star'],
        	['name' => 'Five Star'],
        	['name' => 'Luxury']
        ];
        foreach ($datas as $data) {
        	HotelType::create($data);
        }
    }
}
