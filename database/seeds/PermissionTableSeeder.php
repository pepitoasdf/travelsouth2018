<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\User;
use App\Role;
use App\TabMenu;
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        		[
        			'name' => 'Users',
        			'filter' => '_users'
        		],
        		[
        			'name' => 'Roles',
        			'filter' => '_roles'
        		],
                [
                    'name' => 'Vehicles',
                    'filter' => '_vehicles'
                ],
                [
                    'name' => 'Vehicle Rates',
                    'filter' => '_vrates'
                ],
                [
                    'name' => 'Hotels',
                    'filter' => '_hotels'
                ],
                [
                    'name' => 'Hotel Facilities',
                    'filter' => '_facilities'
                ],
                [
                    'name' => 'Hotel Types',
                    'filter' => '_hoteltypes'
                ],
                [
                    'name' => 'Hotel Room Rates',
                    'filter' => '_roomrates'
                ],
                [
                    'name' => 'Hotel Rooms',
                    'filter' => '_rooms'
                ],
                [
                    'name' => 'Hotel Room Types',
                    'filter' => '_roomtypes'
                ],
                [
                    'name' => 'Countries',
                    'filter' => '_countries'
                ],
                [
                    'name' => 'Vehicle Features',
                    'filter' => '_vfeatures'
                ],
                [
                    'name' => 'Packages',
                    'filter' => '_packages'
                ],
                [
                    'name' => 'Package Features',
                    'filter' => '_pfeatures'
                ],
                [
                    'name' => 'Packages Rates',
                    'filter' => '_prates'
                ],
                [
                    'name' => 'Hotels Gallery',
                    'filter' => '_hgalleries'
                ],
                [
                    'name' => 'Vehicles Gallery',
                    'filter' => '_vgalleries'
                ],
                [
                    'name' => 'Packages Gallery',
                    'filter' => '_pgalleries'
                ],
                [
                    'name' => 'Page Contact',
                    'filter' => '_contacts'
                ],
                [
                    'name' => 'Page About',
                    'filter' => '_abouts'
                ],
                [
                    'name' => 'Bookings',
                    'filter' => '_bookings'
                ],
                [
                    'name' => 'Page Blog Post',
                    'filter' => '_posts'
                ],
                [
                    'name' => 'Dashboard',
                    'filter' => '_dashboard'
                ],
                [
                    'name' => 'Home Background',
                    'filter' => '_background'
                ],
                [
                    'name' => 'Home Increadible Places',
                    'filter' => '_increadibleplaces'
                ],
                [
                    'name' => 'Home Partners',
                    'filter' => '_partners'
                ],
                [
                    'name' => 'Home Popular',
                    'filter' => '_popular'
                ],
                [
                    'name' => 'Home Top Destinations',
                    'filter' => '_topdestinations'
                ],
                [
                    'name' => 'Home Videos',
                    'filter' => '_videos'
                ],
                [
                    'name' => 'Places Provinces',
                    'filter' => '_provinces'
                ],
                [
                    'name' => 'Places Cities',
                    'filter' => '_cities'
                ],
                [
                    'name' => 'Package Itineraries',
                    'filter' => '_pitineraries'
                ]
                ,
                [
                    'name' => 'Package Guides',
                    'filter' => '_pguides'
                ]
                ,
                [
                    'name' => 'Package Videos',
                    'filter' => '_pvideos'
                ]
                ,
                [
                    'name' => 'Hotel Videos',
                    'filter' => '_hvideos'
                ]
                ,
                [
                    'name' => 'Vehicle Videos',
                    'filter' => '_vvideos'
                ],
                [
                    'name' => 'System Admin',
                    'filter' => '_admin'
                ],
                [
                    'name' => 'System Client',
                    'filter' => '_client'
                ]
        	];
        	TabMenu::truncate();
        	foreach ($data as $key => $value) {
        		TabMenu::create($value);
        	}
        $permissions = Permission::defaultPermissions();
        DB::table('permissions')->delete();
        DB::table('role_has_permissions')->delete();
        foreach ($permissions as $perms) {
            Permission::firstOrCreate(['name' => $perms]);
        }
        $role = Role::first();
        $role->syncPermissions(Permission::all());    	
    }
}
