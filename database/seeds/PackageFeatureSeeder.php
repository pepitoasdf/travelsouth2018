<?php

use Illuminate\Database\Seeder;
use App\PackageFeature;
class PackageFeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
        	['description' => 'Dummy Package feature content 1'],
        	['description' => 'Dummy Package feature content 2'],
        	['description' => 'Dummy Package feature content 3'],
        	['description' => 'Dummy Package feature content 4'],
        	['description' => 'Dummy Package feature content 5'],
        	['description' => 'Dummy Package feature content 6'],
        	['description' => 'Dummy Package feature content 7'],
        	['description' => 'Dummy Package feature content 8'],
        	['description' => 'Dummy Package feature content 9'],
        	['description' => 'Dummy Package feature content 10'],
        	['description' => 'Dummy Package feature content 11'],
        	['description' => 'Dummy Package feature content 12'],
        	['description' => 'Dummy Package feature content 13'],
        	['description' => 'Dummy Package feature content 14'],
        	['description' => 'Dummy Package feature content 15'],
        	['description' => 'Dummy Package feature content 16'],
        	['description' => 'Dummy Package feature content 17'],
        	['description' => 'Dummy Package feature content 18']
        	
        ];
        foreach ($datas as $data) {
        	PackageFeature::create($data);
        }
    }
}
