<?php

use Illuminate\Database\Seeder;
use App\Province;
use App\City;
class ProvinceCityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('provinces')->truncate();
    	DB::table('cities')->truncate();
        $provinces = [
				'ILOCOS NORTE',
				'ILOCOS SUR',
				'LA UNION',
				'PANGASINAN',
				'BATANES',
				'CAGAYAN',
				'ISABELA',
				'NUEVA VIZCAYA',
				'QUIRINO',
				'BATAAN',
				'BULACAN',
				'NUEVA ECIJA',
				'PAMPANGA',
				'TARLAC',
				'ZAMBALES',
				'AURORA',
				'BATANGAS',
				'CAVITE',
				'LAGUNA',
				'QUEZON',
				'RIZAL',
				'MARINDUQUE',
				'OCCIDENTAL MINDORO',
				'ORIENTAL MINDORO',
				'PALAWAN',
				'ROMBLON',
				'ALBAY',
				'CAMARINES NORTE',
				'CAMARINES SUR',
				'CATANDUANES',
				'MASBATE',
				'SORSOGON',
				'AKLAN',
				'ANTIQUE',
				'CAPIZ',
				'ILOILO',
				'NEGROS OCCIDENTAL',
				'GUIMARAS',
				'BOHOL',
				'CEBU',
				'NEGROS ORIENTAL',
				'SIQUIJOR',
				'EASTERN SAMAR',
				'LEYTE',
				'NORTHERN SAMAR',
				'SAMAR (WESTERN SAMAR)',
				'SOUTHERN LEYTE',
				'BILIRAN',
				'ZAMBOANGA DEL NORTE',
				'ZAMBOANGA DEL SUR',
				'ZAMBOANGA SIBUGAY',
				'CITY OF ISABELA',
				'BUKIDNON',
				'CAMIGUIN',
				'LANAO DEL NORTE',
				'MISAMIS OCCIDENTAL',
				'MISAMIS ORIENTAL',
				'DAVAO DEL NORTE',
				'DAVAO DEL SUR',
				'DAVAO ORIENTAL',
				'COMPOSTELA VALLEY',
				'DAVAO OCCIDENTAL',
				'COTABATO (NORTH COTABATO)',
				'SOUTH COTABATO',
				'SULTAN KUDARAT',
				'SARANGAN',
				'COTABATO CITY',
				'NCR, CITY OF MANILA, FIRST DISTRICT',
				'CITY OF MANILA',
				'NCR, SECOND DISTRICT',
				'NCR, THIRD DISTRICT',
				'NCR, FOURTH DISTRICT',
				'ABRA',
				'BENGUET',
				'IFUGAO',
				'KALINGA',
				'MOUNTAIN PROVINCE',
				'APAYAO',
				'BASILAN',
				'LANAO DEL SUR',
				'MAGUINDANAO',
				'SULU',
				'TAWI-TAW',
				'AGUSAN DEL NORTE',
				'AGUSAN DEL SUR',
				'SURIGAO DEL NORTE',
				'SURIGAO DEL SUR',
				'DINAGAT ISLANDS'
        	];
        foreach ($provinces as $value) {
        	Province::create([
        		'country' => 173,
        		'name' => ucwords(strtolower($value))
        	]);
        }

        $cities =[
        	[
        		'country' => 173,
        		'province' => 40,
        		'name' => 'Mandaue City'
        	],
        	[
        		'country' => 173,
        		'province' => 40,
        		'name' => 'Cebu City'
        	],
        	[
        		'country' => 173,
        		'province' => 40,
        		'name' => 'Talisay City'
        	],
        	[
        		'country' => 173,
        		'province' => 40,
        		'name' => 'Danao City'
        	],
        	[
        		'country' => 173,
        		'province' => 40,
        		'name' => 'Bogo city'
        	]
        ];

        foreach ($cities as $city) {
        	City::create($city);
        }
    }
}
