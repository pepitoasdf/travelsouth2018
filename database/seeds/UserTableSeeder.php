<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Permission;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Ask for db migration refresh, default is no
        if ($this->command->confirm('Do you wish to refresh migration before seeding, it will clear all old data ?')) {
            // disable fk constrain check
            // \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

            // Call the php artisan migrate:refresh
            $this->command->call('migrate:refresh');
            $this->command->warn("Data cleared, starting from blank database.");

            // enable back fk constrain check
            // \DB::statement('SET FOREIGN_KEY_CHECKS=1;')
        }

        // Seed the default permissions
        $permissions = Permission::defaultPermissions();

        foreach ($permissions as $perms) {
            Permission::firstOrCreate(['name' => $perms]);
        }

        $this->command->info('Default Permissions added.');

        // add roles
        foreach(['Admin','User'] as $role) {
            $role = Role::firstOrCreate(['name' => trim($role)]);

            if( $role->name == 'Admin' ) {
                // assign all permissions
                $role->syncPermissions(Permission::all());
                $this->command->info('Admin granted all the permissions');
            } else {
                // for others by default only read access
                $role->syncPermissions(Permission::where('name', 'LIKE', 'view_%')->get());
            }

            // create one user for each role
            $this->createUser($role);
        }
        $this->command->info('Roles Admin, User added successfully');
        $this->command->warn('All done !');
    }
    private function createUser($role)
    {
        $user = User::firstOrCreate([
                'firstname' => $role->name,
                'lastname' => $role->name,
                'email' => strtolower($role->name).'@'.strtolower($role->name).'.com',
                'password' => bcrypt('secret'),
                'access_admin' => 'Yes'
            ]);
        $user->assignRole($role->name);

        $this->command->info('Here is your '.strtolower($user->email).' details to login:');
        $this->command->warn('Email is : ' .$user->email);
        $this->command->warn('Password is : secret');
        $this->command->info('You can override your credentials in Admin page :)');
    }
}
