<?php

use Illuminate\Database\Seeder;
use App\Hotel;
use App\HotelDetailFacility;
use App\HotelRoom;
use App\Photo;
class HotelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $hotels = [
       		[
       			'name' => 'Dummy Hotel 1',
       			'description' => 'Trivago’s hotel search allows users to compare hotel prices in just a few clicks from more than <strong>250</strong> booking sites for over 1 million hotels throughout the world. More than 120 million travellers use the hotel comparison every month to compare deals in the same city. Get information for trips to cities like Las Vegas or Orland and you can find the right hotel on trivago quickly and easily. New York City and its surrounding area are great for trips that are a week or longer with the numerous hotels available.',
       			'city_id' => 1,
       			'province_id' => 40,
       			'country_id' => 173,
       			'locality' => 'Plaridel Street',
       			'zip_code' => '',
       			'type_id' => 3,
       			'img' => '/admin/images/dummies/img1.jpg'
       		],
       		[
       			'name' => 'Dummy Hotel 1',
       			'description' => 'Trivago’s hotel search allows users to compare hotel prices in just a few clicks from more than <strong>250</strong> booking sites for over 1 million hotels throughout the world. More than 120 million travellers use the hotel comparison every month to compare deals in the same city. Get information for trips to cities like Las Vegas or Orland and you can find the right hotel on trivago quickly and easily. New York City and its surrounding area are great for trips that are a week or longer with the numerous hotels available.',
       			'city_id' => 2,
       			'province_id' => 40,
       			'country_id' => 173,
       			'locality' => 'Burgos Street',
       			'zip_code' => '',
       			'type_id' => 5,
       			'img' => '/admin/images/dummies/3.jpg'
       		],
       		[
       			'name' => 'Dummy Hotel 1',
       			'description' => 'Trivago’s hotel search allows users to compare hotel prices in just a few clicks from more than <strong>250</strong> booking sites for over 1 million hotels throughout the world. More than 120 million travellers use the hotel comparison every month to compare deals in the same city. Get information for trips to cities like Las Vegas or Orland and you can find the right hotel on trivago quickly and easily. New York City and its surrounding area are great for trips that are a week or longer with the numerous hotels available.',
       			'city_id' => 3,
       			'province_id' => 40,
       			'country_id' => 173,
       			'locality' => 'Zamora Street',
       			'zip_code' => '',
       			'type_id' => 2,
       			'img' => '/admin/images/dummies/img1.jpg'
       		],
       		[
       			'name' => 'Dummy Hotel 1',
       			'description' => 'Trivago’s hotel search allows users to compare hotel prices in just a few clicks from more than <strong>250</strong> booking sites for over 1 million hotels throughout the world. More than 120 million travellers use the hotel comparison every month to compare deals in the same city. Get information for trips to cities like Las Vegas or Orland and you can find the right hotel on trivago quickly and easily. New York City and its surrounding area are great for trips that are a week or longer with the numerous hotels available.',
       			'city_id' => 4,
       			'province_id' => 40,
       			'country_id' => 173,
       			'locality' => 'Duterte Street',
       			'zip_code' => '',
       			'type_id' => 1,
       			'img' => '/admin/images/dummies/Image_17.jpg'
       		],
       		[
       			'name' => 'Dummy Hotel 1',
       			'description' => 'Trivago’s hotel search allows users to compare hotel prices in just a few clicks from more than <strong>250</strong> booking sites for over 1 million hotels throughout the world. More than 120 million travellers use the hotel comparison every month to compare deals in the same city. Get information for trips to cities like Las Vegas or Orland and you can find the right hotel on trivago quickly and easily. New York City and its surrounding area are great for trips that are a week or longer with the numerous hotels available.',
       			'city_id' => 5,
       			'province_id' => 40,
       			'country_id' => 173,
       			'locality' => 'Cambaro, Plaridel Street',
       			'zip_code' => '',
       			'type_id' => 4,
       			'img' => '/admin/images/dummies/Image_17.jpg'
       		]
       ];

       foreach ($hotels as $hotel) {
       		$gethotel = Hotel::create([
       			'name' => $hotel['name'],
       			'description' => $hotel['description'],
       			'city_id' => $hotel['city_id'],
       			'province_id' => $hotel['province_id'],
       			'country_id' => $hotel['country_id'],
       			'locality' => $hotel['locality'],
       			'zip_code' => $hotel['zip_code'],
       			'type_id' => $hotel['type_id']
       		]);
       		$destinationPath = 'admin/images/uploads/hotels';
	    	$image = $hotel['img'];

       		$this->saveImage($image, $gethotel->id, 'hotels', $destinationPath);

       		for ($i=1; $i <=10; $i++) { 
       			Hotel::insertFacilities($gethotel->id, $i);
       		}
       }
    }
    public  function saveImage($image, $id, $model, $destinationPath){
    	$destinationPath2 = public_path().'/'.$destinationPath;

		$imgSource = public_path().$image;

		if(!File::exists($destinationPath2)) {
            File::makeDirectory($destinationPath2, $mode = 0777, true, true);
        }

        $filename = uniqid().'_'.time() . '.jpg';

		$original = Image::make($imgSource);
    	$original->save($destinationPath2.'/'.$filename);

    	$destinationResize100 = $destinationPath2.'/thumb_100';
    	$destinationResize200 = $destinationPath2.'/thumb_570x400';
    	$destinationResizeSquare1920X1080 = $destinationPath2.'/1920X1080';

    	if(!File::exists($destinationPath2)) {
		    File::makeDirectory($destinationPath2);
		}
		if(!File::exists($destinationResize100)) {
		    File::makeDirectory($destinationResize100);
		}
		if(!File::exists($destinationResize200)) {
		    File::makeDirectory($destinationResize200);
		}
		if(!File::exists($destinationResizeSquare1920X1080)) {
		    File::makeDirectory($destinationResizeSquare1920X1080);
		}

        $resize = Image::make($imgSource);
        $resize->resize(null, 100, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationResize100.'/'.$filename);

        $resize = Image::make($imgSource);
        $resize->resize(null, 200, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationResize200.'/'.$filename);

        $resize = Image::make($imgSource);
        $resize->resize(50, 50)->save($destinationResizeSquare1920X1080.'/'.$filename);

    	Photo::savePhoto($id, $model, $filename, $destinationPath);
    }
}
