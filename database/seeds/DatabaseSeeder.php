<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(HotelFacilityTableSeeder::class);
        $this->call(VehicleFeatureSeeder::class);
        $this->call(PackageFeatureSeeder::class);
        $this->call(RoomTypeTableSeeder::class);
        $this->call(HotelTypeTableSeeder::class);
        $this->call(ProvinceCityTableSeeder::class);
    }
}
