<?php

use Illuminate\Database\Seeder;
use App\HomeBackground;
use App\Photo;
class PageHomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	////######## Background #########////////
        $backgrounds = [
        	[
        		'title' => 'Starts from $199',
        		'subtitle' => 'Arakaki Realtor',
        		'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        		'status' => 'Published',
        		'img' => '/admin/images/dummies/slider1bg.jpg'
        	],
        	[
        		'title' => 'Starts from $299',
        		'subtitle' => 'Arakaki Realtor2',
        		'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        		'status' => 'Published',
        		'img' => '/admin/images/dummies/slider2bg.jpg'
        	],
        	[
        		'title' => 'Starts from $399',
        		'subtitle' => 'Arakaki Realtor3',
        		'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        		'status' => 'Published',
        		'img' => '/admin/images/dummies/slider1bg.jpg'
        	],
        	[
        		'title' => 'Starts from $499',
        		'subtitle' => 'Arakaki Realtor4',
        		'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        		'status' => 'Published',
        		'img' => '/admin/images/dummies/slider2bg.jpg'
        	],
        	[
        		'title' => 'Starts from $599',
        		'subtitle' => 'Arakaki Realtor5',
        		'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        		'status' => 'Published',
        		'img' => '/admin/images/dummies/slider1bg.jpg'
        	]
        ];

        foreach ($backgrounds as $background) {
        	$save = HomeBackground::create([
        		'title' => $background['title'],
        		'subtitle' => $background['subtitle'],
        		'description' => $background['description'],
        		'status' => $background['status']
        	]);
        	if($save){
	    		$destinationPath = 'admin/images/uploads/background';
	    		$image = $background['img'];
        		$this->saveImage($image,$save->id, 'background', $destinationPath);
        	}
        }
        ////######## Popular #########////////
        $populars = [
        	[
        		'title' => 'OUR MOST POPULAR PACKAGES',
        		'description' => 'Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin',
        		'status' => 'Published',
        		'img' => '/admin/images/dummies/thumb01.jpg'
        	]
        ];
    }
    public  function saveImage($image, $id, $model, $destinationPath){
    	$destinationPath2 = public_path().'/'.$destinationPath;

		$imgSource = public_path().$image;

		if(!File::exists($destinationPath2)) {
            File::makeDirectory($destinationPath2, $mode = 0777, true, true);
        }

        $filename = uniqid().'_'.time() . '.jpg';

		$original = Image::make($imgSource);
    	$original->save($destinationPath2.'/'.$filename);

    	$destinationResize100 = $destinationPath2.'/thumb_100';
    	$destinationResize200 = $destinationPath2.'/thumb_570x400';
    	$destinationResizeSquare1920X1080 = $destinationPath2.'/1920X1080';

    	if(!File::exists($destinationPath2)) {
		    File::makeDirectory($destinationPath2);
		}
		if(!File::exists($destinationResize100)) {
		    File::makeDirectory($destinationResize100);
		}
		if(!File::exists($destinationResize200)) {
		    File::makeDirectory($destinationResize200);
		}
		if(!File::exists($destinationResizeSquare1920X1080)) {
		    File::makeDirectory($destinationResizeSquare1920X1080);
		}

        $resize = Image::make($imgSource);
        $resize->resize(null, 100, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationResize100.'/'.$filename);

        $resize = Image::make($imgSource);
        $resize->resize(null, 200, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationResize200.'/'.$filename);

        $resize = Image::make($imgSource);
        $resize->resize(50, 50)->save($destinationResizeSquare1920X1080.'/'.$filename);

    	Photo::savePhoto($id, $model, $filename, $destinationPath);
    }
}
