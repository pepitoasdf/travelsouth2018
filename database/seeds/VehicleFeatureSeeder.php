<?php

use Illuminate\Database\Seeder;
use App\VehicleFeature;
class VehicleFeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
        	['description' => 'Cruise control'],
        	['description' => 'Heated seats'],
        	['description' => 'Gear indicator'],
        	['description' => 'Sunroof'],
        	['description' => 'Roof rail'],
        	['description' => 'FOG LAMPS'],
        	['description' => 'TRACTION CONTROL'],
        	['description' => 'MULTIPLE 12V POWER OUTLETS'],
        	['description' => 'ANTILOCK BRAKING SYSTEM'],
        	['description' => 'DEFOGGER'],
        	['description' => 'ADJUSTABLE COMFORTS'],
        	['description' => 'AIRBAGS'],
        	['description' => 'REVERSE SENSING SYSTEM'],
        	['description' => 'Power Steering'],
        	['description' => 'Audio system'],
        	['description' => 'Cruise Control'],
        	['description' => 'Airconditions'],
        	['description' => 'Electric windows'],
        	['description' => '24hr Vehicle Assistance']
        ];

        foreach ($datas as $data) {
        	VehicleFeature::create($data);
        }
    }
}
