<?php

use Illuminate\Database\Seeder;
use App\HotelFacility;
class HotelFacilityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
        	['description' => 'Airport Hotel Courtesy Desk'],
        	['description' => 'Bakery / Patisserie'],
        	['description' => 'Beach'],
        	['description' => 'Beauty Parlour'],
        	['description' => 'Bicycle Trail'],
        	['description' => 'Business Centre'],
        	['description' => 'Child Care Services'],
        	['description' => 'Children Playground'],
        	['description' => 'Concierge'],
        	['description' => 'Drug Store'],
        	['description' => 'Dry Cleaning'],
        	['description' => 'Executive Lounge'],
        	['description' => 'Express Check-In'],
        	['description' => 'Express Check-Out'],
        	['description' => 'Fitness Centre / Health Club'],
        	['description' => 'Restaurant'],
        	['description' => 'Table Tennis'],
        	['description' => 'Room Service'],
        	['description' => 'Safe Deposit Box'],
        	['description' => 'Parking - Valet'],
            ['description' => 'High speed Internet access in all rooms'],
            ['description' => 'Complimentary WiFi in public areas'],
            ['description' => 'Elevators'],
            ['description' => 'Meeting rooms'],
            ['description' => 'Local area transportation'],
            ['description' => 'Photo copying service']
        ];

        foreach ($datas as $data) {
        	HotelFacility::create($data);
        }
    }
}