<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('header_id');
            $table->enum('service_type',array('Hotel','Package','Vehicle'));
            $table->integer('hotel_id')->nullable();
            $table->integer('room_id')->nullable();
            $table->integer('package_id')->nullable();
            $table->integer('vehicle_id')->nullable();
            $table->enum('rate_type',array('hourly','daily'))->default('daily');
            $table->integer('duration')->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->decimal('amount',24,2)->default(0);
            $table->decimal('balance',24,2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_details');
    }
}
