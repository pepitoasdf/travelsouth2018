<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->text('address');
            $table->string('phone');
            $table->integer('currency_id');
            $table->text('remarks');
            $table->date('date');
            $table->enum('type', array('Requested','Booked'));
            $table->enum('service_type', array('Package','Hotel','Vehicle'));
            $table->integer('request_id')->nullable();
            $table->integer('service_id');
            $table->integer('duration');
            $table->enum('duration_type',array('days','hours'));
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->decimal('amount',24,2);
            $table->enum('status', array('New','Finalized','Approved','Voided'))->default('New');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
