<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingSuggestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_suggestions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id')->nullable();
            $table->text('remarks');
            $table->integer('service_id');
            $table->integer('duration');
            $table->enum('duration_type',array('days','hours'));
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->decimal('amount',24,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_suggestions');
    }
}
