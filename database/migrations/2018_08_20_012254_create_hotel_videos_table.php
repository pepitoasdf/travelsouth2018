<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_id');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->text('video_url')->nullable();
            $table->enum('status',array('Published','Draft','Pending'))->default('Draft');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_videos');
    }
}
