<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('pricing')->nullable();
            $table->text('description')->nullable();
            $table->string('color')->nullable();
            $table->string('plate_no')->nullable();
            $table->string('year_model')->nullable();
            $table->string('mv_file_no')->nullable();
            $table->string('engine_no')->nullable();
            $table->integer('no_of_wheel')->nullable();
            $table->enum('transmission',array('Automatic','Manual'))->default('Automatic');
            $table->integer('seats');
            $table->integer('doors');
            $table->enum('aircon',array('Yes','No'))->default('Yes');
            $table->enum('status',array('Published','Draft','Pending'))->default('Draft');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
