(function(){
	'use strict';

	window.addEventListener('load', () => {
		let	d = document,
			tabLinks = d.querySelectorAll('.tab__links'),
			navTrigger = d.querySelector('.nav-trigger'),
			hasChilds = d.querySelectorAll('.has-menu-child'),
			menu = d.querySelector('.menu'),
			menuLink = d.querySelectorAll('.menu__item'),
			winWid = window.innerWidth || d.documentElement.clientWidth || d.getElementsByTagName('body')[0].clientWidth,
			photoUpload = d.querySelector('#photo-uploader'),
			preview = d.querySelector('.preview'),
			toUpload = d.querySelector('.to-upload');

		// photoUpload.addEventListener('change', e => {

		// 	let reader = new FileReader();							

		// 	if(e.srcElement.files[0]) {

		// 		reader.addEventListener('load', e => {
		// 			preview.setAttribute('src', e.target.result);

		// 		});	

		// 		reader.readAsDataURL(e.srcElement.files[0]);
		// 	}

		// });

		if(toUpload) {
			toUpload.addEventListener('click', e => {
				e.preventDefault();
				photoUpload.click();
			});
		}


		navTrigger.addEventListener('click', (evt) => {
			evt.preventDefault();

			let navData = navTrigger.getAttribute('data-nav');

			d.getElementById(navData).classList.toggle('active');
		});


		(function() {
		    var throttle = function(type, name, obj) {
		        obj = obj || window;
		        var running = false;
		        var func = function() {
		            if (running) { return; }
		            running = true;
		             requestAnimationFrame(function() {
		                obj.dispatchEvent(new CustomEvent(name));
		                running = false;
		            });
		        };
		        obj.addEventListener(type, func);
		    };

		    throttle("resize", "optimizedResize");
		})();



		function dropDown() {

	    	hasChilds.forEach(el => {

	    		el.addEventListener('click', e => {

	    			let firstEl = el.firstElementChild;

	    			firstEl.nextElementSibling.classList.add('active');

	    			menuLink.forEach((el) => {

	    				if(!e.target.contains(el)) {
	    					let nxtSib = el.nextElementSibling;

	    					if(nxtSib) {
	    						nxtSib.classList.remove('active');
	    					}
	    				}
	    			});
	    		});
	    	});    			    	
		}

		dropDown();
	



		window.addEventListener("optimizedResize", function() {

			let winWid = window.innerWidth || d.documentElement.clientWidth || d.getElementsByTagName('body')[0].clientWidth;

		    if ( winWid > 768) {

		    	menuLink.forEach( el => {
				
					let nxtSib = el.nextElementSibling;

					if(nxtSib) {
						nxtSib.classList.remove('active');
					}					
				});	

		    } else {

		    	dropDown();
		    }
		});


		tabLinks.forEach( tabLink => {
			tabLink.addEventListener('click', (evt) => {
				evt.preventDefault();

				let tab = tabLink.getAttribute('data-tab');

				if(tabLink) {
					tabLinks.forEach( tabLink => {
						let tab = tabLink.getAttribute('data-tab');
						tabLink.classList.remove('triggered');
						d.getElementById(tab).classList.remove('active');
					});
				}

				tabLink.classList.add('triggered');
				d.getElementById(tab).classList.add('active');

			});
		});
	})
})();