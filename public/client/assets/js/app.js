Vue.component('modal', {
  	template: '#modal-template'
});

Vue.component('myForm', {
	template: `<div class="">

		        <h2>Booking for:</h2>
		        <form class="booking-form" id="via-email" v-on:submit.prevent="checkForm">
		        	{{ actionMessage }}
					<div class="gap gap-3">
						<div class="xs-12 md-6">
							<div class="form__group">
								<label for="name">Name:*</label>
								<input type="text" id="name" class="form__control" v-model="name">
								{{ name }}
							</div>
						</div>
						<div class="xs-12 md-6">
							<div class="form__group">
								<label for="email">Email:*</label>
								<input type="text" id="email" class="form__control" v-model="email">
							</div>
						</div>
					</div>
					<div class="gap gap-3">
						<div class="xs-12 md-6">
							<div class="form__group">
								<label for="phone">Phone:*</label>
								<input type="text" id="phone" class="form__control" v-model="phone">
							</div>
						</div>
						<div class="xs-12 md-6">
							<div class="form__group">
								<label for="price">Price:</label>
								<input type="text" id="price" class="form__control" v-model="price">
							</div>
						</div>
					</div>

					<div class="form__group">
						<label for="message">Extra Information:</label>
						<textarea name="" id="message" cols="30" rows="5" class="form__control" v-model="message"></textarea>
					</div>

					<button class="btn btn-book-submit btn-block" :disabled="!formIsValid">Book By E-mail</button>
				</form>
				<br><br>
				<p class="t-center">
					or	
				</p>

				<a href="" class="btn btn-paypal btn-block">Check Out Via PayPal</a>
		    </div>`,
	data() {
		return {
			name: null,
			email: null,
			phone: null,
			price: null,
			message: null,
			actionMessage: null,
			errors: []
		}
	},

	computed: {
		formIsValid() {
			return this.name !== null &&
			this.validName(this.name) &&
			this.email !== null && 
			this.validEmail(this.email) &&
			this.phone !== null,
			this.price !== null,
			this.message !== null
		}
	},

	methods: {

		validEmail: function(email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		},
		validName: function(name){
			var re = /^[A-Za-z\s]+$/;
			return re.test(name);
		},
		submitForm: function() {

			var myData = {
				name: this.name,
                email: this.email,
                phone: this.phone,
                price: this.price,
                message: this.message
			}

			axios.post('sample-send.php', // <------- url 
				myData
			)
			.then(function (resp) {

				console.log(resp);
				
				this.actionMessage = 'Thank you for sending';
                
                this.name = null;
				this.email = null;
				this.phone = null;
				this.price = null;
				this.message = null;
				this.errors = [];

            }, function () {
                console.log('Form submission failed');
            });
		},
		checkForm: function() {

			

		}
	}
});

Vue.component('account-form', {
	props: {
		editIt: {
	      type: Boolean
		}
	},
	template: `<form action="" @submit.prevent="updateAccount">
							
					
					<div class="profile-photo-container"><img src="http://via.placeholder.com/300x300" alt="" class="preview"></div>
					<div v-if="editIt">
						<input type="file" id="photo-uploader" @change="uploadPhoto">
						<span class="to-upload btn btn-blue btn-lg" @click="uploadPhoto('#e')">Upload file</span>
					</div>

					<div class="form__group">
						<input type="text" v-model="accountName" :disabled="!editIt" class="form__control" placeholder="Name" >
					</div>
					<div class="form__group">
						<input type="text" v-model="accountAddress" :disabled="!editIt" class="form__control" placeholder="Address">
					</div>

					<button :disabled="!formIsValid">Submit</button>


						</form>`,
	data() {
		return {
			accountName: 'Sample name',
			accountAddress: 'Sample address 101',
			selectedFile: null,
			errors: []
		}
	},

	computed: {
		formIsValid() {
			return this.accountName !== '' && this.validName(this.accountName) && this.accountAddress !== '';
		}
	},

	methods: {
		uploadPhoto: function(e) {

			let d = document,
				reader = new FileReader(),
				photoUpload = d.querySelector('#photo-uploader'),
				preview = d.querySelector('.preview');				

			
			if(e.srcElement.files[0]) {

				reader.addEventListener('load', e => {
					preview.setAttribute('src', e.target.result);

				});	

				reader.readAsDataURL(e.srcElement.files[0]);
			}
    	},

    	toUpload: function(e) {
    		let photoUpload = document.querySelector('#photo-uploader');
    		this.uploadPhoto(photoUpload).click();
    	},

		validName: function(name){
			var re = /^[A-Za-z\s]+$/;
			return re.test(name);
		},

		submitForm: function() {

			var myData = {
				name: this.accountName,
                address: this.accountAddress
			}

			axios.post('sample-send.php', // <------- url 
				myData
			)
			.then(function (resp) {							
				this.actionMessage = 'Thank you for sending';
                
                this.accountName = null;
				this.accountAddress = null;
				this.errors = [];

            }, function () {
                console.log('Form submission failed');
            });
		},



		updateAccount: function() {

			this.errors = [];

			if(!this.accountName || this.accountName.length < 2) {
				this.errors.push('www')
			} else if (!this.validName(this.accountName)) {
				this.errors.push('waaa')
			}

			if(!this.accountAddress || this.accountAddress == 'null') {
				this.errors.push('way sulod');
			}


			if(this.errors.length == 0) {
				this.submitForm();
			}

		}
	}

});


if(document.querySelector('#depositModal')) {
	new Vue({
		el: '#depositModal',
		data() {
		    return {
		    	showModal: false
		    }
		}
	});
}

if(document.querySelector('#edit-account')) {
	new Vue({ 
		el: '#edit-account',
		data() {
			return {
				editTable: false
			}
		}
	});
}