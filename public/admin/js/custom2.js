$('.datepicker').datepicker({
    format: 'yyyy-mm-dd'
});

$('select').on(
    'select2:close',
    function () {
        $(this).focus();
    }
);