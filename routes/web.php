<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['namespace' => 'Client'], function(){
	Route::get('/', 'PagesController@index')->name('welcome');
	Route::get('/services/packages', 'PagesController@packages')->name('client.packages');
	Route::get('/services/packages/{id}/{slug}', 'PagesController@packageIndividual')->name('client.packages.individual');
	Route::get('/services/hotels', 'PagesController@hotels')->name('client.hotels');
	Route::get('/services/hotels/{id}/{slug}', 'PagesController@hotelIndividual')->name('client.hotels.individual');
	Route::get('/services/vehicles', 'PagesController@vehicles')->name('client.vehicles');
	Route::get('/services/vehicles/{id}/{slug}', 'PagesController@vehicleIndividual')->name('client.vehicles.individual');

	Route::get('/about', 'PagesController@about')->name('client.about');
	Route::get('/contact', 'PagesController@contact')->name('client.contact');
	Route::get('/account', 'PagesController@account')->name('client.account');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function(){
	Route::get('/', 'DashboardController@admin')->name('admin.index');
	Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');
	Route::resource('/roles', 'RoleController');
	Route::resource('/users', 'UserController');
	
	Route::resource('/settings/hotels/facilities', 'HotelFacilityController');
	Route::resource('/settings/hotels/hoteltypes', 'HotelTypeController');
	Route::resource('/settings/hotels/roomtypes', 'HotelRoomTypeController');
	Route::resource('/settings/places/countries', 'CountryController');
	Route::resource('/settings/places/provinces', 'ProvinceController');
	Route::resource('/settings/places/cities', 'CityController');
	Route::resource('/settings/vehicles/vfeatures', 'VehicleFeatureController');
	Route::resource('/settings/packages/pfeatures', 'PackageFeatureController');
	Route::resource('/settings/system/admin', 'SystemAdminController');
	Route::resource('/settings/system/client', 'SystemClientController');

	Route::resource('/services/hotels', 'HotelController');
	Route::resource('/services/hotels/{id}/rooms', 'HotelRoomController');
	Route::resource('/services/hotels/{id}/hgalleries', 'HotelGalleryController');
	Route::resource('/services/hotels/{id}/roomrates', 'HotelRoomRateController');
	Route::resource('/services/hotels/{id}/hvideos', 'HotelVideoController');
	Route::resource('/services/vehicles', 'VehicleController');
	Route::resource('/services/vehicles/{id}/vgalleries', 'VehicleGalleryController');
	Route::resource('/services/vehicles/{id}/vrates', 'VehicleRateController');
	Route::resource('/services/vehicles/{id}/vvideos', 'VehicleVideoController');
	Route::resource('/services/packages', 'PackageController');
	Route::resource('/services/packages/{id}/prates', 'PackageRateController');
	Route::resource('/services/packages/{id}/pgalleries', 'PackageGalleryController');
	Route::resource('/services/packages/{id}/pitineraries', 'PackageItineraryController');
	Route::resource('/services/packages/{id}/pguides', 'PackageGuideController');
	Route::resource('/services/packages/{id}/pvideos', 'PackageVideoController');

	Route::get('/services/hotels/{id}/hgalleries/{hgallery}/rooms','HotelGalleryController@roomIndex')
		->name('hgalleries.roomIndex');
	Route::get('/services/hotels/{id}/hgalleries/{hgallery}/rooms/create','HotelGalleryController@roomCreate')
		->name('hgalleries.roomCreate');
	Route::post('/services/hotels/{id}/hgalleries/{hgallery}/rooms/create','HotelGalleryController@roomStore')
		->name('hgalleries.roomStore');

	Route::resource('/booking/bookings', 'BookingController');
	Route::resource('/booking/requests', 'BookingRequestController');
	Route::resource('/booking/suggests', 'BookingSuggestController');
	Route::resource('/pages/contacts', 'ContactController');
	Route::resource('/pages/abouts', 'AboutController');
	Route::resource('/pages/blog/posts', 'PostController');
	Route::resource('/pages/home/background', 'HomeBackgroundController');
	Route::resource('/pages/home/increadibleplaces', 'HomeIncreadibleplaceController');
	Route::resource('/pages/home/partners', 'HomePartnerController');
	Route::resource('/pages/home/popular', 'HomePopularController');
	Route::resource('/pages/home/topdestinations', 'HomeTopdestinationController');
	Route::resource('/pages/home/videos', 'HomeVideoController');
	

	/***  AJAX ROUTE ***/
	Route::group(['prefix' => 'api', 'namespace' => 'Ajax'], function(){
		Route::get('hotels','AppAjaxController@hotels')->name('hotels.api');
		Route::get('rooms','AppAjaxController@rooms')->name('rooms.api');
		Route::get('hotel/rooms','AppAjaxController@hotelRooms')->name('hotelRooms.api');
		Route::get('vehicles','AppAjaxController@vehicles')->name('vehicles.api');
		Route::get('users','AppAjaxController@users')->name('users.api');
		Route::get('packages','AppAjaxController@packages')->name('packages.api');
		Route::get('getpermissions','AppAjaxController@getrole')->name('getrole.api');
		Route::get('hotel-locations','AppAjaxController@hotelLocations')->name('hotelLocations.api');
		Route::get('countries','AppAjaxController@countries')->name('countries.api');
		Route::get('provinces','AppAjaxController@provinces')->name('provinces.api');
		Route::get('cities','AppAjaxController@cities')->name('cities.api');
		Route::get('topdestination/update','AppAjaxController@topdestinationUpdate')->name('topdestinationUpdate.api');
		Route::get('increadibleplace/update','AppAjaxController@increadibleplaceUpdate')->name('increadibleplaceUpdate.api');
	});
});
