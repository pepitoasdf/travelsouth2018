<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="image-show">Images :</label>
    <div class="col-md-7">
        @if(isset($photos))
            <!-- <div class="imageshow" style="max-height: 500px; overflow-y:scroll;margin:5px 0px 10px 0px"> -->
            <div class="imageshow custom-scroll-content scrollbar-enabled" style="max-height: 500px; overflow-y:scroll;">
                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="data-table">
                        <tbody>
                            @foreach($photos as $photo)
                                <tr>
                                    <td>
                                        <a href="{{ asset($photo->path.'/'.$photo->file_name) }}" target="_blank">
                                            <img src="{{ asset($photo->path.'/thumb_100/'.$photo->file_name) }}">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" data-image="{{ $photo->file_name }}" data-refid="{{ $photo->id }}" class="btn btn-danger btn-rounded btn-sm remove-image" title="Remove">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
        <input multiple=""  name="images[]" id="image-show" accept="image/*" type="file"  />
        @if ($errors->has('images'))
            <span class="help-block">
                <strong>{{ $errors->first('images') }}</strong>
            </span>
        @endif
        <div id="list"></div>
    </div>
</div>

@push('append_js')
    <script type="text/javascript">
        function handleFileSelect(evt) {
            var files = evt.target.files; // FileList object

            $('div#list').empty();

            // Loop through the FileList and render image files as thumbnails.
            for (var i = 0, f; f = files[i]; i++) {

                // Only process image files.
                if (!f.type.match('image.*')) {
                    continue;
                }

                var reader = new FileReader();

                // Closure to capture the file information.
                reader.onload = (function(theFile) {
                    return function(e) {
                        // Render thumbnail.
                        var span = document.createElement('span');
                        span.innerHTML = ['<img class="thumb-preview" src="', e.target.result,
                        '" title="', escape(theFile.name), '"/>'].join('');
                        document.getElementById('list').insertBefore(span, null);
                    };
                })(f); 

                // Read in the image file as a data URL.
                reader.readAsDataURL(f);
            }
        }

        document.getElementById('image-show').addEventListener('change', handleFileSelect, false);

        $('.remove-image').click(function(e){
            e.preventDefault();
            $('.imageshow').append('<input type="hidden" name="removephotoid[]" value="'+$(this).data("refid")+'"/>');
            $('.imageshow').append('<input type="hidden" name="removephotoname[]" value="'+$(this).data("image")+'"/>');
            $(this).closest('tr').remove();
        });
    </script>
@endpush