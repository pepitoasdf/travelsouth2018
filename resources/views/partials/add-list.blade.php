@if(Request::route()->getName() != ($entity.".index"))
	@can('view_'.$entity)
		@if(isset($refid))
			<a href="{{ route($entity.'.index',$refid) }}" class="btn btn-default btn-rounded btn-sm"><i class="list-icon feather feather-list"></i> &nbsp;View Listing</a>
		@else
			<a href="{{ route($entity.'.index') }}" class="btn btn-default btn-rounded btn-sm"><i class="list-icon feather feather-list"></i> &nbsp;View Listing</a>
		@endif
	@endcan
@endif
@can('add_'.$entity)
	@if(isset($refid))
		<a href="{{ route($entity.'.create',$refid) }}" class="btn btn-info btn-rounded btn-sm"><i class="list-icon feather feather-plus"></i> &nbsp;Add {{ ucwords($reference) }}</a>
	@else
		<a href="{{ route($entity.'.create') }}" class="btn btn-info btn-rounded btn-sm"><i class="list-icon feather feather-plus"></i> &nbsp;Add {{ ucwords($reference) }}</a>
	@endif
@endcan