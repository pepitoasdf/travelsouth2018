@if(!isset($remove_edit))
	@can('edit_'.$entity)
		@if(isset($ref_id))
	    	<a title="Edit" href="{{ route($entity.'.edit', ['id' => $ref_id, str_singular($entity) => $id])  }}" class="btn btn-sm btn-rounded btn-info">
	        <i class="fa fa-pencil"></i></a>
	    @else
	    	<a title="Edit" href="{{ route($entity.'.edit', [str_singular($entity) => $id])  }}" class="btn btn-sm btn-rounded btn-info">
	        <i class="fa fa-pencil"></i></a>
	    @endif
	@endcan
@endif

@if(!isset($remove_delete))
	@can('delete_'.$entity)
		@if(isset($ref_id))
		    <form  data-pjax action="{{route($entity.'.destroy', ['id' => $ref_id, str_singular($entity) => $id])}}" style="display: inline" onsubmit="return confirm('Do you really want to delete it?')" method="POST">
		    	{{csrf_field()}}
		    	{{ method_field('DELETE') }}
		        <button title="Delete" type="submit" class="btn-delete btn btn-sm btn-rounded btn-danger">
		            <i class="fa fa-trash-o"></i>
		        </button>
		    </form>
		@else
			<form  data-pjax action="{{route($entity.'.destroy', [str_singular($entity) => $id])}}" style="display: inline" onsubmit="return confirm('Do you really want to delete it?')" method="POST">
		    	{{csrf_field()}}
		    	{{ method_field('DELETE') }}
		        <button title="Delete" type="submit" class="btn-delete btn btn-sm btn-rounded btn-danger">
		            <i class="fa fa-trash-o"></i>
		        </button>
		    </form>
		@endif
	@endcan
@endif