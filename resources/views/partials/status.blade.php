<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="status">Status :</label>
    <div class="col-md-7">
        @if(isset($data))
            <div class="radiobox radio-primary bw-3 pd-t-0 pd-b-0">
                <label class="radiobox">
                    <input type="radio" name="status" value="Draft" {{ $data->status == "Draft" ? 'checked' : '' }} />
                    <span class="label-text">Draft</span>
                </label>
            </div>
            <div class="radiobox radio-primary bw-3 pd-t-0 pd-b-0">
                <label class="radiobox">
                    <input type="radio" name="status" value="Published" {{ $data->status == "Published" ? 'checked' : '' }} />
                    <span class="label-text">Published</span>
                </label>
            </div>
            <div class="radiobox radio-primary bw-3 pd-t-0 pd-b-0">
                <label class="radiobox">
                    <input type="radio" name="status" value="Pending" {{ $data->status == "Pending" ? 'checked' : '' }} />
                    <span class="label-text">Pending</span>
                </label>
            </div>
        @elseif(old('status'))
            <div class="radiobox radio-primary bw-3 pd-t-0 pd-b-0">
                <label class="radiobox">
                    <input type="radio" name="status" value="Draft" {{ old('status') == 'Draft' ? 'checked' : '' }} />
                    <span class="label-text">Draft</span>
                </label>
            </div>
            <div class="radiobox radio-primary bw-3 pd-t-0 pd-b-0">
                <label class="radiobox">
                    <input type="radio" name="status" value="Published" {{ old('status') == 'Published' ? 'checked' : '' }}/>
                    <span class="label-text">Published</span>
                </label>
            </div>
            <div class="radiobox radio-primary bw-3 pd-t-0 pd-b-0">
                <label class="radiobox">
                    <input type="radio" name="status" value="Pending" {{ old('status') == 'Pending' ? 'checked' : '' }}/>
                    <span class="label-text">Pending</span>
                </label>
            </div>
        @else
            <div class="radiobox radio-primary bw-3 pd-t-0 pd-b-0">
                <label class="radiobox">
                    <input type="radio" checked="" name="status" value="Draft" />
                    <span class="label-text">Draft</span>
                </label>
            </div>
            <div class="radiobox radio-primary bw-3 pd-t-0 pd-b-0">
                <label class="radiobox">
                    <input type="radio" name="status" value="Published"/>
                    <span class="label-text">Published</span>
                </label>
            </div>
            <div class="radiobox radio-primary bw-3 pd-t-0 pd-b-0">
                <label class="radiobox">
                    <input type="radio" name="status" value="Pending" />
                    <span class="label-text">Pending</span>
                </label>
            </div>
        @endif
        @if ($errors->has('status'))
            <span class="help-block">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>