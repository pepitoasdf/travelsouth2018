<div class="form-group row">
    <label class="col-md-4 col-form-label text-right" for="image-show">Images :</label>
    <div class="col-md-8">
        @if(isset($photo))
            <!-- <div class="imageshow" style="max-height: 500px; overflow-y:scroll;margin:5px 0px 10px 0px"> -->
            <div class="imageshow custom-scroll-content scrollbar-enabled" style="max-height: 500px; overflow-y:scroll;">
                <a href="{{ asset($photo->path.'/'.$photo->file_name) }}" target="_blank">
                    <img src="{{ asset($photo->path.'/thumb_100/'.$photo->file_name) }}">
                </a>
            </div>
        @endif
        <input multiple=""  name="image" id="image-show" accept="image/*" type="file" />
        @if ($errors->has('image'))
            <span class="help-block">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
        @endif
        <div id="list"></div>
    </div>
</div>

@push('append_js')
    <script type="text/javascript">
        function handleFileSelect(evt) {
            var files = evt.target.files; // FileList object

            $('div#list').empty();

            // Loop through the FileList and render image files as thumbnails.
            for (var i = 0, f; f = files[i]; i++) {

                // Only process image files.
                if (!f.type.match('image.*')) {
                    continue;
                }

                var reader = new FileReader();

                // Closure to capture the file information.
                reader.onload = (function(theFile) {
                    return function(e) {
                        // Render thumbnail.
                        var span = document.createElement('span');
                        span.innerHTML = ['<img class="thumb-preview" src="', e.target.result,
                        '" title="', escape(theFile.name), '"/>'].join('');
                        document.getElementById('list').insertBefore(span, null);
                    };
                })(f); 

                // Read in the image file as a data URL.
                reader.readAsDataURL(f);
            }
        }

        document.getElementById('image-show').addEventListener('change', handleFileSelect, false);
    </script>
@endpush