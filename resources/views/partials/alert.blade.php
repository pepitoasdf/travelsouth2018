@if(Session::has('alert-success'))
	<div class="alert alert-icon alert-success border-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      		<span aria-hidden="true">×</span>
        </button>
        <i class="material-icons list-icon">check_circle</i>
        <strong>Well done!</strong> {{ Session::get('alert-success') }}
    </div>
@elseif(Session::has('alert-info'))
	<div class="alert alert-icon alert-info border-info alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        	<span aria-hidden="true">×</span>
        </button>
        <i class="material-icons list-icon">info</i>
        <strong>Heads up!</strong> {{ Session::get('alert-info') }}
    </div>
@elseif(Session::has('alert-warning'))
	<div class="alert alert-icon alert-warning border-warning alert-dismissible fade show" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    <i class="material-icons list-icon">warning</i>
	    <strong>Important!</strong> {{ Session::get('alert-warning') }}
	</div>
@elseif(Session::has('alert-danger'))
	<div class="alert alert-icon alert-danger border-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        	<span aria-hidden="true">×</span>
        </button>
        <i class="material-icons list-icon">not_interested</i>
        <strong>Oh snap!</strong> {{ Session::get('alert-danger') }}
    </div>
@endif