<script type="text/javascript" src="{{ asset('plugins/toastr/build/toastr.min.js') }}"></script>
<script type="text/javascript">
	toastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-top-right",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}
	@if(Session::has('toast-alert-success'))
		toastr.success("{{ Session::get('toast-alert-success') }}");
	@elseif(Session::has('toast-alert-info'))
		toastr.info("{{ Session::get('toast-alert-info') }}");
	@elseif(Session::has('toast-alert-warning'))
		toastr.warning("{{ Session::get('toast-alert-warning') }}");
	@elseif(Session::has('toast-alert-danger'))
		toastr.error("{{ Session::get('toast-alert-danger') }}");
	@endif
</script>