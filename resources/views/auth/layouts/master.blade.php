
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" type="image/x-icon" sizes="16x16" href="{{ asset('favicon.ico') }}">

    <title>@yield('title')</title>
    <!-- CSS -->
    <link href="{{ asset('admin/css/googleapis/google.css?family=Montserrat:200,300,400,500,600%7CRoboto:400') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/feather-icons/feather.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/css/perfect-scrollbar.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/nprogress/nprogress.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/font-awesome/css/animated.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/css/custom.css') }}" rel="stylesheet" type="text/css">
    <!-- <link rel="stylesheet" href="{{ asset('admin/css/pace.css') }}"> -->
    <script src="{{ asset('admin/js/modernizr.min.js') }}"></script>
</head>

<body class="body-bg-full profile-page" id="body">
    @yield('content')
</body>
<!-- Scripts -->
<script type="text/javascript" src="{{ asset('admin/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('admin/js/pace.min.js') }}"></script> -->
<script type="text/javascript" src="{{ asset('admin/js/perfect-scrollbar.jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/design.js') }}"></script>
<script type="text/javascript" src="{{asset('plugins/nprogress/jquery.pjax.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/nprogress/nprogress.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        if ($.support.pjax) {
            $.pjax.defaults.timeout = 2000; // time in milliseconds
            $(document).pjax('a', '#body');
            $(document).on('submit', 'form[data-pjax]', function(event) {
                $.pjax.submit(event, '#body');
                $("button[type='submit']", this).prepend('<i class="fa fa-circle-o-notch faa-spin faa-fast animated"></i>&nbsp;');
                $("button[type='submit']", this).attr('disabled', 'disabled');
            });
            $(document).on('pjax:start', function() { NProgress.start(); });
            $(document).on('pjax:end',   function() { NProgress.done();  });
        }
    });
    /*
        sample 1
        $(document).on('ready pjax:success', function() {
            // will fire on initial page load, and subsequent PJAX page loads
        });
        sample 2
        $(document).ready(function () {

            function initilizePlugins() {

                $('.plugin1').plugin1({        
                    ...
                });
                $('.plugin2').plugin2({
                    ...
                });
                $('.plugin3').plugin3({
                    ...
                });
                //window.location.pathname
            };

            $(document).on('pjax:end', function() {
                initializePlugins();
            });

        });

        sample 3 

        $.getScript( "ajax/test.js", function( data, textStatus, jqxhr ) {
              console.log( data ); // Data returned
              console.log( textStatus ); // Success
              console.log( jqxhr.status ); // 200
              console.log( "Load was performed." );
            });
    */
</script>
</html>