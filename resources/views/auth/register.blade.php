@extends('auth.layouts.master')
@section('title','Travel South')
@section('content')
<div id="wrapper" class="row wrapper">
    <div class="row container-min-full-height">
        <div class="col-lg-6 p-3 login-left">
            <div class="w-50">
                <h2 class="mb-4 text-center">Sign Up</h2>
                <form class="text-center" method="POST" action="{{ route('register') }}" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="text-muted" for="firstname">First Name</label>
                        <input type="text" required class="form-control form-control-line {{$errors->has('firstname') ? 'is-invalid' : ''}}" id="firstname" autofocus="" name="firstname" value="{{ old('firstname') }}">
                        @if ($errors->has('firstname'))
                            <span class="help-block">
                                {{ $errors->first('firstname') }}
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="text-muted" for="lastname">Last Name</label>
                        <input type="text" required class="form-control form-control-line {{$errors->has('lastname') ? 'is-invalid' : ''}}" id="lastname" name="lastname" value="{{ old('lastname') }}">
                        @if ($errors->has('lastname'))
                            <span class="help-block">
                                {{ $errors->first('lastname') }}
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="text-muted" for="email">Email</label>
                        <input type="email" required="" class="form-control form-control-line {{$errors->has('email') ? 'is-invalid' : ''}}" id="email" name="email"  value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
                        <label class="text-muted" for="password">Password</label>
                        <input type="password" required="" class="form-control form-control-line" id="password" name="password" >
                        @if ($errors->has('password'))
                            <span class="help-block">
                                {{ $errors->first('password') }}
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="text-muted" for="password_confirmation">Confirm Password</label>
                        <input type="password" class="form-control form-control-line" id="password_confirmation" name="password_confirmation">
                    </div>
                    <div class="form-group mr-b-20">
                        <button class="btn btn-block btn-rounded btn-md btn-primary text-uppercase fw-600 ripple" type="submit">
                            Sign Up
                        </button>
                    </div>
                </form>
                <!-- /form -->
            </div>
            <!-- /.w-75 -->
        </div>
        <div class="col-lg-6 login-right d-lg-flex d-none pos-fixed pos-right text-inverse container-min-full-height" style="background-image: url({{ asset('admin/images/defaults/background/bg.jpg') }})">
            <div class="login-content px-3 w-75 text-center">
                <h2 class="mb-4 text-center fw-300">Already Registered?</h2>
                <p class="heading-font-family fw-300 letter-spacing-minus pd-b-30">Welcome to Travel South</p>
                <a href="{{ route('login') }}" class="btn btn-rounded btn-md btn-outline-inverse text-uppercase fw-600 ripple pd-lr-60 mr-t-200">Sign In</a>
                <ul class="list-inline mt-4 heading-font-family text-uppercase fs-13 mr-t-20">
                    <li class="list-inline-item"><a href="{{ url('/') }}">Home</a>
                    </li>
                    <li class="list-inline-item"><a href="{{ route('client.about') }}">About</a>
                    </li>
                    <li class="list-inline-item"><a href="{{ route('client.contact') }}">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.login-content -->
        </div>
        <!-- /.login-right -->
    </div>
    <!-- /.row -->
    </div>
@endsection
