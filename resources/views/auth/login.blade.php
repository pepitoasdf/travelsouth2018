@extends('auth.layouts.master')
@section('title','Travel South')
@section('content')
<div id="wrapper" class="wrapper">
    <div class="row container-min-full-height">
        <div class="col-lg-6 p-3 login-left">
            <div class="w-50">
                <h2 class="mb-4 text-center">Sign In</h2>
                <form class="text-center"  method="POST" action="{{ route('login') }}" autocomplete="off" >
                    {{ csrf_field() }}
                    @if($errors->has('email'))
                        <div class="alert alert-icon alert-danger border-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <i class="material-icons list-icon">not_interested</i>
                            <strong>Oh snap!</strong> {{ $errors->first('email') }}
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="text-muted" for="email">Email</label>
                        <input type="email"  class="form-control form-control-line" id="email" name="email" autofocus="" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <label class="text-muted" for="password">Password</label>
                        <input type="password"  class="form-control form-control-line" id="password" name="password" >
                    </div>
                    <div class="form-group no-gutters mb-5 text-center">
                        <div class="checkbox checkbox-primary bw-3 mail-select-checkbox">
                            <label class="checkbox-checked">
                                <input type="checkbox"  name="remember" {{ old('remember') ? 'checked' : '' }}/>
                                <span class="label-text">Remember Me</span>
                            </label>
                        </div>

                        <a href="{{ route('password.request') }}" id="to-recover" class="text-muted fw-700 text-uppercase heading-font-family fs-12">Forgot Password?</a>
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group mr-b-20">
                        <button class="btn btn-block btn-rounded btn-md btn-primary text-uppercase fw-600 ripple" type="submit">Sign In</button>
                    </div>
                </form>
                <!-- /form -->
            </div>
            <!-- /.w-75 -->
        </div>
        <div class="col-lg-6 login-right d-lg-flex d-none pos-fixed pos-right text-inverse container-min-full-height" style="background-image: url({{ asset('admin/images/defaults/background/bg.jpg') }})">
            <div class="login-content px-3 w-75 text-center">
                <h2 class="mb-4 text-center fw-300">New here?</h2>
                <p class="heading-font-family fw-300 letter-spacing-minus">Welcome to Travel South</p>
                <a href="{{ route('register') }}" class="btn btn-rounded btn-md btn-outline-inverse text-uppercase fw-600 ripple pd-lr-60 mr-t-200">Sign Up</a>
                <ul class="list-inline mt-4 heading-font-family text-uppercase fs-13 mr-t-20">
                    <li class="list-inline-item"><a href="{{ url('/') }}">Home</a>
                    </li>
                    <li class="list-inline-item"><a href="{{ route('client.about') }}">About</a>
                    </li>
                    <li class="list-inline-item"><a href="{{ route('client.contact') }}">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.login-content -->
        </div>
        <!-- /.login-right -->
    </div>
    <!-- /.row -->
</div>
@endsection
