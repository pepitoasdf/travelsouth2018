@extends('admin.layouts.master')

@section('title', 'Users')

@section('header-title')
   <i class="list-icon feather feather-users"></i> Users
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">List</h5>
                        <div class="widget-actions">
                            @include('partials.add-list',['entity' => 'users','reference' => 'Users'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="data-table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Created At</th>
                                    @can('edit_users', 'delete_users')
                                    <th class="text-center">Actions</th>
                                    @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($result as $item)
                                    <tr>
                                        <td>{{ $item->firstname }}  {{ $item->lastname }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>
                                            {{-- $item->roles->implode('name', ', ') --}}
                                            @foreach($item->roles as $role)
                                                <span class="badge bg-success ">{{ $role->name }}</span>
                                            @endforeach
                                        </td>
                                        <td>{{ $item->created_at }}</td>

                                        @can('edit_users')
                                        <td class="text-center">
                                           @include('partials.actions', [
                                                'entity' => 'users',
                                                'id' => $item->id
                                            ])
                                        </td>
                                        @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $result->links() }}
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection