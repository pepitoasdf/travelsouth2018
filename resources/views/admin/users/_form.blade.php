{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="firstname">First Name :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('firstname') ? ' is-invalid' : '' }}" id="firstname" placeholder="" type="text" placeholder="First Name" required name="firstname" value="{{ isset($user) ? $user->firstname : old('firstname') }}">
        @if ($errors->has('firstname'))
            <span class="help-block">
                <strong>{{ $errors->first('firstname') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="lastname">Last Name :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('lastname') ? ' is-invalid' : '' }}" id="lastname" placeholder="" type="text" placeholder="Last Name" required name="lastname" value="{{ isset($user) ? $user->lastname : old('lastname') }}">
        @if ($errors->has('lastname'))
            <span class="help-block">
                <strong>{{ $errors->first('lastname') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="email">Email :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" placeholder="" type="email" placeholder="Email" required name="email" value="{{ isset($user) ? $user->email : old('email') }}">
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>

@if(isset($user))
    <div class="form-group row">
        <label class="col-md-3 col-form-label text-right" for="oldpassword">Old Password :</label>
        <div class="col-md-7">
            <input class="form-control form {{ $errors->has('oldpassword') ? ' is-invalid' : '' }}" id="oldpassword" placeholder="" type="password" placeholder="oldpassword" required name="oldpassword" value="">
            @if ($errors->has('oldpassword'))
                <span class="help-block">
                    <strong>{{ $errors->first('oldpassword') }}</strong>
                </span>
            @endif
        </div>
    </div>
@endif
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="password">Password :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" placeholder="" type="password" placeholder="Password" required name="password" >
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="password_confirmation">Confirm Password :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password_confirmation" placeholder="" type="password_confirmation" placeholder="password_confirmation" required name="password_confirmation">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="password_confirmation">Roles :</label>
    <div class="col-md-7">
        <select class="form-control  {{ $errors->has('roles') ? ' is-invalid' : '' }} form-control-sm" id="roles" multiple="true" name="roles[]">
            @if(isset($user))
                @foreach($roles as $role)
                    <option value="{{$role->id}}" {{in_array($role->id,$user_role) ? 'selected' : ''}}>{{$role->name}}</option>
                @endforeach
            @else
                @foreach($roles as $role)
                    <option value="{{$role->id}}">{{$role->name}}</option>
                @endforeach
            @endif
        </select>
        @if ($errors->has('roles'))
            <span class="help-block">
                <strong>{{ $errors->first('roles') }}</strong>
            </span>
        @endif
    </div>
</div>

