@extends('admin.layouts.master')

@section('title', 'Users')

@section('header-title')
   <i class="list-icon feather feather-users"></i> Users
@endsection

@section('content')
<div class="widget-list">
    <div class="row">
        <div class="widget-holder col-md-12">
            <div class="widget-bg">
                <div class="widget-heading widget-heading-border">
                    <h5 class="widget-title">Create</h5>
                    <div class="widget-actions">
                        @include('partials.add-list',['entity' => 'users','reference' => 'Users'])
                    </div>
                    <!-- /.widget-actions -->
                </div>
                <!-- /.widget-heading -->
                <div class="widget-body">
                    <form class="form-horizontal" data-pjax action="{{route('users.store')}}" method="POST" autocomplete="off">
                        @include('admin.users._form')
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-right" ></label>
                            <div class="col-md-5">
                                @can('add_users')
                                    <button type="submit" name="save" value="save" class="btn btn-success btn-rounded btn-sm"> Submit</button>
                                @endcan
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.widget-body -->
            </div>
            <!-- /.widget-bg -->
        </div>
        <!-- /.widget-holder -->
    </div>
    <!-- /.row -->
</div>
@endsection