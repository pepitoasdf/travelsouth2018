<ul class="nav nav-tabs">
    <li class="nav-item">
    	<a class="nav-link {{ Request::is('admin/booking/bookings*') ? 'active' : '' }}" href="{{ route('bookings.index') }}" aria-expanded="true">
    		Booking Charges
    	</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/booking/requests*') ? 'active' : '' }}" href="{{ route('requests.index') }}" aria-expanded="true">
            Booking Requests
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/booking/suggests*') ? 'active' : '' }}" href="{{ route('suggests.index') }}" aria-expanded="true">
            Booking Suggestions
        </a>
    </li>
</ul>