<h6 class="form-section">Customer Information</h6>
{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="name">Name :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" type="text" placeholder="Name" name="name" value="{{ isset($booking) ? $booking->name : old('name') }}" required="">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="email">Email :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" type="text" placeholder="email" name="email" value="{{ isset($booking) ? $booking->email : old('email') }}" required="">
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="phone">Phone :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('phone') ? ' is-invalid' : '' }}" id="phone" type="text" placeholder="phone" name="phone" value="{{ isset($booking) ? $booking->phone : old('phone') }}" required="">
        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="address">Address :</label>
    <div class="col-md-7">
        <textarea rows="2" class="form-control form {{ $errors->has('address') ? ' is-invalid' : '' }}" id="address"  placeholder="address" name="address">{{ isset($booking) ? $booking->address : old('address') }}</textarea>
        @if ($errors->has('address'))
            <span class="help-block">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>
</div>
<hr/>
<h6 class="form-section">Booking Information</h6>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="date">Date :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('date') ? ' is-invalid' : '' }} datepicker" id="date" type="text" placeholder="date" name="date" value="{{ isset($booking) ? $booking->date : old('date') }}" required="">
        @if ($errors->has('date'))
            <span class="help-block">
                <strong>{{ $errors->first('date') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="currency_id">Currency :</label>
    <div class="col-md-7">
        <select data-placeholder="Currency" data-toggle="select2" class="mr-b-5 form-control {{ $errors->has('currency_id') ? ' is-invalid' : '' }}" name="currency_id" id="currency_id" required="">
            <option value=""></option>
            @foreach($country as $currency)
                @if(isset($booking))
                    <option value="{{ $currency->id }}" {{ $booking->currency_id == $currency->id ? 'selected' : '' }}>
                        {{ $currency->name }}  ({{ $currency->currency_code }})
                    </option>
                @else
                    <option value="{{ $currency->id }}" {{ (old('currency_id') == $currency->id) ?  'selected' : '' }}>
                        {{ $currency->name }}  ({{ $currency->currency_code }})
                    </option>
                @endif
            @endforeach
        </select>
        @if ($errors->has('currency_id'))
            <span class="help-block">
                <strong>{{ $errors->first('currency_id') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="remarks">Remarks :</label>
    <div class="col-md-7">
        <textarea rows="2" class="form-control form {{ $errors->has('remarks') ? ' is-invalid' : '' }}" id="remarks"  placeholder="remarks" name="remarks">{{ isset($booking) ? $booking->remarks : old('remarks') }}</textarea>
        @if ($errors->has('remarks'))
            <span class="help-block">
                <strong>{{ $errors->first('remarks') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="service_type">Service Type :</label>
    <div class="col-md-7">
        <select data-placeholder="Service Type" data-toggle="select2" class="mr-b-5 form-control {{ $errors->has('service_type') ? ' is-invalid' : '' }}" name="service_type" id="service_type" required="">
            <option value="">--Select--</option>
            @if(isset($booking))
                <option value="Hotel" {{ ($booking->service_type =="Hotel") ?  'selected' : '' }}>Hotel</option>
                <option value="Package" {{ ($booking->service_type == "Package") ?  'selected' : '' }}>Package</option>
                <option value="Vehicle" {{ ($booking->service_type == "Vehicle") ?  'selected' : '' }}>Vehicle</option>
            @else
                <option value="Hotel" {{ (old('service_type') == "Hotel") ?  'selected' : '' }}>Hotel</option>
                <option value="Package" {{ (old('service_type') == "Package") ?  'selected' : '' }}>Package</option>
                <option value="Vehicle" {{ (old('service_type') == "Vehicle") ?  'selected' : '' }}>Vehicle</option>
            @endif
        </select>
        @if ($errors->has('service_type'))
            <span class="help-block">
                <strong>{{ $errors->first('service_type') }}</strong>
            </span>
        @endif
    </div>
</div>
<hr/>
<div class="table-responsive div-hotel-rooms" style="display: none;">
    <table class="table table-striped table-hover hotel-room-table" id="data-table">
        <thead>
            <tr>
                <th width="25%">Hotels</th>
                <th width="25%">Rooms</th>
                <th width="8%">Duration (Days)</th>
                <th width="12%">Start Date</th>
                <th width="12%">End Date</th>
                <th width="15%">Amount</th>
                <th width="3%"></th>
            </tr>
            <tr>
                <th>
                    <select id="hotel_id" data-placeholder="Hotel" class="form-control hotel_id" >
                        <option value=""></option>
                        @foreach(App\Hotel::all() as $hotel)
                            <option value="{{ $hotel->id }}">{{ $hotel->name }}</option>
                        @endforeach
                    </select>
                </th>
                <th>
                    <select id="room_id" data-placeholder="Room" class="form-control room_id" >
                    </select>
                </th>
                <th>
                    <input type="text" class="form-control duration" onkeypress="return isNumberKey(event, this);">
                </th>
                <th>
                    <input type="text" class="form-control start_date datepicker">
                </th>
                <th>
                    <input type="text" class="form-control end_date datepicker">
                </th>
                <th>
                    <input type="text" class="form-control room_rate" id="room_rate" onkeypress="return isNumberKey(event, this);"/>
                </th>
                <th>
                    <a href="#" class="btn btn-success btn-rounded btn-sm add-hotel-room">
                        <i class="list-icon feather feather-plus"></i>
                    </a>
                </th>
            </tr>
        </thead>
        <tbody class="hotelsrooms">
            @if(isset($booking))
                
            @endif
        </tbody>
        <tfoot class="hotelsrooms-foot">
            <tr>
                <td colspan="5" style="text-align: right;font-weight: bold">Total : </td>
                <td style="font-weight: bold;" class="totalRate">0.00</td>
            </tr>
        </tfoot>
    </table>
</div>
<div class="table-responsive div-packages" style="display: none;">
    <table class="table table-striped table-hover package-table" id="data-table">
        <thead>
            <tr>
                <th width="30%">Packages</th>
                <th width="10%">Duration (Days)</th>
                <th width="15%">Start Date</th>
                <th width="15%">End Date</th>
                <th width="17%">Amount</th>
                <th width="3%"></th>
            </tr>
            <tr>
                <th>
                    <select id="package_id" data-placeholder="Package" class="form-control package_id" >
                        <option value=""></option>
                        @foreach(App\Package::all() as $package)
                            <option value="{{ $package->id }}" data-rate="{{ $package->getrate->rate }}">{{ $package->name }}</option>
                        @endforeach
                    </select>
                </th>
                <th>
                    <input type="text" class="form-control duration2 " onkeypress="return isNumberKey(event, this);">
                </th>
                <th>
                    <input type="text" class="form-control start_date2 datepicker">
                </th>
                <th>
                    <input type="text" class="form-control end_date2 datepicker">
                </th>
                <th>
                    <input type="text" class="form-control amount2" id="amount2" onkeypress="return isNumberKey(event, this);"/>
                </th>
                <th>
                    <a href="#" class="btn btn-success btn-rounded btn-sm add-package">
                        <i class="list-icon feather feather-plus"></i>
                    </a>
                </th>
            </tr>
        </thead>
        <tbody class="packages-body">
            @if(isset($booking))
                
            @endif
        </tbody>
        <tfoot class="packages-foot">
            <tr>
                <td colspan="5" style="text-align: right;font-weight: bold">Total : </td>
                <td style="font-weight: bold;" class="totalRate-package">0.00</td>
            </tr>
        </tfoot>
    </table>
</div>
<div class="table-responsive div-vehicles" style="display: none;">
    <table class="table table-striped table-hover vehicle-table" id="data-table">
        <thead>
            <tr>
                <th width="20%">Vehicles</th>
                <th width="15%">Rate Type</th>
                <th width="10%%">Duration (Days)</th>
                <th width="15%">Start Date</th>
                <th width="15%">End Date</th>
                <th width="17%">Amount</th>
                <th width="3%"></th>
            </tr>
            <tr>
                <th>
                    <select id="vehicle_id" data-placeholder="Vehicle" class="form-control vehicle_id" >
                        <option value=""></option>
                        @foreach(App\Vehicle::all() as $vehicle)
                            <option value="{{ $vehicle->id }}" data-ratehour="{{ $vehicle->getrate->rate_hour }}" data-rateday="{{ $vehicle->getrate->rate_hour }}">{{ $vehicle->name }}</option>
                        @endforeach
                    </select>
                </th>
                <th>
                    <select class="form-control rate_type3">
                        <option>--Select--</option>
                        <option value="hourly">Hourly</option>
                        <option value="daily">Daily</option>
                    </select>
                </th>
                <th>
                    <input type="text" class="form-control duration3 ">
                </th>
                <th>
                    <input type="text" class="form-control start_date3 datepicker">
                </th>
                <th>
                    <input type="text" class="form-control end_date3 datepicker">
                </th>
                <th>
                    <input type="text" class="form-control amount3" id="amount3" onkeypress="return isNumberKey(event, this);"/>
                </th>
                <th>
                    <a href="#" class="btn btn-success btn-rounded btn-sm add-vehicle">
                        <i class="list-icon feather feather-plus"></i>
                    </a>
                </th>
            </tr>
        </thead>
        <tbody class="vehicle-body">
            @if(isset($booking))
                
            @endif
        </tbody>
        <tfoot class="vehicle-foot">
            <tr>
                <td colspan="5" style="text-align: right;font-weight: bold">Total : </td>
                <td style="font-weight: bold;" class="totalRate-package">0.00</td>
            </tr>
        </tfoot>
    </table>
</div>

@push('append_js')

    <script type="text/javascript">
        $('#service_type').on('change',function(e){
            var getval = $(this).val();
            if(getval == 'Hotel'){
                $('.div-hotel-rooms').show();
                $('.div-packages').hide();
                $('.div-vahicles').hide();
            }
            else if(getval == 'Package'){
                $('.div-hotel-rooms').hide();
                $('.div-packages').show();
                $('.div-vahicles').hide();
            }
            else if(getval == 'Vehicle'){
                $('.div-hotel-rooms').hide();
                $('.div-packages').hide();
                $('.div-vahicles').show();
            }
        });
        $('.add-hotel-room').click(function(e){
            e.preventDefault();
            var hotel_id = $('.hotel_id').val();
            var hotel_text = $('.hotel_id option:selected').text();
            var room_id = $('.room_id').val();
            var room_text = $('.room_id option:selected').text();
            var room_rate = $('.room_rate').val();
            var duration = $('.duration').val();
            var start_date = $('.start_date').val();
            var end_date = $('.end_date').val();
            var c = 0;
            if(!hotel_id){
                alert('Hotel is required.');
                $('.hotel_id').focus();
                return false;
            }
            else if(!room_id){
                alert('Room is required.');
                $('.room_id').focus();
                return false;
            }
            else if(!room_rate){
                alert('Room rate is required.');
                $('.room_id').focus();
                return false;
            }
            else if(!duration){
                alert('Duration is required.');
                $('.duration').focus();
                return false;
            }
            else if(!room_rate){
                alert('Room rate is required.');
                $('.room_rate').focus();
                return false;
            }
            else if(!start_date){
                alert('Start Date rate is required.');
                $('.start_date').focus();
                return false;
            }
            else if(!end_date){
                alert('End Date rate is required.');
                $('.end_date').focus();
                return false;
            }

            $('.room_rate').val(parseFloat(room_rate).toFixed(2));

            $('table.table tbody.hotelsrooms').find('input[name="hotel_id1[]"]').each(function(){
                var h_id = $(this).val();
                var r_id = $(this).closest('tr').find('input[name="room_id1[]"]').val();
                if(h_id == hotel_id && room_id == r_id){
                    c++;
                }
            });
            if(c > 0){
                alert('Hotel and Room already added.');
                return false;
            }

            $('table.table tbody.hotelsrooms').append('\
                <tr>\
                    <td style="padding-left:20px">\
                        '+hotel_text+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+room_text+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+duration+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+start_date+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+end_date+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+currencyFormat(room_rate)+'\
                    </td>\
                    <td>\
                        <a href="#" class="btn btn-danger btn-rounded btn-sm remove-hotel-room">\
                        <i class="list-icon feather feather-x"></i>\
                        </a>\
                    </td>\
                    <input type="hidden" name="hotel_id1[]" value="'+hotel_id+'"/>\
                    <input type="hidden" name="room_id1[]" value="'+room_id+'"/>\
                    <input type="hidden" name="duration1[]" value="'+duration+'"/>\
                    <input type="hidden" name="start_date1[]" value="'+start_date+'"/>\
                    <input type="hidden" name="end_date1[]" value="'+end_date+'"/>\
                    <input type="hidden" name="amount1[]" class="amount_value" value="'+room_rate+'"/>\
                </tr>\
            ');
            getTotalRate('.hotelsrooms','.hotelsrooms-foot');
            
            $('.hotel_id').val('');
            $('.room_id').val('');
            $('.duration').val('');
            $('.start_date').val('');
            $('.end_date').val('');
            $('.room_rate').val('');
        });
        $('.add-package').click(function(e){
            e.preventDefault();
            var package_id = $('.package_id').val();
            var package_text = $('.package_id option:selected').text();
            var start_date = $('.start_date2').val();
            var end_date = $('.end_date2').val();
            var amount = $('.amount2').val();
            var duration = $('.duration2').val();
            var c = 0;
            if(!package_id){
                alert('Package is required.');
                $('.package_id').focus();
                return false;
            }
            else if(!start_date){
                alert('Start Date rate is required.');
                $('.start_date2').focus();
                return false;
            }
            else if(!end_date){
                alert('End Date rate is required.');
                $('.end_date2').focus();
                return false;
            }
            else if(!amount){
                alert('Amount is required.');
                $('.amount2').focus();
                return false;
            }
            else if(!duration){
                alert('Duration is required.');
                $('.amount2').focus();
                return false;
            }

            $('table.table tbody.packages-body').find('input[name="package_id2[]"]').each(function(){
                var h_id = $(this).val();
                if(h_id == package_id){
                    c++;
                }
            });
            if(c > 0){
                alert('Package already added.');
                return false;
            }

            $('table.table tbody.packages-body').append('\
                <tr>\
                    <td style="padding-left:20px">\
                        '+package_text+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+duration+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+start_date+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+end_date+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+currencyFormat(amount)+'\
                    </td>\
                    <td>\
                        <a href="#" class="btn btn-danger btn-rounded btn-sm remove-package">\
                        <i class="list-icon feather feather-x"></i>\
                        </a>\
                    </td>\
                    <input type="hidden" name="duration2[]" value="'+duration+'"/>\
                    <input type="hidden" name="package_id2[]" value="'+package_id+'"/>\
                    <input type="hidden" name="start_date2[]" value="'+start_date+'"/>\
                    <input type="hidden" name="end_date2[]" value="'+end_date+'"/>\
                    <input type="hidden" name="amount2[]" class="amount_value" value="'+amount+'"/>\
                </tr>\
            ');
            getTotalRate('.packages-body','.packages-foot');
            $('.package_id').val('');
            $('.duration2').val('');
            $('.start_date2').val('');
            $('.end_date2').val('');
            $('.amount2').val('');
        });
        $('.add-vehicle').click(function(e){
            e.preventDefault();
            var package_id = $('.vehicle_id').val();
            var vehicle_text = $('.vehicle_id option:selected').text();
            var start_date = $('.start_date3').val();
            var end_date = $('.end_date3').val();
            var amount = $('.amount3').val();
            var duration = $('.duration3').val();
            var rate_type = $('.rate_type3').val();
            var rate_type_text = $('.rate_type3 option:selected').text();
            var c = 0;
            if(!package_id){
                alert('Vehicle is required.');
                $('.vehicle_id').focus();
                return false;
            }
            else if(!start_date){
                alert('Start Date rate is required.');
                $('.start_date3').focus();
                return false;
            }
            else if(!end_date){
                alert('End Date rate is required.');
                $('.end_date3').focus();
                return false;
            }
            else if(!amount){
                alert('Amount is required.');
                $('.amount3').focus();
                return false;
            }
            else if(!rate_type){
                alert('Rate Type is required.');
                $('.amount3').focus();
                return false;
            }
            else if(!duration){
                alert('Duration is required.');
                $('.amount3').focus();
                return false;
            }

            $('table.table tbody.vehicle-body').find('input[name="vehicle_id3[]"]').each(function(){
                var h_id = $(this).val();
                if(h_id == package_id){
                    c++;
                }
            });
            if(c > 0){
                alert('Vehicle already added.');
                return false;
            }

            $('table.table tbody.vehicle-body').append('\
                <tr>\
                    <td style="padding-left:20px">\
                        '+vehicle_text+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+rate_type_text+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+duration+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+start_date+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+end_date+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+currencyFormat(amount)+'\
                    </td>\
                    <td>\
                        <a href="#" class="btn btn-danger btn-rounded btn-sm remove-vehicle">\
                        <i class="list-icon feather feather-x"></i>\
                        </a>\
                    </td>\
                    <input type="hidden" name="duration3[]" value="'+duration+'"/>\
                    <input type="hidden" name="rate_type3[]" value="'+rate_type+'"/>\
                    <input type="hidden" name="package_id3[]" value="'+package_id+'"/>\
                    <input type="hidden" name="start_date3[]" value="'+start_date+'"/>\
                    <input type="hidden" name="end_date3[]" value="'+end_date+'"/>\
                    <input type="hidden" name="amount3[]" class="amount_value" value="'+amount+'"/>\
                </tr>\
            ');
            getTotalRate('.vehicle-body','.vehicle-foot');
            $('.vehicle_id').val('');
            $('.duration3').val('');
            $('.rate_type3').val('');
            $('.start_date3').val('');
            $('.end_date3').val('');
            $('.amount3').val('');
        });
        $('#hotel_id').change(function(e){
            var id = $(this).val();
            var url = '{{ route("hotelRooms.api") }}';
            $.getJSON(url,{id:id},function(data){
                $('#room_id').empty();
                $('#room_id').append('<option value="">Select</option>');
                $.each(data,function(index, value){
                    $('#room_id').append('<option value="'+value.id+'" data-rate="'+value.getrate.rate+'">'+value.type.name+'</option>');
                });
            });
        });
        $('#room_id').change(function(e){
            var rate = $('option:selected',this).data('rate');
            $('#room_rate').val(parseFloat(rate).toFixed(2));
        });
        $('#package_id').on('change',function(){
            var rate = $(this).data('rate');
            $('#amount2').val(parseFloat(rate).toFixed(2));
        });
        $('#rate_type3').on('change',function(){
            var ratehour = $(this).closest('tr').find('#vehicle_id').data('ratehour');
            var rateday = $(this).closest('tr').find('#vehicle_id').data('rateday');
            var rate_type = $(this).val();
            if(rate_type == 'hourly'){
                $('#amount3').val(parseFloat(ratehour).toFixed(2));
            }
            else if(rate_type == 'daily'){
                $('#amount3').val(parseFloat(rateday).toFixed(2));
            }
        });
        $(document).click('.vehicle-table','.remove-vehicle',function(e){
            e.preventDefault();
            $(this).closest('tr').remove();
        });
        $(document).click('.package-table','.remove-package',function(e){
            e.preventDefault();
            $(this).closest('tr').remove();
        });
        $(document).click('.hotel-room-table','.remove-hotel-room',function(e){
            e.preventDefault();
            $(this).closest('tr').remove();
        });
        function getTotalRate(classVarBody,classVarFoot){
            var rate = 0;

            $('table.table tbody'+classVar).find('.amount_value').each(function(){
                var rate2 = $(this).val();
                rate += parseFloat(rate2);
            });

            $(classVarFoot).find('.totalRate').html(parseFloat(rate).toFixed(2));
        }
        function currencyFormat(value){
            return (value + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        }
        function isNumberKey(evt,el) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            // Added to allow decimal, period, or delete
            if (charCode == 110 || charCode == 190 || charCode == 46) 
                return true;

            if (charCode > 31 && (charCode < 48 || charCode > 57)) 
                return false;

            return true;
        } // isNumberKey
    </script>

@endpush