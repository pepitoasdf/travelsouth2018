@extends('admin.layouts.master')

@section('title', 'Booking')

@section('header-title')
   <i class="list-icon feather feather-settings"></i> Booking
@endsection

@section('header-body')
    <span>Charges</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">List</h5>
                        <div class="widget-actions">
                            @include('partials.add-list',['entity' => 'bookings','reference' => 'Booking'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.booking.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="data-table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Service</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                            @can('edit_facilities', 'delete_facilities')
                                            <th class="text-center">Actions</th>
                                            @endcan
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($results as $item)
                                            <tr>
                                                <td>{{ $item->name }}</td>
                                                <td></td>
                                                <td>{{ $item->description }}</td>
                                                <td>
                                                    @if($item->status == "New")
                                                        <span class="badge bg-default">New</span>
                                                    @elseif($item->status == "Finalized")
                                                        <span class="badge bg-info">Finalized</span>
                                                    @elseif($item->status == "Approved")
                                                        <span class="badge bg-success">Approved</span>
                                                    @elseif($item->status == "Void")
                                                        <span class="badge bg-danger">Voided</span>
                                                    @endif
                                                </td>

                                                @can('edit_facilities', 'delete_facilities')
                                                <td class="text-center">
                                                   @include('partials.actions', [
                                                        'entity' => 'facilities',
                                                        'id' => $item->id
                                                    ])
                                                </td>
                                                @endcan
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{ $results->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection