@extends('admin.layouts.master')

@section('title', 'Edit Roles')

@section('header-title')
   <i class="list-icon feather feather-lock"></i> Roles
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">Edit</h5>
                        <div class="widget-actions">
                            @include('partials.add-list',['entity' => 'roles','reference' => 'Roles'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="accordion" id="accordion-roles" role="tablist" aria-multiselectable="true">
                            <div class="card card-outline-primary">
                                <div class="card-header" role="tab" id="heading{{$role->id}}">
                                    <h6 class="card-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion-roles" href="#collapse{{$role->id}}" aria-expanded="true" aria-controls="collapse{{$role->id}}">
                                            {{$role->name}}
                                        </a>
                                    </h6>
                                </div>
                                <!-- /.card-header -->
                                <div id="collapse{{$role->id}}" class="card-collapse collapse show" role="tabpanel" aria-labelledby="heading{{ $role->id }}">
                                    <div class="card-body">
                                        <form action="{{ route('roles.update',$role->id) }}" data-pjax method="POST" name="edit-role" id="edit-role">
                                            {{ method_field('PUT') }}
                                            {{csrf_field()}}
                                            <div class="row">
                                                 @foreach($menus as $menu)
                                                    <div class="col-sm-3">
                                                        <fieldset style="min-height: 200px">
                                                            <legend>
                                                                {{ucfirst($menu->name)}}
                                                            </legend>
                                                            <div class="form-layout">
                                                                <div class="row mg-b-5">
                                                                    @foreach($menu->crop() as $k => $perm)
                                                                        <?php
                                                                            $per_found = null;

                                                                            if( isset($role) ) {
                                                                                $per_found = $role->hasPermissionTo($perm->name);
                                                                            }

                                                                            if( isset($user)) {
                                                                                $per_found = $user->hasDirectPermission($perm->name);
                                                                            }
                                                                            $permname = explode('_', $perm->name)
                                                                        ?>
                                                                        <div class="col-sm-12">
                                                                            <div class="checkbox checkbox-primary bw-3 pd-t-0 pd-b-0">
                                                                                <label class="{{ str_contains($perm->name, 'delete') ? 'text-danger' : '' }} checkbox-checked">
                                                                                    <!-- <input type="checkbox" class="js-switch {{$permname[0]}}" data-color="#009efb" data-size="small" name="permissions[]" {{($per_found) ? 'checked' : ''}} value="{{$perm->name}}"/> -->
                                                                                    <input type="checkbox" class="{{$permname[0]}}" name="permissions[]" {{($per_found) ? 'checked' : ''}} value="{{$perm->name}}"/>
                                                                                    <span class="label-text">{{ ucfirst($permname[0]) }}</span> {{-- ucfirst($permname[1]) --}}
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <hr/>
                                                    @can('edit_roles')
                                                        <button type="submit" class="pull-right btn btn-success btn-rounded btn-sm" form="edit-role">
                                                            Update
                                                        </button>
                                                    @endcan
                                                </div>
                                            </div>
                                            </form>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card-collapse -->
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection