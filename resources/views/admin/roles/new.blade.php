@extends('admin.layouts.master')

@section('title', 'Roles')

@section('header-title')
   <i class="list-icon feather feather-lock"></i> Roles
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">Create</h5>
                        <div class="widget-actions">
                            @include('partials.add-list',['entity' => 'roles','reference' => 'Roles'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <form class="form-horizontal" data-pjax action="{{route('roles.store')}}" method="POST" autocomplete="off">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label text-right" for="role-name">Role Name :</label>
                                <div class="col-md-7">
                                    <input class="form-control form" id="role-name" placeholder="" type="text" placeholder="Name" required name="name" value="{{ old('name') }}">
                                </div>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label text-right" for="role-name"></label>
                                <div class="col-md-7">
                                    @can('add_roles')
                                        <button type="submit" class="btn btn-success btn-rounded btn-sm ">
                                            Submit
                                        </button>
                                    @endcan
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection