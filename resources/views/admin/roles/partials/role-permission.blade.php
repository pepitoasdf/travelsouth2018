@foreach($menus as $menu)
    <div class="col-sm-3">
        <fieldset style="min-height: 200px">
            <legend>
                {{ucfirst($menu->name)}}
            </legend>
            <div class="form-layout">
                <div class="row">
                    @foreach($menu->crop() as $k => $perm)
                        <?php
                            $per_found = null;

                            if( isset($role) ) {
                                $per_found = $role->hasPermissionTo($perm->name);
                            }

                            if( isset($user)) {
                                $per_found = $user->hasDirectPermission($perm->name);
                            }
                            $permname = explode('_', $perm->name)
                        ?>
                        <div class="col-sm-12 ">
                            <div class="checkbox checkbox-primary bw-3 pd-t-0 pd-b-0">
                                <label class="{{ str_contains($perm->name, 'delete') ? 'text-danger' : '' }} checkbox-checked">
                                    <!-- <input type="checkbox" class="js-switch" data-color="#009efb" data-size="small" disabled="" {{($per_found) ? 'checked' : ''}} value="{{$perm->name}}" style="cursor: default;"/> -->
                                    <input type="checkbox" disabled="" {{($per_found) ? 'checked' : ''}} value="{{$perm->name}}" s/>
                                    <span class="label-text">{{ ucfirst($permname[0]) }}</span>
                                </label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </fieldset>
    </div>
@endforeach