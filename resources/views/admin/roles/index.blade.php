@extends('admin.layouts.master')

@section('title', 'Roles')

@section('header-title')
   <i class="list-icon feather feather-lock"></i> Roles
@endsection
@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">List</h5>
                        <div class="widget-actions">
                           @include('partials.add-list',['entity' => 'roles','reference' => 'Roles'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="accordion" id="accordion-roles" role="tablist" aria-multiselectable="true">
                            @foreach ($roles as $k => $role)
                                <div class="card card-outline-primary">
                                    <div class="card-header click-header" data-refid="{{ $role->id }}" role="tab" id="heading{{$role->id}}">
                                        <h6 class="card-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion-roles" href="#collapse{{$role->id}}" aria-expanded="true" aria-controls="collapse{{$role->id}}">
                                                {{$role->name}}
                                            </a>
                                        </h6>
                                    </div>
                                    <!-- /.card-header -->
                                    <div id="collapse{{$role->id}}" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $role->id }}">
                                        <div class="card-body">
                                            <div class="row display-results">
                                                <!-- display DOM html here -->
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <hr/>
                                                    @if($role->name != "Admin")
                                                        @can('edit_roles')
                                                            <a href="{{ route('roles.edit',$role->id) }}" class="pull-right btn btn-info btn-rounded btn-sm">
                                                                Edit Permission
                                                            </a>
                                                        @endcan
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card-collapse -->
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    @push('append_js')
        <script type="text/javascript">
            $('.click-header').on('click',function(){
                var id = $(this).data('refid');
                var url = "{{ route('getrole.api') }}";
                var that = this;
                if(id){
                    $.get(url, { id : id }, function($result){
                        $(that).closest('div.card-outline-primary').find('div.display-results').html($result);
                    });
                }
            });
        </script>
    @endpush
@endsection
