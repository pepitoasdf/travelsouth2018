@extends('admin.layouts.master')

@section('title', 'Hotels - Gallery')

@section('header-title')
   <i class="list-icon feather feather-command"></i> Services
@endsection

@section('header-body')
    <span>Hotels</span>
    <i class="feather feather-chevron-right"></i>
    <span> Gallery</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">Create <span class="text-success">( {{ strtoupper($hotel->name) }} )</span></h5>
                        <div class="widget-actions">
                            @include('partials.add-list',['entity' => 'hgalleries', 'refid' => $hotel->id, 'reference' => 'Hotel Photos'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.hotels.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <form class="form-horizontal" data-pjax action="{{route('hgalleries.store',$hotel->id)}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <h6 class="form-section">Upload Images For Hotel</h6>
                                    @include('partials.photo')
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-right"></label>
                                        <div class="col-md-5">
                                            @can('add_hgalleries')
                                                <button type="submit" name="save" value="save" class="btn btn-success btn-rounded btn-sm"> Submit</button>
                                            @endcan
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection