@extends('admin.layouts.master')

@section('title', 'Hotels - Gallery')

@section('header-title')
   <i class="list-icon feather feather-command"></i> Services
@endsection

@section('header-body')
    <span>Hotels</span>
    <i class="feather feather-chevron-right"></i>
    <span> Rooms</span>
    <i class="feather feather-chevron-right"></i>
    <span> Gallery</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">List <span class="text-success">( {{ strtoupper($hotel->name) }} )</span></h5>
                        <div class="widget-actions">
                            <a href="{{ route('hgalleries.index',['id' => $hotel->id, 'hgallery' => $room->id]) }}" class="btn btn-default btn-rounded btn-sm"><i class="list-icon feather feather-list"></i> &nbsp;View Listing</a>

                            <a href="{{ route('hgalleries.roomCreate',['id' => $hotel->id, 'hgallery' => $room->id]) }}" class="btn btn-info btn-rounded btn-sm"><i class="list-icon feather feather-plus"></i> &nbsp;Add Room Photos</a>
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.hotels.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <h5 class="box-title">{{ $room->type->name }} Images</h5>
                                <div class="row lightbox-gallery" data-toggle="lightbox-gallery" data-type="image" data-effect="fadeInRight">
                                    @foreach($roomPhotos as $photo)
                                        <div id="lightbox-popup-gallery" class="col-md-4 lightbox">
                                            <h5 class="form-section mr-b-5 mr-t-30">{{ $photo->file_name }}</h5>
                                            <a href="{{ asset($photo->path.'/'.$photo->file_name) }}" title="{{ $photo->file_name }}">
                                                <img src="{{ asset($photo->path.'/'.$photo->file_name) }}" alt="Thumb 1">
                                            </a>
                                        </div>
                                        <!-- /.col-md-4 -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    @push('append_css')
        <link href="{{ asset('plugins/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" >
    @endpush
    @push('append_js')
        <script src="{{ asset('plugins/magnific-popup/popper.min.js') }}"></script>
        <script src="{{ asset('plugins/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    @endpush
@endsection