@extends('admin.layouts.master')

@section('title', 'Hotels - Rooms')

@section('header-title')
   <i class="list-icon feather feather-command"></i> Services
@endsection

@section('header-body')
    <span>Hotels</span>
    <i class="feather feather-chevron-right"></i>
    <span> Rooms</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">List <span  class="text-success">( {{  strtoupper($hotel->name) }} )</span></h5>
                        <div class="widget-actions">
                            @include('partials.add-list',['entity' => 'rooms', 'refid' => $hotel->id, 'reference' => 'Rooms'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.hotels.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="data-table">
                                        <thead>
                                        <tr>
                                            <th>Room Type</th>
                                            <th>No. Of Rooms</th>
                                            <th>Status</th>
                                            @can('edit_rooms', 'delete_rooms')
                                            <th class="text-center">Actions</th>
                                            @endcan
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($results as $item)
                                            <tr>
                                                <td>{{ $item->type->name }}</td>
                                                <td>{{ number_format($item->room_no,2) }}</td>
                                                <td>
                                                    @if($item->status == "Draft")
                                                        <span class="badge bg-info ">{{ $item->status }}</span>
                                                    @elseif($item->status == "Published")
                                                        <span class="badge bg-success ">{{ $item->status }}</span>
                                                    @elseif($item->status == "Pending")
                                                        <span class="badge bg-warning ">{{ $item->status }}</span>
                                                    @endif
                                                </td>
                                                @can('edit_rooms','delete_rooms')
                                                <td class="text-center">
                                                   @include('partials.actions', [
                                                        'entity' => 'rooms',
                                                        'ref_id' =>$hotel->id,
                                                        'id' => $item->id
                                                    ])
                                                </td>
                                                @endcan
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{ $results->links() }}
                            </div>
                            <!-- /.tab-content -->
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection