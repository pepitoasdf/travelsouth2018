{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="room_type_id">Room Type :</label>
    <div class="col-md-7">
        <select required="" data-placeholder="Room Type" data-toggle="select2" class="form-control {{ $errors->has('room_type_id') ? ' is-invalid' : '' }}" name="room_type_id">
            <option value=""></option>
            @foreach(App\HotelRoomType::all() as $type)
                @if(isset($room->type))
                    <option value="{{ $type->id }}" {{ ($room->room_type_id == $type->id) ? 'selected' : '' }}>{{ $type->name }}</option>
                @else
                    <option value="{{ $type->id }}" {{ (old('room_type_id') == $type->id) ? 'selected' : ''  }}>{{ $type->name }}</option>
                @endif
            @endforeach
        </select>
        @if ($errors->has('room_type_id'))
            <span class="help-block">
                <strong>{{ $errors->first('room_type_id') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Description :</label>
    <div class="col-md-7">
        <textarea rows="4" required="" class="form-control form {{ $errors->has('description') ? ' is-invalid' : '' }}" id="description" placeholder="description" name="description">{{ isset($room) ? $room->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="pricing">Pricing :</label>
    <div class="col-md-7">
        <input required="" class="form-control form {{ $errors->has('pricing') ? ' is-invalid' : '' }}" id="pricing" type="text" placeholder="Pricing" name="pricing" value="{{ isset($room) ? $room->pricing : old('pricing') }}">
        @if ($errors->has('pricing'))
            <span class="help-block">
                <strong>{{ $errors->first('pricing') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="room_no">No. Of Rooms :</label>
    <div class="col-md-7">
        <input required="" class="form-control form {{ $errors->has('room_no') ? ' is-invalid' : '' }}" id="room_no" type="text" placeholder="No. Of Rooms" name="room_no" value="{{ isset($room) ? $room->room_no : old('room_no') }}">
        @if ($errors->has('room_no'))
            <span class="help-block">
                <strong>{{ $errors->first('room_no') }}</strong>
            </span>
        @endif
    </div>
</div>

<hr>
<h6 class="form-section">Rates</h6>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="rate">Rate:</label>
    <div class="col-md-7">
        <input required="" class="form-control form {{ $errors->has('rate') ? ' is-invalid' : '' }}" id="rate" type="number" placeholder="0.00" name="rate" value="{{ isset($room->getrate) ? $room->getrate->rate : old('rate') }}">
        @if ($errors->has('rate'))
            <span class="help-block">
                <strong>{{ $errors->first('rate') }}</strong>
            </span>
        @endif
    </div>
    @if(isset($room->getrate))
        <input type="hidden" name="rate_id" value="{{ $room->getrate->id }}">
    @endif
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="effective_date">Effective Date:</label>
    <div class="col-md-7">
        <input required="" class="datepicker form-control form {{ $errors->has('effective_date') ? ' is-invalid' : '' }}" id="effective_date" type="text" name="effective_date" value="{{ isset($room->getrate) ? $room->getrate->effective_date : old('effective_date') }}">
        @if ($errors->has('effective_date'))
            <span class="help-block">
                <strong>{{ $errors->first('effective_date') }}</strong>
            </span>
        @endif
    </div>
</div>
@include('partials.status', ['data' => (isset($room) ? $room : null)])
<hr>
<h6 class="form-section">Upload Images</h6>
@include('partials.photo')