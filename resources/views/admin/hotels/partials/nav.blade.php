<ul class="nav nav-tabs">
    @can('edit_hotels')
        <li class="nav-item ">
            <a class="nav-link {{ Request::is('admin/services/hotels/'.$hotel->id.'/edit*') ? 'active' : '' }}" href="{{route('hotels.edit',$hotel->id)}}" aria-expanded="true">
                Profile
            </a>
        </li>
    @endcan
    @can('view_rooms')
        <li class="nav-item">
            <a class="nav-link {{ Request::is('admin/services/hotels/'.$hotel->id.'/rooms*') ? 'active' : '' }}" href="{{ route('rooms.index',$hotel->id) }}" aria-expanded="true">Room Type</a>
        </li>
    @endcan
    @can('view_hgalleries')
        <li class="nav-item">
            <a class="nav-link {{ Request::is('admin/services/hotels/'.$hotel->id.'/hgalleries*') ? 'active' : '' }}" href="{{ route('hgalleries.index',$hotel->id) }}" aria-expanded="true">Gallery</a>
        </li>
    @endcan
    @can('view_roomrates')
        <li class="nav-item">
            <a class="nav-link {{ Request::is('admin/services/hotels/'.$hotel->id.'/roomrates*') ? 'active' : '' }}" href="{{ route('roomrates.index',$hotel->id) }}" aria-expanded="true">Rates</a>
        </li>
    @endcan
    @can('view_hvideos')
        <li class="nav-item">
            <a class="nav-link {{ Request::is('admin/services/hotels/'.$hotel->id.'/hvideos*') ? 'active' : '' }}" href="{{ route('hvideos.index',$hotel->id) }}" aria-expanded="true">Videos</a>
        </li>
    @endcan
</ul>