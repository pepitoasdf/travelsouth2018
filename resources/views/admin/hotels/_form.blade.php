{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="name">Name :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" type="text" placeholder="Name" name="name" value="{{ isset($hotel) ? $hotel->name : old('name') }}" required="">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Description :</label>
    <div class="col-md-7">
        <textarea rows="8" class="form-control form {{ $errors->has('description') ? ' is-invalid' : '' }}" id="description"  placeholder="description" name="description">{{ isset($hotel) ? $hotel->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="pricing">Pricing :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('pricing') ? ' is-invalid' : '' }}" id="pricing" type="text" placeholder="pricing" name="pricing" value="{{ isset($hotel) ? $hotel->pricing : old('pricing') }}" required="">
        @if ($errors->has('pricing'))
            <span class="help-block">
                <strong>{{ $errors->first('pricing') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right ">Address :</label>
    <div class="col-md-7">
        <br/>
        <label class="col-inline" for="country_id"> Countries </label>
        <select data-placeholder="Country" data-toggle="select2" class="mr-b-5 form-control {{ $errors->has('country') ? ' is-invalid' : '' }}" name="country_id" id="country_id" required="">
            <option value=""></option>
            @foreach(App\Country::all() as $country)
                @if(isset($hotel))
                    <option value="{{ $country->id }}" {{ $hotel->country_id == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                @else
                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                @endif
            @endforeach
        </select>
        @if ($errors->has('country_id'))
            <span class="help-block">
                <strong>{{ $errors->first('country_id') }}</strong>
            </span>
        @endif

        <br/>
        <label class="col-inline" for="province_id"> States/Provinces </label>
        <select data-placeholder="States/Provinces" class="mr-b-5 form-control {{ $errors->has('province_id') ? ' is-invalid' : '' }} select-province" name="province_id" id="province_id" required="">
            @if(isset($hotel))
                <option value="{{ $hotel->province_id }}">{{ $hotel->province->name }}</option>
            @endif
        </select>
        @if ($errors->has('province_id'))
            <span class="help-block">
                <strong>{{ $errors->first('province_id') }}</strong>
            </span>
        @endif

        <br/>
        <label class="col-inline" for="city_id"> Cities/Municipalities</label>
        <select data-placeholder="Cities/Municipalities" class="mr-b-5 form-control {{ $errors->has('city') ? ' is-invalid' : '' }} select-city" name="city_id" id="city_id">
            @if(isset($hotel->city))
                <option value="{{ $hotel->city_id }}">{{ $hotel->city->name }}</option>
            @endif
        </select>
        @if ($errors->has('city_id'))
            <span class="help-block">
                <strong>{{ $errors->first('city_id') }}</strong>
            </span>
        @endif

        <br/>
        <label class="col-inline" for="locality"> Locality</label>
        <input class="form-control form {{ $errors->has('locality') ? ' is-invalid' : '' }}" id="locality" type="text" placeholder="Street, Barangay" name="locality" value="{{ isset($hotel) ? $hotel->locality : old('locality') }}">
        @if ($errors->has('country'))
            <span class="help-block">
                <strong>{{ $errors->first('country') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="zip_code">Zip Code :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('zip_code') ? ' is-invalid' : '' }}" id="zip_code" type="text" placeholder="Zip Code" name="zip_code" value="{{ isset($hotel) ? $hotel->zip_code : old('zip_code') }}">
        @if ($errors->has('zip_code'))
            <span class="help-block">
                <strong>{{ $errors->first('zip_code') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="type_id">Type :</label>
    <div class="col-md-7">
        <select data-placeholder="Types" data-toggle="select2" class="form-control {{ $errors->has('type_id') ? ' is-invalid' : '' }}" name="type_id">
            <option value=""></option>
            @foreach(App\HotelType::all() as $type)
                @if(isset($hotel->type))
                    <option value="{{ $type->id }}" {{ $hotel->type_id == $type->id ? 'selected' : '' }}>{{ $type->name }}</option>
                @else
                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                @endif
            @endforeach
        </select>
        @if ($errors->has('type_id'))
            <span class="help-block">
                <strong>{{ $errors->first('type_id') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="facility_id">Facilities :</label>
    <div class="col-md-7">
        <?php
            if(isset($hotel)){
                $chars = $hotel->facilities()->pluck('facility_id')->toArray();
            }
        ?>
        <div class="pd-t-10 pd-b-10 custom-scroll-content scrollbar-enabled" style="overflow-y: scroll; max-height: 400px;">
            @foreach(App\HotelFacility::all() as $facility)
                @if(isset($hotel))
                    @if(in_array($facility->id, $chars))
                        <div class="checkbox checkbox-primary bw-3 mail-select-checkbox pd-t-0 pd-b-0">
                            <label class="checkbox-checked">
                                <input type="checkbox" checked="" name="facility_id[]" value="{{ $facility->id }}" />
                                <span class="label-text">{{ $facility->description }}</span>
                            </label>
                        </div>
                    @else
                        <div class="checkbox checkbox-primary bw-3 mail-select-checkbox pd-t-0 pd-b-0">
                            <label class="checkbox-checked">
                                <input type="checkbox" name="facility_id[]" value="{{ $facility->id }}" />
                                <span class="label-text">{{ $facility->description }}</span>
                            </label>
                        </div>
                    @endif
                @else
                    <div class="checkbox checkbox-primary bw-3 mail-select-checkbox pd-t-0 pd-b-0">
                        <label class="checkbox-checked">
                            <input type="checkbox" name="facility_id[]" value="{{ $facility->id }}" />
                            <span class="label-text">{{ $facility->description }}</span>
                        </label>
                    </div>
                @endif
            @endforeach
        </div>
        @if ($errors->has('facility_id'))
            <span class="help-block">
                <strong>{{ $errors->first('facility_id') }}</strong>
            </span>
        @endif
    </div>
</div>
@include('partials.status', ['data' => (isset($hotel) ? $hotel : null)])
<hr>
<h6 class="form-section">Upload Images</h6>
@include('partials.photo')

@push('append_js')
    <script type="text/javascript">
        $("select.select-province").select2({
            minimumInputLength : 1,
            allowClear: false,
            ajax : {
                url : "{{ route('provinces.api') }}",
                dataType : 'json',
                data : function (params) {
                    var country_id = $('#country_id').val();
                    return {
                        name: params.term,
                        country : country_id
                    };
                },
                processResults: function (data) {
                      return {
                        results: data
                    };
                },
                cache: true
            }
            ,
            escapeMarkup: function (markup) { return markup; },
            templateResult: function(repo) {
                return repo.text;
            },
            templateSelection: function(repo){
                return repo.text;
            }
        });

        $("select.select-city").select2({
            minimumInputLength : 1,
            allowClear: false,
            ajax : {
                url : "{{ route('cities.api') }}",
                dataType : 'json',
                data : function (params) {
                    var country_id = $('#country_id').val();
                    var province_id = $('#province_id').val();
                    return {
                        name: params.term,
                        country : country_id,
                        province : province_id
                    };
                },
                processResults: function (data) {
                      return {
                        results: data
                    };
                },
                cache: true
            }
            ,
            escapeMarkup: function (markup) { return markup; },
            templateResult: function(repo) {
                return repo.text;
            },
            templateSelection: function(repo){
                return repo.text;
            }
        });

    </script>
@endpush