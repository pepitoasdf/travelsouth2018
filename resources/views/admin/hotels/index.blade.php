@extends('admin.layouts.master')

@section('title', 'Hotels')

@section('header-title')
   <i class="list-icon feather feather-command"></i> Services
@endsection

@section('header-body')
    <span>Hotels</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">List</h5>
                        <div class="widget-actions">
                            @include('partials.add-list',['entity' => 'hotels','reference' => 'Hotels'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="data-table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    @can('edit_hotels', 'delete_hotels')
                                    <th class="text-center">Actions</th>
                                    @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($results as $item)
                                    <tr>
                                        <td>{{ $item->name }}</td>
                                        <td>
                                            {{ ($item->locality) ?  $item->locality.', ' : ''}} {{ ($item->city) ? $item->city->name.', ' : '' }} {{ ($item->province) ? $item->province->name.', ' : ''}} {{ isset($item->country->name) ? $item->country->name : '' }}
                                        </td>
                                        <td>{{ isset($item->type->name) ? $item->type->name : '' }}</td>
                                        <td>
                                            @if($item->status == "Draft")
                                                <span class="badge bg-info ">{{ $item->status }}</span>
                                            @elseif($item->status == "Published")
                                                <span class="badge bg-success ">{{ $item->status }}</span>
                                            @elseif($item->status == "Pending")
                                                <span class="badge bg-warning ">{{ $item->status }}</span>
                                            @endif
                                        </td>
                                        @can('edit_hotels','delete_hotels')
                                        <td class="text-center">
                                           @include('partials.actions', [
                                                'entity' => 'hotels',
                                                'id' => $item->id
                                            ])
                                        </td>
                                        @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $results->links() }}
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection