{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="title">Title :</label>
    <div class="col-md-7">
        <input required="" class="form-control form {{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" type="text" placeholder="title" name="title" value="{{ isset($video) ? $video->title : old('title') }}">
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Description :</label>
    <div class="col-md-7">
        <textarea rows="8" class="form-control form {{ $errors->has('description') ? ' is-invalid' : '' }}" required="" id="description" placeholder="description" name="description">{{ isset($video) ? $video->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="video_url">Video Url :</label>
    <div class="col-md-7">
        <input required="" class="form-control form {{ $errors->has('video_url') ? ' is-invalid' : '' }}" id="video_url" type="text" placeholder="video_url" name="video_url" value="{{ isset($video) ? $video->video_url : old('video_url') }}">
        @if ($errors->has('video_url'))
            <span class="help-block">
                <strong>{{ $errors->first('video_url') }}</strong>
            </span>
        @endif
    </div>
</div>
@include('partials.status', ['data' => (isset($video) ? $video : null)])
<hr>
<h6 class="form-section">Upload Images</h6>
@include('partials.photo')