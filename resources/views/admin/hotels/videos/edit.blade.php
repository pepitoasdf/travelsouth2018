@extends('admin.layouts.master')

@section('title', 'Hotels - Videos')

@section('header-title')
   <i class="list-icon feather feather-command"></i> Services
@endsection

@section('header-body')
    <span>Hotels</span>
    <i class="feather feather-chevron-right"></i>
    <span> Videos</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">Edit <span class="text-success">( {{  strtoupper($hotel->name) }} )</span></h5>
                        <div class="widget-actions">
                            @include('partials.add-list',['entity' => 'hvideos', 'refid' => $hotel->id,'reference' => 'Videos'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.hotels.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <form class="form-horizontal" data-pjax action="{{route('hvideos.update',['id' => $hotel->id, 'vvideo' => $video->id])}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                    {{ method_field('PATCH') }}
                                    @include('admin.hotels.videos._form')
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-right"></label>
                                        <div class="col-md-7">
                                            @can('add_hvideos')
                                                <button type="submit" name="save" value="save" class="btn btn-success btn-rounded btn-sm"> Update</button>
                                            @endcan
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection