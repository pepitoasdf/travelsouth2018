@extends('admin.layouts.master')

@section('title', 'Hotels - Rooms')

@section('header-title')
   <i class="list-icon feather feather-command"></i> Services
@endsection

@section('header-body')
    <span>Hotels</span>
    <i class="feather feather-chevron-right"></i>
    <span> Rooms</span>
    <i class="feather feather-chevron-right"></i>
    <span> Rates</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">Edit<span class="text-success">( {{  strtoupper($hotel->name) }} )</h5>
                        <div class="widget-actions">
                            <a href="{{ route('roomrates.index',['id' => $hotel->id]) }}" class="btn btn-default btn-rounded btn-sm"><i class="list-icon feather feather-list"></i> &nbsp;View Listing</a>
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.hotels.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <h6 class="form-section">Room - {{ strtoupper($room->name) }}</h6>
                                <form class="form-horizontal" data-pjax action="{{route('roomrates.update',['id' => $hotel->id, 'room' => $room->id])}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                    {{ method_field('PATCH') }}
                                    @include('admin.hotels.rates._form')
                                </form>
                            </div>
                            <!-- /.tab-content -->
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection