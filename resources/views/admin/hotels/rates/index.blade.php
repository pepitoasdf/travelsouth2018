@extends('admin.layouts.master')

@section('title', 'Room - Rates')

@section('header-title')
   <i class="list-icon feather feather-command"></i> Services
@endsection

@section('header-body')
    <span>Hotels</span>
    <i class="feather feather-chevron-right"></i>
    <span> Rooms</span>
    <i class="feather feather-chevron-right"></i>
    <span> Rates</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">List <span  class="text-success">( {{  strtoupper($hotel->name) }} )</span></h5>
                        <div class="widget-actions">
                            
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.hotels.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="data-table">
                                        <thead>
                                        <tr>
                                            <th>Room Type</th>
                                            <th>Total Room</th>
                                            <th class="text-right pd-r-30" width="20%">Rate</th>
                                            <th>Effective Date</th>
                                            @can('edit_roomrates', 'delete_roomrates')
                                            <th class="text-center">Actions</th>
                                            @endcan
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($results as $item)
                                            <tr>
                                                <td>{{ $item->type->name }}</td>
                                                <td>{{ number_format($item->room_no,2) }}</td>
                                                <td class="text-right pd-r-30">{{ ($item->getrate) ? number_format($item->getrate->rate,2) : '0.00'}}</td>
                                                <td>{{ ($item->getrate) ? $item->getrate->effective_date : '' }}</td>
                                                @can('edit_roomrates','delete_roomrates')
                                                <td class="text-center">
                                                   @include('partials.actions', [
                                                        'entity' => 'roomrates',
                                                        'ref_id' =>$hotel->id,
                                                        'id' => $item->id,
                                                        'remove_delete' => true
                                                    ])
                                                </td>
                                                @endcan
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{ $results->links() }}
                            </div>
                            <!-- /.tab-content -->
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection