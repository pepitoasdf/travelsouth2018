{{ csrf_field() }}
<div class="table-responsive">
    <table class="table table-striped table-hover" id="data-table">
        <thead>
        <tr>
            <th>Rate</th>
            <th>Effective Date</th>
            <th>Status</th>
            <th></th>
        </tr>
        <tr>
            <th>
                <input type="number"  class="form-control form rate">
            </th>
            <th>
                <input type="text"  class="datepicker form-control form effective_date">
            </th>
            <th></th>
            <th>
                <a href="#" class="btn btn-success btn-rounded btn-sm add-rate">
                    <i class="list-icon feather feather-plus"></i>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($room->rates as $item)
            <tr>
                <td>
                    {{  number_format($item->rate,2) }}
                    <input type="hidden" name="rate[]" value="{{ $item->rate }}">
                    <input type="hidden" name="rate_id[]" value="{{ $item->id }}">
                </td>
                <td>
                    {{ $item->effective_date }}
                    <input type="hidden" name="effective_date[]" value="{{ $item->effective_date }}">
                </td>
                <td>
                    @if($room->getrate->id == $item->id)
                        <span class="badge bg-success ">Active</span>
                    @else
                        <span class="badge bg-warning ">Inactive</span>
                    @endif
                </td>
                <td>
                    <a href="#" class="btn btn-danger btn-rounded btn-sm remove-rate">
                    <i class="list-icon feather feather-x"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3">
                    <br>
                    <br>
                    @can('edit_roomrates')
                        <button type="submit" name="save" value="save" class="btn btn-success btn-rounded btn-sm "> Submit</button>
                    @endcan
                </td>
            </tr>
        </tfoot>
    </table>
</div>
@push('append_js')
    <script type="text/javascript">
        $('.add-rate').click(function(e){
            e.preventDefault();
            var rate = $(this).closest('tr').find('.rate').val();
            var date = $(this).closest('tr').find('.effective_date').val();
            
            if(!rate){
                alert('Rate Per Day is required.');
                $('.rate').focus();
                return false;
            }
            else if(!date){
                alert('Effective Date is required.');
                $('.effective_date').focus();
                return false
            }

            $('table#data-table tbody').append(
                '<tr>\
                    <td>\
                        '+rate+'\
                        <input type="hidden" name="rate[]" value="'+rate+'">\
                        <input type="hidden" name="rate_id[]" value="0">\
                    </td>\
                    <td>\
                        '+date+'\
                        <input type="hidden" name="effective_date[]" value="'+date+'">\
                    </td>\
                    <td>\
                        <span class="badge bg-default"> ... </span>\
                    </td>\
                    <td>\
                        <a href="#" class="btn btn-danger btn-rounded btn-sm remove-rate">\
                        <i class="list-icon feather feather-x"></i>\
                        </a>\
                    </td>\
                </tr>'
            );
            $(this).closest('tr').find('.rate').val('');
            $(this).closest('tr').find('.effective_date').val('');

             $(this).closest('tr').find('.rate').focus();
        });

        $('table#data-table').on('click','.remove-rate', function(e){
            e.preventDefault();
            var refid = $(this).closest('tr').find('input[name="rate_id[]"]').val();
            $(this).closest('tr').remove();
            $('table#data-table tbody').append(
                '<input type="hidden" name="remove_id[]" value="'+refid+'">'
            );
        });

    </script>
@endpush