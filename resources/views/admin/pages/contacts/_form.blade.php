{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="name">Name :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="" type="text" placeholder="Name" required name="name" value="{{ isset($contact) ? $contact->name : old('name') }}">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="email">Email :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" placeholder="" type="email" placeholder="Email" required name="email" value="{{ isset($contact) ? $contact->email : old('email') }}">
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="phone">Phone :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('phone') ? ' is-invalid' : '' }}" id="phone" placeholder="" type="text" placeholder="Phone" required name="phone" value="{{ isset($contact) ? $contact->phone : old('phone') }}">
        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="address">Address :</label>
    <div class="col-md-7">
        <textarea class="form-control form {{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" id="address" required>{{ isset($contact) ? $contact->address : old('address') }}</textarea>
        @if ($errors->has('address'))
            <span class="help-block">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>
</div>
@include('partials.status', ['data' => (isset($contact) ? $contact : null)])

