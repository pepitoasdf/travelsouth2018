{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="name">Name :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" type="text" placeholder="name" required name="name" value="{{ isset($partner) ? $partner->name : old('name') }}">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="url">Website URL :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('url') ? ' is-invalid' : '' }}" id="url" placeholder="" type="text" placeholder="url" required name="url" value="{{ isset($partner) ? $partner->url : old('url') }}">
        @if ($errors->has('url'))
            <span class="help-block">
                <strong>{{ $errors->first('url') }}</strong>
            </span>
        @endif
    </div>
</div>

@include('partials.status', ['data' => (isset($partner) ? $partner : null)])
<hr>
<h6 class="form-section">Upload Images</h6>
@include('partials.photo')