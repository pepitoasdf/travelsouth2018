{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="name">Title :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" type="text" placeholder="Title" required name="title" value="{{ isset($background) ? $background->title : old('title') }}">
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="subtitle">Subtitle :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('subtitle') ? ' is-invalid' : '' }}" id="subtitle" placeholder="" type="text" placeholder="Subtitle" required name="subtitle" value="{{ isset($background) ? $background->subtitle : old('subtitle') }}">
        @if ($errors->has('subtitle'))
            <span class="help-block">
                <strong>{{ $errors->first('subtitle') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Description :</label>
    <div class="col-md-7">
        <textarea rows="4" placeholder="Description" class="form-control form {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="description" required>{{ isset($background) ? $background->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>
@include('partials.status', ['data' => (isset($background) ? $background : null)])
<hr>
<h6 class="form-section">Upload Images</h6>
@include('partials.photo')