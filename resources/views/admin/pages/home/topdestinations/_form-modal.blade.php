<div class="form-group row">
    <label class="col-md-4 col-form-label text-right" for="name">Name :</label>
    <div class="col-md-8">
        <input class="form-control form {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" type="text" placeholder="Name" required name="name" value="{{ isset($detail) ? $detail->name : old('name') }}">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-4 col-form-label text-right" for="location">Location :</label>
    <div class="col-md-8">
        <select data-placeholder="Location"  class="form-control select-location {{ $errors->has('location') ? ' is-invalid' : '' }}" name="location" required="">
            @if(isset($detail))
                <option value="{{$detail->country_id}}|{{$detail->province_id}}|{{$detail->city_id}}">
                    {{ ($detail->city->name) ? $detail->city->name.', ' : '' }}
                    {{ ($detail->province->name) ? $detail->province->name.', ' : '' }}
                    {{ $detail->country->name }}
                </option>
            @endif
        </select>
        @if ($errors->has('location'))
            <span class="help-block">
                <strong>{{ $errors->first('location') }}</strong>
            </span>
        @endif
    </div>
</div>
@if(isset($detail))
    <input type="hidden" name="detail_id" value="{{ $detail->id }}">
@endif
@include('partials.single-photo')

@push('append_js')
<script type="text/javascript">
    $(document).ready(function(){
        $(".select-location").select2({
            minimumInputLength : 1,
            allowClear: false,
            ajax : {
                url : "{{ route('hotelLocations.api') }}",
                dataType : 'json',
                data : function (params) {
                    return {
                        name: params.term
                    };
                },
                processResults: function (data) {
                      return {
                        results: data
                    };
                },
                cache: true
            }
            ,
            escapeMarkup: function (markup) { return markup; },
            templateResult: function(repo) {
                return repo.text;
            },
            templateSelection: function(repo){
                return repo.text;
            }
        });
    });
</script>
@endpush