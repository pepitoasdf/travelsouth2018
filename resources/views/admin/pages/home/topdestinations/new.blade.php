@extends('admin.layouts.master')

@section('title', 'Home - Top Destinations')

@section('header-title')
   <i class="list-icon feather feather-globe"></i> Pages
@endsection

@section('header-body')
    <span>Home</span>
    <i class="feather feather-chevron-right"></i>
    <span>Top Destinations</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">Create</h5>
                        <div class="widget-actions">
                             @include('partials.add-list',['entity' => 'topdestinations','reference' => 'Top Destination'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.pages.home.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <form class="form-horizontal" data-pjax action="{{route('topdestinations.store')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                    @include('admin.pages.home.topdestinations._form')
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-right"></label>
                                        <div class="col-md-5">
                                            @can('add_topdestinations')
                                                <button type="submit" name="save" value="save" class="btn btn-success btn-rounded btn-sm"> Submit</button>
                                            @endcan
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-content -->
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection