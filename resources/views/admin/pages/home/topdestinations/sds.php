{{ csrf_field() }}
<div class="form-group row form-hotels">
    <label class="col-md-3 col-form-label text-right ">Hotels</label>
    <div class="col-md-7">
        <select data-placeholder="Hotels" class="form-control select-hotels {{ $errors->has('hotels') ? ' is-invalid' : '' }}" name="hotels[]" multiple required="">
        </select>
        @if ($errors->has('hotels'))
            <span class="help-block">
                <strong>{{ $errors->first('hotels') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Description :</label>
    <div class="col-md-7">
        <textarea rows="4" placeholder="Description" class="form-control form {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="description" required>{{ isset($topdestination) ? $topdestination->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>
@include('partials.status', ['data' => (isset($topdestination) ? $topdestination : null)])

@push('append_js')
    <script type="text/javascript">
        var initialPropertyOptions = {!! isset($topdestination) ? $details : [] !!};

        $('.select-hotels').select2({
            minimumInputLength : 1,
            allowClear: false,
            ajax : {
                url : "{{ route('hotels.api') }}",
                dataType : 'json',
                data : function (params) {
                    return {
                        name: params.term
                    };
                },
                processResults: function (data) {
                      return {
                        results: data
                    };
                },
                cache: true
            }
            ,
            escapeMarkup: function (markup) { return markup; },
            templateResult: function(repo) {
                return repo.text;
            },
            templateSelection: function(repo){
                return repo.text;
            },
            data : initialPropertyOptions
        });
    </script>
@endpush