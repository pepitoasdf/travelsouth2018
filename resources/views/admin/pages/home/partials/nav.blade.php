<ul class="nav nav-tabs">
	@can('view_background')
	    <li class="nav-item">
	    	<a class="nav-link {{ Request::is('admin/pages/home/background*') ? 'active' : '' }}" href="{{ route('background.index') }}" aria-expanded="true">
	    		Background
	    	</a>
	    </li>
	@endcan
	@can('view_increadibleplaces')
	    <li class="nav-item">
	    	<a class="nav-link {{ Request::is('admin/pages/home/increadibleplaces*') ? 'active' : '' }}" href="{{ route('increadibleplaces.index') }}" aria-expanded="true">
	    		Increadible Places
	    	</a>
	    </li>
	@endcan
	@can('view_partners')
	    <li class="nav-item">
	    	<a class="nav-link {{ Request::is('admin/pages/home/partners*') ? 'active' : '' }}" href="{{ route('partners.index') }}" aria-expanded="true">
	    		Partners
	    	</a>
	    </li>
	@endcan
	@can('view_popular')
	    <li class="nav-item">
	    	<a class="nav-link {{ Request::is('admin/pages/home/popular*') ? 'active' : '' }}" href="{{ route('popular.index') }}" aria-expanded="true">
	    		Popular
	    	</a>
	    </li>
	@endcan
	@can('view_topdestinations')
	    <li class="nav-item">
	    	<a class="nav-link {{ Request::is('admin/pages/home/topdestinations*') ? 'active' : '' }}" href="{{ route('topdestinations.index') }}" aria-expanded="true">
	    		Top Destinations
	    	</a>
	    </li>
	@endcan
	@can('view_videos')
	    <li class="nav-item">
	    	<a class="nav-link {{ Request::is('admin/pages/home/videos*') ? 'active' : '' }}" href="{{ route('videos.index') }}" aria-expanded="true">
	    		Videos
	    	</a>
	    </li>
	@endcan
</ul>