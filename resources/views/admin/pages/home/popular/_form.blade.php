{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="name">Title :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('title') ? ' is-invalid' : '' }}" id="title"  type="text" placeholder="Title" required name="title" value="{{ isset($popular) ? $popular->title : old('title') }}">
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Description :</label>
    <div class="col-md-7">
        <textarea rows="4" placeholder="Description" class="form-control form {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="description" required>{{ isset($popular) ? $popular->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>
@include('partials.status', ['data' => (isset($popular) ? $popular : null)])


<hr/>
<h6 class="form-section">Popular Services</h6>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right">Service Type :</label>
    <div class="col-md-7">
        <select class="form-control service-type" name="type" required="">
            @if(isset($popular))
                <option value="Packages" {{ $popular->type == "Packages" ? 'selected' : '' }}>Packages</option>
                <option value="Vehicles" {{ $popular->type == "Vehicles" ? 'selected' : '' }}>Vehicles</option>
                <option value="Hotels" {{ $popular->type == "Hotels" ? 'selected' : '' }}>Hotels</option>
            @elseif(old('type'))
                <option value="Packages" {{ old('type') == "Packages" ? 'selected' : '' }}>Packages</option>
                <option value="Vehicles" {{ old('type') == "Vehicles" ? 'selected' : '' }}>Vehicles</option>
                <option value="Hotels" {{ old('type') == "Hotels" ? 'selected' : '' }}>Hotels</option>
            @else
                <option value="Packages" >Packages</option>
                <option value="Vehicles">Vehicles</option>
                <option value="Hotels">Hotels</option>
            @endif
        </select>
        @if ($errors->has('type'))
            <span class="help-block">
                <strong>{{ $errors->first('type') }}</strong>
            </span>
        @endif
    </div>
</div>
<?php
    $packages = '';
    $vehicles = 'display:none';
    $hotels = 'display:none';
    if(isset($popular)){
        if($popular->type == "Packages"){
            $packages = '';
            $hotels = 'display:none';
            $vehicles = 'display:none';
        }
        else if($popular->type == "Hotels"){
            $packages = 'display:none';
            $hotels = '';
            $vehicles = 'display:none';
        }
        else if($popular->type == "Vehicles"){
            $packages = 'display:none';
            $hotels = 'display:none';
            $vehicles = '';
        }
    }
    else{
        if($errors->has('service_packages')){
            $packages = '';
            $hotels = 'display:none';
            $vehicles = 'display:none';
        }
        else if($errors->has('service_vehicles')){
            $packages = 'display:none';
            $hotels = 'display:none';
            $vehicles = '';
        }
        else if($errors->has('service_hotels')){
            $packages = 'display:none';
            $hotels = '';
            $vehicles = 'display:none';
        }
    }

?>
<div class="form-group row form-packages" style="{{ $packages  }}">
    <label class="col-md-3 col-form-label text-right service-name {{ $errors->has('service_packages') ? ' is-invalid' : '' }}">Packages</label>
    <div class="col-md-7">
        <select data-placeholder="Packages" class="form-control select-packages" name="service_packages[]" multiple required="">
            @if(isset($popular))
                @if($popular->type == "Packages")
                    @foreach($popular->details as $p)
                        <option value="{{ $p->ref_id }}" selected="">{{ $p->package->name }}</option>
                    @endforeach
                @endif
            @endif
        </select>
        @if ($errors->has('service_packages'))
            <span class="help-block">
                <strong>{{ $errors->first('service_packages') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row form-vehicles" style="{{ $vehicles }}">
    <label class="col-md-3 col-form-label text-right service-name">Vehicles</label>
    <div class="col-md-7">
        <select data-placeholder="Vehicles" class="form-control select-vehicles {{ $errors->has('service_vehicles') ? ' is-invalid' : '' }}" name="service_vehicles[]" multiple>
            @if(isset($popular))
                @if($popular->type == "Vehicles")
                    @foreach($popular->details as $p)
                        <option value="{{ $p->ref_id }}" selected="">{{ $p->vehicle->name }}</option>
                    @endforeach
                @endif
            @endif
        </select>
        @if ($errors->has('service_vehicles'))
            <span class="help-block">
                <strong>{{ $errors->first('service_vehicles') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row form-hotels" style="{{ $hotels  }}">
    <label class="col-md-3 col-form-label text-right service-name">Hotels</label>
    <div class="col-md-7">
        <select data-placeholder="Hotels" class="form-control select-hotels {{ $errors->has('service_hotels') ? ' is-invalid' : '' }}" name="service_hotels[]" multiple>
            @if(isset($popular))
                @if($popular->type == "Hotels")
                    @foreach($popular->details as $p)
                        <option value="{{ $p->ref_id }}" selected="">{{ $p->hotel->name }}</option>
                    @endforeach
                @endif
            @endif
        </select>
        @if ($errors->has('service_hotels'))
            <span class="help-block">
                <strong>{{ $errors->first('service_hotels') }}</strong>
            </span>
        @endif
    </div>
</div>

@push('append_js')
    <script type="text/javascript">
        var urlPackage = "{{ route('packages.api') }}";
        var urlHotel = "{{ route('hotels.api') }}";
        var urlVehicle = "{{ route('vehicles.api') }}";
        var vehicles = $('.select-vehicles');
        var hotels = $('.select-hotels');
        var packages = $('.select-packages');

        $('.service-type').on('change',function(e){
            e.preventDefault();
            var val = $(this).val();
            if(val == "Packages"){
                $('.form-packages').show();
                $('.form-hotels').hide();
                $('.form-vehicles').hide();

                packages.attr('required','required');
                hotels.removeAttr('required');
                vehicles.removeAttr('required');
            }
            else if(val == "Hotels"){
                $('.form-packages').hide();
                $('.form-hotels').show();
                $('.form-vehicles').hide();

                packages.removeAttr('required');
                hotels.attr('required','required');
                vehicles.removeAttr('required');
            }
            else if(val == "Vehicles"){
                $('.form-packages').hide();
                $('.form-hotels').hide();
                $('.form-vehicles').show();

                packages.removeAttr('required');
                hotels.removeAttr('required');
                vehicles.attr('required','required');
            }
        });

        function enableSelect2js(element, url){
            element.select2({
                minimumInputLength : 1,
                allowClear: false,
                ajax : {
                    url : url,
                    dataType : 'json',
                    data : function (params) {
                        return {
                            name: params.term
                        };
                    },
                    processResults: function (data) {
                          return {
                            results: data
                        };
                    },
                    cache: true
                }
                ,
                escapeMarkup: function (markup) { return markup; },
                templateResult: function(repo) {
                    return repo.text;
                },
                templateSelection: function(repo){
                    return repo.text;
                }
            });
        }

        enableSelect2js(vehicles, urlVehicle);
        enableSelect2js(hotels, urlHotel);
        enableSelect2js(packages, urlPackage);
    </script>
@endpush