@extends('admin.layouts.master')

@section('title', 'Home - Popular')

@section('header-title')
   <i class="list-icon feather feather-globe"></i> Pages
@endsection

@section('header-body')
    <span>Home</span>
    <i class="feather feather-chevron-right"></i>
    <span>Popular</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">Edit</h5>
                        <div class="widget-actions">
                             @include('partials.add-list',['entity' => 'popular','reference' => 'Popular'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.pages.home.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <form class="form-horizontal" data-pjax action="{{route('popular.update',$popular->id)}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                    {{ method_field('PATCH') }}
                                    @include('admin.pages.home.popular._form')
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-right"></label>
                                        <div class="col-md-5">
                                            @can('edit_popular')
                                                <button type="submit" name="save" value="save" class="btn btn-success btn-rounded btn-sm"> Update</button>
                                            @endcan
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-content -->
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection