@extends('admin.layouts.master')

@section('title', 'Home - Videos')

@section('header-title')
   <i class="list-icon feather feather-globe"></i> Pages
@endsection

@section('header-body')
    <span>Home</span>
    <i class="feather feather-chevron-right"></i>
    <span>Videos</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">List</h5>
                        <div class="widget-actions">
                             @include('partials.add-list',['entity' => 'videos','reference' => 'Video Url'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.pages.home.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="data-table">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                            @can('edit_videos', 'delete_videos')
                                                <th class="text-center">Actions</th>
                                            @endcan
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($results as $item)
                                            <tr>
                                                <td>{{ $item->title }}</td>
                                                <td>{{ $item->description }}</td>
                                                <td>
                                                    @if($item->status == "Draft")
                                                        <span class="badge bg-info ">{{ $item->status }}</span>
                                                    @elseif($item->status == "Published")
                                                        <span class="badge bg-success ">{{ $item->status }}</span>
                                                    @elseif($item->status == "Pending")
                                                        <span class="badge bg-warning ">{{ $item->status }}</span>
                                                    @endif
                                                </td>
                                                @can('edit_videos','delete_videos')
                                                    <td class="text-center">
                                                       @include('partials.actions', [
                                                            'entity' => 'videos',
                                                            'id' => $item->id
                                                        ])
                                                    </td>
                                                @endcan
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{ $results->links() }}
                            </div>
                            <!-- /.tab-content -->
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection