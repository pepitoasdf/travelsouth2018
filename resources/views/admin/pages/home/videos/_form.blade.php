{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="name">Title :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" placeholder="" type="text" placeholder="Title" required name="title" value="{{ isset($video) ? $video->title : old('title') }}">
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Description :</label>
    <div class="col-md-7">
        <textarea rows="4" placeholder="Description" class="form-control form {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="description" required>{{ isset($video) ? $video->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="url">Video URL :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('url') ? ' is-invalid' : '' }}" id="url" placeholder="" type="text" placeholder="https://" required name="url" value="{{ isset($video) ? $video->url : old('url') }}">
        @if ($errors->has('url'))
            <span class="help-block">
                <strong>{{ $errors->first('url') }}</strong>
            </span>
        @endif
    </div>
</div>
@include('partials.status', ['data' => (isset($video) ? $video : null)])