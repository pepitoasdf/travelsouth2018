@extends('admin.layouts.master')

@section('title', 'Home - Increadible Places')

@section('header-title')
   <i class="list-icon feather feather-globe"></i> Pages
@endsection

@section('header-body')
    <span>Home</span>
    <i class="feather feather-chevron-right"></i>
    <span>Increadible Places</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">Edit</h5>
                        <div class="widget-actions">
                             @include('partials.add-list',['entity' => 'increadibleplaces','reference' => 'Increadible Place'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.pages.home.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <form class="form-horizontal" data-pjax action="{{route('increadibleplaces.update',$increadibleplace->id)}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                    {{ method_field('PATCH') }}
                                    @include('admin.pages.home.increadibleplaces._form')
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-right"></label>
                                        <div class="col-md-5">
                                            @can('edit_increadibleplaces')
                                                <button type="submit" name="save" value="save" class="btn btn-success btn-rounded btn-sm"> Update</button>
                                            @endcan
                                        </div>
                                    </div>
                                </form>
                                @if(count($increadibleplace))
                                    <hr/>
                                    <h6 class="form-section">Location Details</h6>
                                    <button type="button" class="btn btn-info btn-rounded btn-sm pull-right" data-toggle="modal" data-target=".add-image">
                                        <i class="list-icon feather feather-plus"></i>
                                        Image
                                    </button>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover" id="data-table">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Location</th>
                                                @can('edit_increadibleplaces', 'delete_increadibleplaces')
                                                    <th class="text-center">Actions</th>
                                                @endcan
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($increadibleplace->details as $item)
                                                <tr>
                                                    <td>{{ $item->name }}</td>
                                                    <td class=>
                                                        {{ ($item->city->name) ? $item->city->name.', ' : '' }}
                                                        {{ ($item->province->name) ? $item->province->name.', ' : '' }}
                                                        {{ $item->country->name }}
                                                    </td>
                                                    @can('edit_increadibleplaces','delete_increadibleplaces')
                                                        <td class="text-center">
                                                            <a title="Edit" href="#" class="btn btn-sm btn-rounded btn-info edit-btn"
                                                                data-toggle="modal" data-target=".add-image"
                                                                data-id="{{ $item->id }}"
                                                            >
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                            <form  data-pjax action="{{route('increadibleplaces.destroy', $id)}}" style="display: inline" onsubmit="return confirm('Do you really want to delete it?')" method="POST">
                                                                {{csrf_field()}}
                                                                {{ method_field('DELETE') }}
                                                                <input type="hidden" name="detal_id" value="{{ $item->id }}">
                                                                <button title="Delete" type="submit" class="btn-delete btn btn-sm btn-rounded btn-danger">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </button>
                                                        </form>
                                                        </td>
                                                    @endcan
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            </div>
                            <!-- /.tab-content -->
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>


    <!-- Modal -->
    <div class="modal fade add-image" role="dialog" aria-labelledby="imageModal" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="imageModal">
                        Top Destination Location
                    </h5>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="image-add-update" name="image-add-update" data-pjax action="{{route('increadibleplaces.update',$increadibleplace->id)}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}
                        <div class="results">
                            @include('admin.pages.home.increadibleplaces._form-modal')
                        </div>
                    </form>
                </div>
                <div class="modal-footer"> 
                    <button type="submit" form="image-add-update" type="submit" class="btn btn-success btn-rounded ripple text-left btn-sm">
                        Submit
                    </button>
                    <button type="button" class="btn btn-default btn-rounded ripple text-left btn-sm" data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- End modal -->

    @push('append_js')

        <script type="text/javascript">
            
            $(document).ready(function(){
                $('.edit-btn').click(function(e){
                    e.preventDefault();
                    var id = $(this).data('id');
                    var url = "{{ route('increadibleplaceUpdate.api') }}";
                    $.get(url,{id:id},function(results){
                        $('form#image-add-update').find('.results').html(results);
                    });
                });
            });

        </script>

    @endpush
@endsection