@extends('admin.layouts.master')

@section('title', 'Abouts')

@section('header-title')
   <i class="list-icon feather feather-globe"></i> Pages
@endsection

@section('header-body')
    <span>Abouts</span>
@endsection

@section('content')
<div class="widget-list">
    <div class="row">
        <div class="widget-holder col-md-12">
            <div class="widget-bg">
                <div class="widget-heading widget-heading-border">
                    <h5 class="widget-title">Edit</h5>
                    <div class="widget-actions">
                        @include('partials.add-list',['entity' => 'abouts','reference' => 'Abouts'])
                    </div>
                    <!-- /.widget-actions -->
                </div>
                <!-- /.widget-heading -->
                <div class="widget-body">
                    <form class="form-horizontal" data-pjax action="{{route('abouts.update',$about->id)}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        @include('admin.pages.abouts._form')
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-right"></label>
                            <div class="col-md-5">
                                @can('add_abouts')
                                    <button type="submit" name="update" value="update" class="btn btn-success btn-rounded btn-sm"> Update</button>
                                @endcan
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.widget-body -->
            </div>
            <!-- /.widget-bg -->
        </div>
        <!-- /.widget-holder -->
    </div>
    <!-- /.row -->
</div>
@endsection

