{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="company">Company :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('company') ? ' is-invalid' : '' }}" id="company" placeholder="" type="text" placeholder="Company" required name="company" value="{{ isset($about) ? $about->company : old('company') }}">
        @if ($errors->has('company'))
            <span class="help-block">
                <strong>{{ $errors->first('company') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Description :</label>
    <div class="col-md-7">
        <textarea data-toggle="tinymce" rows="6" class="form-control form {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="description" required>{{ isset($about) ? $about->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="email">Email :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" placeholder="" type="email" placeholder="Email" required name="email" value="{{ isset($about) ? $about->email : old('email') }}">
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="phone">Phone :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('phone') ? ' is-invalid' : '' }}" id="phone" placeholder="" type="text" placeholder="Phone" required name="phone" value="{{ isset($about) ? $about->phone : old('phone') }}">
        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="address">Address :</label>
    <div class="col-md-7">
        <textarea class="form-control form {{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" id="address" required>{{ isset($about) ? $about->address : old('address') }}</textarea>
        @if ($errors->has('address'))
            <span class="help-block">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>
</div>
@include('partials.status', ['data' => (isset($about) ? $about : null)])
<hr/>
<h6 class="form-section">Upload Images</h6>
@include('partials.photo')

@push('append_js')
    <script src="{{ asset('plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('plugins/tinymce/themes/inlite/theme.min.js') }}"></script>
    <script src="{{ asset('plugins/tinymce/jquery.tinymce.min.js') }}"></script>
@endpush