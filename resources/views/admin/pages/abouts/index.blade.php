@extends('admin.layouts.master')

@section('title', 'Abouts')

@section('header-title')
   <i class="list-icon feather feather-globe"></i> Pages
@endsection

@section('header-body')
    <span>Abouts</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">List</h5>
                        <div class="widget-actions">
                            @include('partials.add-list',['entity' => 'abouts','reference' => 'About'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="data-table">
                                <thead>
                                <tr>
                                    <th>Company</th>
                                    <th>Description</th>
                                    <th>Created At</th>
                                    @can('edit_abouts', 'delete_abouts')
                                    <th class="text-center">Actions</th>
                                    @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($results as $item)
                                    <tr>
                                        <td>{{ $item->company }}</td>
                                        <td>{!! $item->description !!}</td>
                                        <td>{{ $item->created_at }}</td>

                                        @can('edit_abouts')
                                        <td class="text-center">
                                           @include('partials.actions', [
                                                'entity' => 'abouts',
                                                'id' => $item->id
                                            ])
                                        </td>
                                        @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $results->links() }}
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection