@extends('admin.layouts.master')

@section('title', 'Blog - Post')

@section('header-title')
   <i class="list-icon feather feather-globe"></i> Pages
@endsection

@section('header-body')
    <span>Blog</span>
    <i class="feather feather-chevron-right"></i>
    <span>Posts</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">List</h5>
                        <div class="widget-actions">
                            @include('partials.add-list',['entity' => 'posts','reference' => 'Posts'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="data-table">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Body</th>
                                    <th>Author</th>
                                    <th>Status</th>
                                    @can('edit_posts', 'delete_posts')
                                    <th class="text-center">Actions</th>
                                    @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($results as $item)
                                    <tr>
                                        <td>{{ $item->title }}</td>
                                        <td>{!! str_limit($item->body,40) !!}</td>
                                        <td>{{ $item->author->lastname.' '.$item->author->firstname }}</td>
                                        <td>
                                            @if($item->status == "Draft")
                                                <span class="badge bg-info ">{{ $item->status }}</span>
                                            @elseif($item->status == "Published")
                                                <span class="badge bg-success ">{{ $item->status }}</span>
                                            @elseif($item->status == "Pending")
                                                <span class="badge bg-warning ">{{ $item->status }}</span>
                                            @endif
                                        </td>
                                        @can('edit_posts')
                                        <td class="text-center">
                                           @include('partials.actions', [
                                                'entity' => 'posts',
                                                'id' => $item->id
                                            ])
                                        </td>
                                        @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $results->links() }}
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection