@extends('admin.layouts.master')

@section('title', 'Blog - Post')

@section('header-title')
   <i class="list-icon feather feather-globe"></i> Pages
@endsection

@section('header-body')
    <span>Blog</span>
    <i class="feather feather-chevron-right"></i>
    <span>Posts</span>
@endsection

@section('content')
<div class="widget-list">
    <div class="row">
        <div class="widget-holder col-md-12">
            <div class="widget-bg">
                <div class="widget-heading widget-heading-border">
                    <h5 class="widget-title">Create</h5>
                    <div class="widget-actions">
                        @include('partials.add-list',['entity' => 'posts','reference' => 'Posts'])
                    </div>
                    <!-- /.widget-actions -->
                </div>
                <!-- /.widget-heading -->
                <div class="widget-body">
                    <form class="form-horizontal" data-pjax action="{{route('posts.store')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                        @include('admin.pages.blog.posts._form')
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-right" ></label>
                            <div class="col-md-5">
                                @can('add_posts')
                                    <button type="submit" name="save" value="save" class="btn btn-success btn-rounded btn-sm"> Submit</button>
                                @endcan
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.widget-body -->
            </div>
            <!-- /.widget-bg -->
        </div>
        <!-- /.widget-holder -->
    </div>
    <!-- /.row -->
</div>
@endsection