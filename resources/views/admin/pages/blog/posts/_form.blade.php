{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="author_id">Author :</label>
    <div class="col-md-7">
        <select id="author_id" data-placeholder="Author" class="form-control {{ $errors->has('author_id') ? ' is-invalid' : '' }}" name="author_id">
            @if(isset($post))
                <option value="{{ $post->author_id }}" >{{ $post->author->firstname }} {{ $post->author->lastname }}</option>
            @elseif($errors->has('author_id'))
                <option value="{{ old('author_id') }}" >{{ App\User::getName(old('author_id')) }}</option>
            @else
                <option value="{{ auth()->user()->id }}" >{{ auth()->user()->firstname }} {{  auth()->user()->lastname }}</option>
            @endif
        </select>
        @if ($errors->has('author_id'))
            <span class="help-block">
                <strong>{{ $errors->first('author_id') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="title">Title :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" placeholder="" type="text" placeholder="Title" required name="title" value="{{ isset($post) ? $post->title : old('title') }}">
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="body">Body :</label>
    <div class="col-md-7">
        <textarea data-toggle="tinymce" rows="6" class="form-control form {{ $errors->has('body') ? ' is-invalid' : '' }}" name="body" id="body" required>{{ isset($post) ? $post->body : old('body') }}</textarea>
        @if ($errors->has('body'))
            <span class="help-block">
                <strong>{{ $errors->first('body') }}</strong>
            </span>
        @endif
    </div>
</div>
@include('partials.status', ['data' => (isset($post) ? $post : null)])
<h6 class="form-section">Upload Images</h6>
@include('partials.photo')
@push('append_js')
    <script src="{{ asset('plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('plugins/tinymce/themes/inlite/theme.min.js') }}"></script>
    <script src="{{ asset('plugins/tinymce/jquery.tinymce.min.js') }}"></script>

    <script type="text/javascript">
        $("select#author_id").select2({
            minimumInputLength : 1,
            allowClear: false,
            ajax : {
                url : "{{ route('users.api') }}",
                dataType : 'json',
                data : function (params) {
                    return {
                        name: params.term
                    };
                },
                processResults: function (data) {
                      return {
                        results: data
                    };
                },
                cache: true
            }
            ,
            escapeMarkup: function (markup) { return markup; },
            templateResult: function(repo) {
                return repo.text;
            },
            templateSelection: function(repo){
                return repo.text;
            }
        });
    </script>
@endpush