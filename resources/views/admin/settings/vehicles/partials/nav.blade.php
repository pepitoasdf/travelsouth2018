<ul class="nav nav-tabs">
	@can('view_vfeatures')
	    <li class="nav-item">
	    	<a class="nav-link {{ Request::is('admin/settings/vehicles/vfeatures*') ? 'active' : '' }}" href="{{ route('vfeatures.index') }}" aria-expanded="true">
	    		Features
	    	</a>
	    </li>
	@endcan
</ul>