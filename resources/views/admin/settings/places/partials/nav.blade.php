<ul class="nav nav-tabs">
	@can('view_countries')
	    <li class="nav-item">
	    	<a class="nav-link {{ Request::is('admin/settings/places/countries*') ? 'active' : '' }}" href="{{ route('countries.index') }}" aria-expanded="true">
	    		Countries
	    	</a>
	    </li>
	@endcan
	@can('view_provinces')
	    <li class="nav-item">
	    	<a class="nav-link {{ Request::is('admin/settings/places/provinces*') ? 'active' : '' }}" href="{{ route('provinces.index') }}" aria-expanded="true">
	    		States/Provinces
	    	</a>
	    </li>
	@endcan
	@can('view_cities')
	    <li class="nav-item">
	    	<a class="nav-link {{ Request::is('admin/settings/places/cities*') ? 'active' : '' }}" href="{{ route('cities.index') }}" aria-expanded="true">
	    		Cities/Municipalities
	    	</a>
	    </li>
	@endcan
</ul>