@extends('admin.layouts.master')

@section('title', 'Countries')

@section('header-title')
   <i class="list-icon feather feather-settings"></i> Settings
@endsection

@section('header-body')
    <span>Places</span>
    <i class="feather feather-chevron-right"></i>
    <span>Countries</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">List</h5>
                        <div class="widget-actions">
                            @include('partials.add-list',['entity' => 'countries','reference' => 'Country'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.settings.places.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="data-table">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Currency Name</th>
                                            <th>Currency Code</th>
                                            @can('edit_countries', 'delete_countries')
                                            <th class="text-center">Actions</th>
                                            @endcan
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($results as $item)
                                            <tr>
                                                <td>{{ $item->code }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->currency_name }}</td>
                                                <td>{{ $item->currency_code }}</td>

                                                @can('edit_countries','delete_countries')
                                                <td class="text-center">
                                                   @include('partials.actions', [
                                                        'entity' => 'countries',
                                                        'id' => $item->id
                                                    ])
                                                </td>
                                                @endcan
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{ $results->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection