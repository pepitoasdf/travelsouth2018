{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="code">Code :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('code') ? ' is-invalid' : '' }}" id="code" type="text" placeholder="Code" name="code" value="{{ isset($country) ? $country->code : old('code') }}">
        @if ($errors->has('code'))
            <span class="help-block">
                <strong>{{ $errors->first('code') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="name">Name :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" type="text" placeholder="Name" name="name" value="{{ isset($country) ? $country->name : old('name') }}">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="currency_name">Currency Name :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('currency_name') ? ' is-invalid' : '' }}" id="currency_name" placeholder="Currency name" type="text"  name="currency_name" value="{{ isset($country) ? $country->currency_name : old('currency_name') }}">
        @if ($errors->has('currency_name'))
            <span class="help-block">
                <strong>{{ $errors->first('currency_name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="dial_code">Dial Code :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('dial_code') ? ' is-invalid' : '' }}" id="dial_code" placeholder="Dial code" type="text"  name="dial_code" value="{{ isset($country) ? $country->dial_code : old('dial_code') }}">
        @if ($errors->has('dial_code'))
            <span class="help-block">
                <strong>{{ $errors->first('dial_code') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="currency_symbol">Currency Symbol :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('currency_symbol') ? ' is-invalid' : '' }}" id="currency_symbol" placeholder="Currency Symbol" type="text" name="currency_symbol" value="{{ isset($country) ? $country->currency_symbol : old('currency_symbol') }}">
        @if ($errors->has('currency_symbol'))
            <span class="help-block">
                <strong>{{ $errors->first('currency_symbol') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="currency_code">Currency Code :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('currency_code') ? ' is-invalid' : '' }}" id="currency_code" placeholder="Currency Code" type="text" name="currency_code" value="{{ isset($country) ? $country->currency_code : old('currency_code') }}">
        @if ($errors->has('currency_code'))
            <span class="help-block">
                <strong>{{ $errors->first('currency_code') }}</strong>
            </span>
        @endif
    </div>
</div>
