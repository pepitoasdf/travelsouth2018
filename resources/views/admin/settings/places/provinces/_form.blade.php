{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="country">Country :</label>
    <div class="col-md-7">
        <select class="select-country form-control form {{ $errors->has('country') ? ' is-invalid' : '' }}" id="country" data-placeholder="Country" name="country" required="">
            @if(isset($province->getcountry))
                <option value="{{ $province->country }}">{{ $province->getcountry->name }}</option>
            @endif
        </select>
        @if ($errors->has('country'))
            <span class="help-block">
                <strong>{{ $errors->first('country') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="province">State/Province :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('province') ? ' is-invalid' : '' }}" id="province" type="text" placeholder="State/Provinces" name="name" value="{{ isset($province) ? $province->name : old('province') }}" required="">
        @if ($errors->has('province'))
            <span class="help-block">
                <strong>{{ $errors->first('province') }}</strong>
            </span>
        @endif
    </div>
</div>

@push('append_js')
    <script type="text/javascript">
        $(".select-country").select2({
            minimumInputLength : 1,
            allowClear: false,
            ajax : {
                url : "{{ route('countries.api') }}",
                dataType : 'json',
                data : function (params) {
                    return {
                        name: params.term
                    };
                },
                processResults: function (data) {
                      return {
                        results: data
                    };
                },
                cache: true
            }
            ,
            escapeMarkup: function (markup) { return markup; },
            templateResult: function(repo) {
                return repo.text;
            },
            templateSelection: function(repo){
                return repo.text;
            }
        });
    </script>
@endpush