<ul class="nav nav-tabs">
	@can('view_pfeatures')
	    <li class="nav-item">
	    	<a class="nav-link {{ Request::is('admin/settings/packages/pfeatures*') ? 'active' : '' }}" href="{{ route('pfeatures.index') }}" aria-expanded="true">
	    		Features
	    	</a>
	    </li>
	@endcan
</ul>