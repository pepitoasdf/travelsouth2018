@extends('admin.layouts.master')

@section('title', 'Packages - Features')

@section('header-title')
   <i class="list-icon feather feather-settings"></i> Settings
@endsection

@section('header-body')
    <span>Packages</span>
    <i class="feather feather-chevron-right"></i>
    <span> Features</span>
@endsection

@section('content')
<div class="widget-list">
    <div class="row">
        <div class="widget-holder col-md-12">
            <div class="widget-bg">
                <div class="widget-heading widget-heading-border">
                    <h5 class="widget-title">Create</h5>
                    <div class="widget-actions">
                        @include('partials.add-list',['entity' => 'pfeatures','reference' => 'Features'])
                    </div>
                    <!-- /.widget-actions -->
                </div>
                <!-- /.widget-heading -->
                <div class="widget-body">
                    <div class="tabs">
                        @include('admin.settings.packages.partials.nav')
                        <!-- /.nav-tabs -->
                        <div class="tab-content">
                            <form class="form-horizontal" data-pjax  action="{{route('pfeatures.store')}}" method="POST" autocomplete="off">
                                @include('admin.settings.packages.features._form')
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label text-right"></label>
                                    <div class="col-md-5">
                                        @can('add_pfeatures')
                                            <button type="submit" name="save" value="save" class="btn btn-success btn-rounded btn-sm"> Submit</button>
                                        @endcan
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.widget-body -->
            </div>
            <!-- /.widget-bg -->
        </div>
        <!-- /.widget-holder -->
    </div>
    <!-- /.row -->
</div>
@endsection