<ul class="nav nav-tabs">
	@can('view_facilities')
	    <li class="nav-item">
	    	<a class="nav-link {{ Request::is('admin/settings/hotels/facilities*') ? 'active' : '' }}" href="{{ route('facilities.index') }}" aria-expanded="true">
	    		Facilities
	    	</a>
	    </li>
	@endcan
	@can('view_roomtypes')
	    <li class="nav-item">
	    	<a class="nav-link {{ Request::is('admin/settings/hotels/roomtypes*') ? 'active' : '' }}" href="{{ route('roomtypes.index') }}" aria-expanded="true">
	    		Room Types
	    	</a>
	    </li>
	@endcan
	@can('view_hoteltypes')
	    <li class="nav-item">
	    	<a class="nav-link {{ Request::is('admin/settings/hotels/hoteltypes*') ? 'active' : '' }}" href="{{ route('hoteltypes.index') }}" aria-expanded="true">
	    		Types
	    	</a>
	    </li>
	@endcan
</ul>