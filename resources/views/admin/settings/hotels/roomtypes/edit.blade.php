@extends('admin.layouts.master')

@section('title', 'Hotel - Room Types')

@section('header-title')
   <i class="list-icon feather feather-settings"></i> Settings
@endsection

@section('header-body')
    <span>Hotels</span>
    <i class="feather feather-chevron-right"></i>
    <span> Room Types</span>
@endsection

@section('content')
<div class="widget-list">
    <div class="row">
        <div class="widget-holder col-md-12">
            <div class="widget-bg">
                <div class="widget-heading widget-heading-border">
                    <h5 class="widget-title">Edit</h5>
                    <div class="widget-actions">
                        @include('partials.add-list',['entity' => 'roomtypes','reference' => 'Room Types'])
                    </div>
                    <!-- /.widget-actions -->
                </div>
                <!-- /.widget-heading -->
                <div class="widget-body">
                    <div class="tabs">
                        @include('admin.settings.hotels.partials.nav')
                        <!-- /.nav-tabs -->
                        <div class="tab-content">
                            <form class="form-horizontal" data-pjax  action="{{route('roomtypes.update',$roomtype->id)}}" method="POST" autocomplete="off">
                                {{ method_field('PATCH') }}
                                @include('admin.settings.hotels.roomtypes._form')
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label text-right"></label>
                                    <div class="col-md-5">
                                        @can('add_roomtypes')
                                            <button type="submit" name="update" value="update" class="btn btn-success btn-rounded btn-sm"> Update</button>
                                        @endcan
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.widget-body -->
            </div>
            <!-- /.widget-bg -->
        </div>
        <!-- /.widget-holder -->
    </div>
    <!-- /.row -->
</div>
@endsection

