{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Name :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="" type="text" placeholder="Name" name="name" value="{{ isset($roomtype) ? $roomtype->name : old('name') }}">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Description :</label>
    <div class="col-md-7">
        <textarea class="form-control form {{ $errors->has('description') ? ' is-invalid' : '' }}" id="description" placeholder="Description" name="description" value="">{{ isset($roomtype) ? $roomtype->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

