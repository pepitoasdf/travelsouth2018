{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="name">name :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" placeholder="" type="text" placeholder="name" name="name" value="{{ isset($type) ? $type->name : old('name') }}">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

