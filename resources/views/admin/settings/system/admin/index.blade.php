@extends('admin.layouts.master')

@section('title', 'Admin')

@section('header-title')
   <i class="list-icon feather feather-settings"></i> Settings
@endsection

@section('header-body')
    <span>System</span>
    <i class="feather feather-chevron-right"></i>
    <span> Admin</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">Edit</h5>
                        <div class="widget-actions">
                            
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.settings.system.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                    <form class="form-horizontal" data-pjax action="{{route('admin.store')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                        @include('admin.settings.system.admin._form')
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-right"></label>
                                            <div class="col-md-5">
                                                @can('add_admin')
                                                    @if(isset($admin))
                                                        <button type="submit" name="update" value="update" class="btn btn-success btn-rounded btn-sm"> Update</button>
                                                    @else
                                                        <button type="submit" name="save" value="save" class="btn btn-success btn-rounded btn-sm"> Submit</button>
                                                    @endif
                                                @endcan
                                            </div>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection