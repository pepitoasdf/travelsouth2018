<ul class="nav nav-tabs">
    <li class="nav-item">
    	<a class="nav-link {{ Request::is('admin/settings/system/admin*') ? 'active' : '' }}" href="{{ route('admin.index') }}" aria-expanded="true">
    		Admin
    	</a>
    </li>
    <li class="nav-item">
    	<a class="nav-link {{ Request::is('admin/settings/system/client*') ? 'active' : '' }}" href="{{ route('client.index') }}" aria-expanded="true">
    		Site
    	</a>
    </li>
</ul>