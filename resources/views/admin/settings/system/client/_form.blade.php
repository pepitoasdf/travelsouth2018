{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="title">Title :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" type="text" placeholder="title" name="title" value="{{ isset($client) ? $client->title : old('title') }}" required="">
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Description :</label>
    <div class="col-md-7">
        <textarea rows="8" class="form-control form {{ $errors->has('description') ? ' is-invalid' : '' }}" id="description"  placeholder="description" name="description">{{ isset($client) ? $client->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="image-logo">Logo :</label>
    <div class="col-md-7">
        <input name="logo" id="image-logo" accept="image/*" type="file"  />
        @if(isset($client))
            <div class="list">
                <span><img class="thumb-preview" src="{{ asset($client->logo) }}" title="{{ asset($client->name) }}"/></span>
            </div>
        @else
            <div class="list"></div>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="image-background">Background Image :</label>
    <div class="col-md-7">
        <input name="background" id="image-background" accept="image/*" type="file"  />
        @if(isset($client))
            <div class="list">
                <span><img class="thumb-preview" src="{{ asset($client->background) }}" title="{{ asset($client->name) }}"/></span>
            </div>
        @else
            <div class="list"></div>
        @endif
            
    </div>
</div>
@push('append_js')
    <script type="text/javascript">
        function handleFileSelect(evt) {
            var files = evt.target.files; // FileList object
            var that = this;
            $(this).closest('div').find('div.list').empty();

            // Loop through the FileList and render image files as thumbnails.
            for (var i = 0, f; f = files[i]; i++) {

                // Only process image files.
                if (!f.type.match('image.*')) {
                    continue;
                }

                var reader = new FileReader();

                // Closure to capture the file information.
                reader.onload = (function(theFile) {
                    return function(e) {
                        $(that).closest('div').find('div.list').html('<span><img class="thumb-preview" src="'+ e.target.result+'" title="'+ escape(theFile.name)+ '"/></span>');
                    };
                })(f);

                // Read in the image file as a data URL.
                reader.readAsDataURL(f);
            }
        }


        $('#image-logo').on('change',handleFileSelect);
        $('#image-background').on('change',handleFileSelect);
        
    </script>
@endpush