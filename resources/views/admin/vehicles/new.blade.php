@extends('admin.layouts.master')

@section('title', 'Vehicles')

@section('header-title')
   <i class="list-icon feather feather-command"></i> Services
@endsection

@section('header-body')
    <span>Vehicles</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">Create</h5>
                        <div class="widget-actions">
                            @include('partials.add-list',['entity' => 'vehicles','reference' => 'Vehicles'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs tabs-vertical tabs-vertical-right clearfix">
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <div class="tab-pane active">
                                    <form class="form-horizontal" data-pjax action="{{route('vehicles.store')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                        @include('admin.vehicles._form')
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-right"></label>
                                            <div class="col-md-5">
                                                @can('add_vehicles')
                                                    <button type="submit" name="save" value="save" class="btn btn-success btn-rounded btn-sm"> Submit</button>
                                                @endcan
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection
