<ul class="nav nav-tabs">
    @can('edit_vehicles')
        <li class="nav-item">
            <a class="nav-link  {{ Request::is('admin/services/vehicles/'.$vehicle->id.'/edit*') ? 'active' : '' }}" href="{{route('vehicles.edit',$vehicle->id)}}" aria-expanded="true">
                Profile
            </a>
        </li>
    @endcan
    @can('view_vgalleries')
        <li class="nav-item ">
            <a class="nav-link {{ Request::is('admin/services/vehicles/'.$vehicle->id.'/vgalleries*') ? 'active' : '' }}" href="{{ route('vgalleries.index',$vehicle->id) }}" aria-expanded="true">Gallery</a>
        </li>
    @endcan
    @can('view_vrates')
        <li class="nav-item ">
            <a class="nav-link {{ Request::is('admin/services/vehicles/'.$vehicle->id.'/vrates*') ? 'active' : '' }}" href="{{ route('vrates.index',$vehicle->id) }}" aria-expanded="true">Rates</a>
        </li>
    @endcan
    @can('view_vvideos')
        <li class="nav-item ">
            <a class="nav-link {{ Request::is('admin/services/vehicles/'.$vehicle->id.'/vvideos*') ? 'active' : '' }}" href="{{ route('vvideos.index',$vehicle->id) }}" aria-expanded="true">Videos</a>
        </li>
    @endcan
</ul>