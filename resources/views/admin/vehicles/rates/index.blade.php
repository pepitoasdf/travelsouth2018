@extends('admin.layouts.master')

@section('title', 'Vehicles - Rates')

@section('header-title')
   <i class="list-icon feather feather-command"></i> Services
@endsection

@section('header-body')
    <span>Vehicles</span>
    <i class="feather feather-chevron-right"></i>
    <span> Rates</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">List <span class="text-success">( {{  strtoupper($vehicle->name) }} )</span></h5>
                        <div class="widget-actions">
                           @include('partials.add-list',['entity' => 'vehicles','reference' => 'Vehicles'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.vehicles.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <form class="form-horizontal" data-pjax action="{{route('vrates.store',['id' => $vehicle->id])}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                    
                                    @include('admin.vehicles.rates._form')
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection