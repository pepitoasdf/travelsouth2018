{{ csrf_field() }}
<h6 class="form-section">Vehicle Rate</h6>
<div class="table-responsive">
    <table class="table table-striped table-hover vehicle-rate" id="data-table">
        <thead>
        <tr>
            <th>Rate Per Day</th>
            <th>Rate Per Hour</th>
            <th>Effective Date</th>
            <th>Status</th>
            <th></th>
        </tr>
        <tr>
            <th>
                <input type="number"  class="form-control form rate_day">
            </th>
            <th>
                <input type="number"  class="form-control form rate_hour">
            </th>
            <th>
                <input type="text"  class="form-control form effective_date datepicker">
            </th>
            <th></th>
            <th>
                <a href="#" class="btn btn-success btn-rounded btn-sm add-rate">
                    <i class="list-icon feather feather-plus"></i>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($vehicle->rates as $item)
            <tr>
                <td>
                    {{  number_format($item->rate_day,2) }}
                    <input type="hidden" name="rate_day[]" value="{{ $item->rate_day }}">
                </td>
                <td>
                    {{  number_format($item->rate_hour,2) }}
                    <input type="hidden" name="rate_hour[]" value="{{ $item->rate_hour }}">
                    <input type="hidden" name="rate_id[]" value="{{ $item->id }}">
                </td>
                <td>
                    {{ $item->effective_date }}
                    <input type="hidden" name="effective_date[]" value="{{ $item->effective_date }}">
                </td>
                <td>
                    @if(isset($vehicle->getrate))
                        @if($vehicle->getrate->id == $item->id)
                            <span class="badge bg-success ">Active</span>
                        @else
                            <span class="badge bg-warning ">Inactive</span>
                        @endif
                    @endif
                </td>
                <td>
                    <a href="#" class="btn btn-danger btn-rounded btn-sm remove-rate">
                    <i class="list-icon feather feather-x"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<h6 class="form-section">Driver Rate </h6>
<div class="table-responsive">
    <table class="table table-striped table-hover driver-rate" id="data-table">
        <thead>
            <tr>
                <th>Rate Per Day</th>
                <th>Rate Per Hour</th>
                <th>Effective Date</th>
                <th>Status</th>
                <th></th>
            </tr>
            <tr>
                <th>
                    <input type="number"  class="form-control form rate_day2">
                </th>
                <th>
                    <input type="number"  class="form-control form rate_hour2">
                </th>
                <th>
                    <input type="text"  class="form-control form effective_date2 datepicker">
                </th>
                <th></th>
                <th>
                    <a href="#" class="btn btn-success btn-rounded btn-sm add-rate2">
                        <i class="list-icon feather feather-plus"></i>
                    </a>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($vehicle->driverrates as $item)
                <tr>
                    <td>
                        {{  number_format($item->rate_day,2) }}
                        <input type="hidden" name="rate_day2[]" value="{{ $item->rate_day }}">
                    </td>
                    <td>
                        {{  number_format($item->rate_hour,2) }}
                        <input type="hidden" name="rate_hour2[]" value="{{ $item->rate_hour }}">
                        <input type="hidden" name="rate_id2[]" value="{{ $item->id }}">
                    </td>
                    <td>
                        {{ $item->effective_date }}
                        <input type="hidden" name="effective_date2[]" value="{{ $item->effective_date }}">
                    </td>
                    <td>
                        @if(isset($vehicle->drivergetrate))
                            @if($vehicle->drivergetrate->id == $item->id)
                                <span class="badge bg-success ">Active</span>
                            @else
                                <span class="badge bg-warning ">Inactive</span>
                            @endif
                        @endif
                    </td>
                    <td>
                        <a href="#" class="btn btn-danger btn-rounded btn-sm remove-rate2">
                        <i class="list-icon feather feather-x"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3">
                    <br>
                    <br>
                    @can('edit_vrates')
                        <button type="submit" name="save" value="save" class="btn btn-success btn-rounded btn-sm "> Submit</button>
                    @endcan
                </td>
            </tr>
        </tfoot>
    </table>
@push('append_js')
    <script type="text/javascript">
        $('.add-rate').click(function(e){
            addRate(this, '.vehicle-rate', '.rate_day','.rate_hour', '.effective_date', e, '');
        });
        $('.add-rate2').click(function(e){
            addRate(this, '.driver-rate', '.rate_day2','.rate_hour2', '.effective_date2', e, 2);
        });

        $('table.vehicle-rate').on('click', '.remove-rate', function(e){
            removeRate(this, '.vehicle-rate', 'input[name="rate_id[]"]', e, '');
        });
        $('table.driver-rate').on('click','.remove-rate2', function(e){
            removeRate(this, '.driver-rate', 'input[name="rate_id2[]"]', e, 2);
        });

        function addRate(that, base, rate_dayEl, rate_hourEl, effective_dateEl, event, uid){
            event.preventDefault();
            var rate_day = $(that).closest('tr').find(rate_dayEl).val();
            var rate_hour = $(that).closest('tr').find(rate_hourEl).val();
            var date = $(that).closest('tr').find(effective_dateEl).val();
            
            if(!rate_day){
                alert('Rate Per Day is required.');
                $(rate_dayEl).focus();
                return false;
            }
            else if(!rate_hour){
                alert('Rate Per Hour is required.');
                $(rate_hourEl).focus();
                return false;
            }
            else if(!date){
                alert('Effective Date is required.');
                $(effective_dateEl).focus();
                return false
            }

            $('table'+base+' tbody').append(
                '<tr>\
                    <td>\
                        '+rate_day+'\
                        <input type="hidden" name="rate_day'+uid+'[]" value="'+rate_day+'">\
                    </td>\
                    <td>\
                        '+rate_hour+'\
                        <input type="hidden" name="rate_hour'+uid+'[]" value="'+rate_hour+'">\
                        <input type="hidden" name="rate_id'+uid+'[]" value="0">\
                    </td>\
                    <td>\
                        '+date+'\
                        <input type="hidden" name="effective_date'+uid+'[]" value="'+date+'">\
                    </td>\
                    <td>\
                        <span class="badge bg-default"> ... </span>\
                    </td>\
                    <td>\
                        <a href="#" class="btn btn-danger btn-rounded btn-sm remove-rate'+uid+'">\
                        <i class="list-icon feather feather-x"></i>\
                        </a>\
                    </td>\
                </tr>'
            );
            $(that).closest('tr').find(rate_dayEl).val('');
            $(that).closest('tr').find(rate_hourEl).val('');
            $(that).closest('tr').find(effective_dateEl).val('');

             $(that).closest('tr').find(rate_dayEl).focus();
        }

        function removeRate(that,base, rateid,event,uid){
            event.preventDefault();
            var refid = $(that).closest('tr').find(rateid).val();
            $(that).closest('tr').remove();
            $('table'+base+' tbody').append(
                '<input type="hidden" name="remove_id'+uid+'[]" value="'+refid+'">'
            );
        }
    </script>
@endpush