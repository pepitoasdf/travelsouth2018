{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="name">Name :</label>
    <div class="col-md-7">
        <input required="" class="form-control form {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" type="text" placeholder="Name" name="name" value="{{ isset($vehicle) ? $vehicle->name : old('name') }}">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Description :</label>
    <div class="col-md-7">
        <textarea rows="8" class="form-control form {{ $errors->has('description') ? ' is-invalid' : '' }}" id="description" placeholder="description" name="description">{{ isset($vehicle) ? $vehicle->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="pricing">Pricing :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('pricing') ? ' is-invalid' : '' }}" id="pricing" type="text" placeholder="pricing" name="pricing" value="{{ isset($vehicle) ? $vehicle->pricing : old('pricing') }}">
        @if ($errors->has('pricing'))
            <span class="help-block">
                <strong>{{ $errors->first('pricing') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="color">Color :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('color') ? ' is-invalid' : '' }}" id="color" type="text" placeholder="Color" name="color" value="{{ isset($vehicle) ? $vehicle->color : old('color') }}">
        @if ($errors->has('color'))
            <span class="help-block">
                <strong>{{ $errors->first('color') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="plate_no">Plate No. :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('plate_no') ? ' is-invalid' : '' }}" id="plate_no" type="text" placeholder="Plate no." name="plate_no" value="{{ isset($vehicle) ? $vehicle->plate_no : old('plate_no') }}">
        @if ($errors->has('plate_no'))
            <span class="help-block">
                <strong>{{ $errors->first('plate_no') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="year_model">Year Model :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('year_model') ? ' is-invalid' : '' }}" id="year_model" type="text" placeholder="Year Model" name="year_model" value="{{ isset($vehicle) ? $vehicle->year_model : old('year_model') }}">
        @if ($errors->has('year_model'))
            <span class="help-block">
                <strong>{{ $errors->first('year_model') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="mv_file_no">MV File No. :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('mv_file_no') ? ' is-invalid' : '' }}" id="mv_file_no" type="text" placeholder="MV File No." name="mv_file_no" value="{{ isset($vehicle) ? $vehicle->mv_file_no : old('mv_file_no') }}">
        @if ($errors->has('mv_file_no'))
            <span class="help-block">
                <strong>{{ $errors->first('mv_file_no') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="engine_no">Engine No. :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('engine_no') ? ' is-invalid' : '' }}" id="engine_no" type="text" placeholder="Engine No." name="engine_no" value="{{ isset($vehicle) ? $vehicle->engine_no : old('engine_no') }}">
        @if ($errors->has('engine_no'))
            <span class="help-block">
                <strong>{{ $errors->first('engine_no') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="no_of_wheel">No. of Wheel :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('no_of_wheel') ? ' is-invalid' : '' }}" id="no_of_wheel" type="text" placeholder="No. of Wheel " name="no_of_wheel" value="{{ isset($vehicle) ? $vehicle->no_of_wheel : old('no_of_wheel') }}">
        @if ($errors->has('no_of_wheel'))
            <span class="help-block">
                <strong>{{ $errors->first('no_of_wheel') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="transmision">Transmission :</label>
    <div class="col-md-7">
        <select class="form-control form {{ $errors->has('transmision') ? ' is-invalid' : '' }}"  id="transmision" name="transmision">
            @if(isset($vehicle))
                <option value="Automatic" {{ $vehicle->transmission == 'Automatic' ? 'selected' : '' }}>Automatic</option>
                <option value="Manual" {{ $vehicle->transmission  == 'Manual' ? 'selected' : '' }}>Manual</option>
            @else
                <option value="Automatic" {{ old('transmision') == 'Automatic' ? 'selected' : '' }}>Automatic</option>
                <option value="Manual" {{ old('transmision') == 'Manual' ? 'selected' : '' }}>Manual</option>
            @endif
        </select>
        @if ($errors->has('transmision'))
            <span class="help-block">
                <strong>{{ $errors->first('transmision') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="aircon">Aircon :</label>
    <div class="col-md-7">
        <select class="form-control form {{ $errors->has('aircon') ? ' is-invalid' : '' }}"  id="aircon" name="aircon">
            @if(isset($vehicle))
                <option value="Yes" {{ $vehicle->transmission == 'Yes' ? 'selected' : '' }}>Yes</option>
                <option value="No" {{ $vehicle->transmission  == 'No' ? 'selected' : '' }}>No</option>
            @else
                <option value="Yes" {{ old('aircon') == 'Yes' ? 'selected' : '' }}>Yes</option>
                <option value="No" {{ old('aircon') == 'No' ? 'selected' : '' }}>No</option>
            @endif
        </select>
        @if ($errors->has('aircon'))
            <span class="help-block">
                <strong>{{ $errors->first('aircon') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="seats">No. of Seats :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('seats') ? ' is-invalid' : '' }}" id="seats" type="text" placeholder="No. of Seats " name="seats" value="{{ isset($vehicle) ? $vehicle->seats : old('seats') }}">
        @if ($errors->has('seats'))
            <span class="help-block">
                <strong>{{ $errors->first('seats') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="doors">No. of Doors :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('doors') ? ' is-invalid' : '' }}" id="doors" type="text" placeholder="No. of Doors " name="doors" value="{{ isset($vehicle) ? $vehicle->doors : old('doors') }}">
        @if ($errors->has('doors'))
            <span class="help-block">
                <strong>{{ $errors->first('doors') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="feature_id">Features :</label>
    <div class="col-md-7">
        <?php
            if(isset($vehicle)){
                $class = $vehicle->features()->pluck('feature_id')->toArray();
            }
        ?>
        <div class="pd-t-10 pd-b-10 custom-scroll-content scrollbar-enabled" style="overflow-y: scroll; max-height: 400px;">
            @foreach(App\VehicleFeature::all() as $feature)
                @if(isset($vehicle))
                    @if(in_array($feature->id, $class))
                        <div class="checkbox checkbox-primary bw-3 mail-select-checkbox pd-t-0 pd-b-0">
                            <label class="checkbox-checked">
                                <input type="checkbox" checked="" name="feature_id[]" value="{{ $feature->id }}" />
                                <span class="label-text">{{ $feature->description }}</span>
                            </label>
                        </div>
                    @else
                        <div class="checkbox checkbox-primary bw-3 mail-select-checkbox pd-t-0 pd-b-0">
                            <label class="checkbox-checked">
                                <input type="checkbox" name="feature_id[]" value="{{ $feature->id }}" />
                                <span class="label-text">{{ $feature->description }}</span>
                            </label>
                        </div>
                    @endif
                @else
                    <div class="checkbox checkbox-primary bw-3 mail-select-checkbox pd-t-0 pd-b-0">
                        <label class="checkbox-checked">
                            <input type="checkbox" name="feature_id[]" value="{{ $feature->id }}" />
                            <span class="label-text">{{ $feature->description }}</span>
                        </label>
                    </div>
                @endif
            @endforeach
        </div>
        @if ($errors->has('feature_id'))
            <span class="help-block">
                <strong>{{ $errors->first('feature_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<hr>
<h6 class="form-section">Rates</h6>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="rate_day">Rate Per Day:</label>
    <div class="col-md-7">
        <input required="" class="form-control form {{ $errors->has('rate_day') ? ' is-invalid' : '' }}" id="rate_day" type="number" placeholder="0.00" name="rate_day" value="{{ isset($vehicle->getrate) ? $vehicle->getrate->rate_day : old('rate_day') }}">
        @if ($errors->has('rate_day'))
            <span class="help-block">
                <strong>{{ $errors->first('rate_day') }}</strong>
            </span>
        @endif
    </div>
    @if(isset($vehicle->getrate))
        <input type="hidden" name="rate_id" value="{{ $vehicle->getrate->id }}">
    @endif
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="rate_hour">Rate Per Hour:</label>
    <div class="col-md-7">
        <input required="" class="form-control form {{ $errors->has('rate_hour') ? ' is-invalid' : '' }}" id="rate_hour" type="number" placeholder="0.00" name="rate_hour" value="{{ isset($vehicle->getrate) ? $vehicle->getrate->rate_hour : old('rate_hour') }}">
        @if ($errors->has('rate_hour'))
            <span class="help-block">
                <strong>{{ $errors->first('rate_hour') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="effective_date">Effective Date:</label>
    <div class="col-md-7">
        <input class="datepicker form-control form {{ $errors->has('effective_date') ? ' is-invalid' : '' }}" id="effective_date" type="text" name="effective_date" value="{{ isset($vehicle->getrate) ? $vehicle->getrate->effective_date : old('effective_date') }}">
        @if ($errors->has('effective_date'))
            <span class="help-block">
                <strong>{{ $errors->first('effective_date') }}</strong>
            </span>
        @endif
    </div>
</div>
@include('partials.status', ['data' => (isset($vehicle) ? $vehicle : null)])
<hr>
<h6 class="form-section">Upload Images</h6>
@include('partials.photo')
