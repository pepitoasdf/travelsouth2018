@extends('admin.layouts.master')

@section('title','Dashboard')

@section('header-title')
    <i class="list-icon feather feather-home"></i> Dashboard
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">Blank Starter Page</h5>
                        <div class="widget-actions">
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <p>WELCOME TO TRAVEL SOUTH</p>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection