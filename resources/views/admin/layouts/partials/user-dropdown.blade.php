<ul class="nav navbar-nav">
    <li class="dropdown">
        <a href="javascript:void(0);" class="dropdown-toggle dropdown-toggle-user ripple" data-toggle="dropdown">
            <span class="avatar thumb-xs2">
                @if(auth()->user()->image)
                    <img src="{{ asset('admin/images/uploads/users/'.auth()->user()->image) }}" class="rounded-circle" alt="">
                @else
                    <img src="{{ asset('admin/images/defaults/user-profile/avatar.png') }}" class="rounded-circle" alt="">
                @endif
                <i class="feather feather-chevron-down list-icon"></i>
            </span>
        </a>
        <div class="dropdown-menu dropdown-left dropdown-card dropdown-card-profile animated flipInY">
            <div class="card">
                <ul class="list-unstyled card-body">
                    <li>
                        <a href="#"><span class="align-middle">Profile</span></a>
                    </li>
                    <li>
                        <a href="#"><span class="align-middle">Change Password</span></a>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            <span class="align-middle">Sign Out</span>
                        </a>
                    </li>
                </ul>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.dropdown-card-profile -->
    </li>
    <!-- /.dropdown -->
</ul>