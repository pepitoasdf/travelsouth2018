<div class="navbar-header">
    <a href="{{ route('dashboard.index') }}" class="navbar-brand">
        <img class="logo-expand" alt="" src="{{ asset('admin/images/defaults/logo-dark.png') }}">
        <img class="logo-collapse" alt="" src="{{ asset('admin/images/defaults/logo-collapse.png') }}">
        <!-- <p>BonVue</p> -->
    </a>
</div>