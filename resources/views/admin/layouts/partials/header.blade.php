<div class="row page-title clearfix">
    <div class="page-title-left">
        <h6 class="page-title-heading mr-0 mr-r-5">
            @yield('header-title')
        </h6>
        <p class="page-title-description mr-0 d-none d-md-inline-block">
            @yield('header-body')
        </p>
    </div>
    <!-- /.page-title-left -->
    <div class="page-title-right d-none d-sm-inline-flex">
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item">
                <a href="index.html">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Blank Page</li> -->
             @yield('bread-item')
        </ol>
    </div>
    <!-- /.page-title-right -->
</div>