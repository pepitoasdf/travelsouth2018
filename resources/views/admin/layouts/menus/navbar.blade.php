<nav class="navbar">
      <!-- Logo Area -->
      @include('admin.layouts.partials.logo')
      <!-- /.navbar-header -->
      <!-- Left Menu & Sidebar Toggle -->
      <ul class="nav navbar-nav">
          <li class="sidebar-toggle dropdown">
              <a href="javascript:void(0)" class="ripple"><i class="feather feather-menu list-icon fs-20"></i></a>
          </li>
      </ul>
      <!-- /.navbar-left -->
      <!-- Search Form -->
      @include('admin.layouts.partials.search')
      <!-- /.navbar-search -->
      <div class="spacer"></div>
      <!-- /.navbar-right -->
      <!-- User Image with Dropdown -->
      @include('admin.layouts.partials.user-dropdown')
      <!-- /.navbar-nav -->
</nav>