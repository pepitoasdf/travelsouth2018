<aside class="site-sidebar scrollbar-enabled" data-suppress-scroll-x="true">
    <!-- User Details -->
    <div class="side-user d-none">
        <div class="col-sm-12 text-center p-0 clearfix">
            <div class="d-inline-block pos-relative mr-b-10">
                <figure class="thumb-sm mr-b-0 user--online">
                    <img src="{{ asset('admin/images/uploads/users/user1.jpg') }}" class="rounded-circle" alt="">
                </figure>
                <a href="page-profile.html" class="text-muted side-user-link">
                    <i class="feather feather-settings list-icon"></i>
                </a>
            </div>
            <!-- /.d-inline-block -->
            <div class="lh-14 mr-t-5">
                <a href="page-profile.html" class="hide-menu mt-3 mb-0 side-user-heading fw-500">Scott Adams</a>
                <br>
                <small class="hide-menu">Developer</small>
            </div>
        </div>
        <!-- /.col-sm-12 -->
    </div>
    <!-- /.side-user -->
    <!-- Call to Action -->
    <div class="side-content mr-t-30 mr-lr-15">
        <a class="btn btn-block btn-success ripple fw-600" href="{{ url('/') }}" target="_blank">
            <i class="fa fa-plus-square-o mr-1 mr-0-rtl ml-1-rtl"></i> Visit Website
        </a>
    </div>
    <!-- Sidebar Menu -->
    <nav class="sidebar-nav">
        <ul class="nav in side-menu">
            @can('view_dashboard')
                <li class="{{ Request::is('admin/dashboard') ? 'active' : '' }}">
                    <a href="{{ route('dashboard.index') }}">
                        <i class="list-icon feather feather-home"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>
            @endcan
            @can('view_users')
                <li class="{{ Request::is('admin/users*') ? 'active' : '' }}">
                    <a href="{{ route('users.index') }}">
                        <i class="list-icon feather feather-users"></i>
                        <span class="hide-menu">Users</span>
                    </a>
                </li>
            @endcan
            @can('view_roles')
                <li class="{{ Request::is('admin/roles*') ? 'active' : '' }}">
                    <a href="{{ route('roles.index') }}">
                        <i class="list-icon feather feather-lock"></i>
                        <span class="hide-menu">Roles</span>
                    </a>
                </li>
            @endcan
            @can('view_bookings')
                <li class="{{ Request::is('admin/booking*') ? 'active' : '' }}">
                    <a href="{{ route('bookings.index') }}">
                        <i class="list-icon feather feather-book"></i>
                        <span class="hide-menu">Booking</span>
                    </a>
                </li>
            @endcan
            @if(auth()->user()->can('view_hotels') || auth()->user()->can('view_vehicles') || auth()->user()->can('view_packages'))
                <li class="menu-item-has-children {{ Request::is('admin/services*') ? 'active' : '' }}">
                    <a href="#">
                        <i class="list-icon feather feather-command"></i>
                        <span class="hide-menu">Services</span>
                    </a>
                    <ul class="list-unstyled sub-menu">
                        @can('view_hotels')
                            <li class="{{ Request::is('admin/services/hotels*') ? 'active' : '' }}">
                                <a href="{{ route('hotels.index') }}">Hotels</a>
                            </li>
                        @endcan
                        @can('view_vehicles')
                            <li class="{{ Request::is('admin/services/vehicles*') ? 'active' : '' }}">
                                <a href="{{ route('vehicles.index') }}">Vehicles</a>
                            </li>
                        @endcan
                        @can('view_packages')
                            <li class="{{ Request::is('admin/services/packages*') ? 'active' : '' }}">
                                <a href="{{ route('packages.index') }}">Packages</a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endif
            @if(auth()->user()->can('view_contacts') || auth()->user()->can('view_abouts'))
                <li class="menu-item-has-children {{ Request::is('admin/pages*') ? 'active' : ''}}">
                    <a href="#">
                        <i class="list-icon feather feather-globe"></i>
                        <span class="hide-menu">Pages</span>
                    </a>
                    <ul class="list-unstyled sub-menu">
                        @if(auth()->user()->can('view_background'))
                            <li class="{{ Request::is('admin/pages/home/background*') ? 'active' : ''}}">
                                <a href="{{ route('background.index') }}">Home</a>
                            </li>
                        @elseif(auth()->user()->can('view_increadibleplaces'))
                            <li class="{{ Request::is('admin/pages/home/increadibleplaces*') ? 'active' : ''}}">
                                <a href="{{ route('increadibleplaces.index') }}">Home</a>
                            </li>
                        @elseif(auth()->user()->can('view_partners'))
                            <li class="{{ Request::is('admin/pages/home/partners*') ? 'active' : ''}}">
                                <a href="{{ route('partners.index') }}">Home</a>
                            </li>
                        @elseif(auth()->user()->can('view_popular'))
                            <li class="{{ Request::is('admin/pages/home/popular*') ? 'active' : ''}}">
                                <a href="{{ route('popular.index') }}">Home</a>
                            </li>
                        @elseif(auth()->user()->can('view_topdestinations'))
                            <li class="{{ Request::is('admin/pages/home/topdestinations*') ? 'active' : ''}}">
                                <a href="{{ route('topdestinations.index') }}">Home</a>
                            </li>
                        @elseif(auth()->user()->can('view_videos'))
                            <li class="{{ Request::is('admin/pages/home/videos*') ? 'active' : ''}}">
                                <a href="{{ route('videos.index') }}">Home</a>
                            </li>
                        @endif


                        @can('view_contacts')
                            <li class="{{ Request::is('admin/pages/contacts*') ? 'active' : ''}}">
                                <a href="{{ route('contacts.index') }}">Contacts</a>
                            </li>
                        @endcan
                        @can('view_abouts')
                            <li class="{{ Request::is('admin/pages/abouts*') ? 'active' : ''}}">
                                <a href="{{ route('abouts.index') }}">About</a>
                            </li>
                        @endcan
                        @if(auth()->user()->can('view_posts'))
                            <li class="menu-item-has-children {{ Request::is('admin/pages/blog*') ? 'active' : ''}}">
                                <a href="#">
                                    <span class="hide-menu">Blog</span>
                                </a>
                                <ul class="list-unstyled sub-menu">
                                    @can('view_posts')
                                        <li class="{{ Request::is('admin/pages/blog/posts*') ? 'active' : ''}}">
                                            <a href="{{ route('posts.index') }}">Post</a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
            @if(auth()->user()->can('view_facilities') || auth()->user()->can('view_hoteltypes') || auth()->user()->can('view_roomtypes') || auth()->user()->can('view_vfeatures') || auth()->user()->can('view_pfeatures') || auth()->user()->can('view_countries'))
                <li class="menu-item-has-children {{ Request::is('admin/settings*') ? 'active' : ''}}">
                    <a href="#">
                        <i class="list-icon feather feather-settings"></i>
                        <span class="hide-menu">Settings</span>
                    </a>
                    <ul class="list-unstyled sub-menu">
                        @if(auth()->user()->can('view_facilities') || auth()->user()->can('view_hoteltypes') || auth()->user()->can('view_roomtypes'))
                            <li class="menu-item-has-children {{ Request::is('admin/settings/hotels*') ? 'active' : ''}}">
                                <a href="#">Hotels</a>
                                <ul class="list-unstyled sub-menu">
                                    @can('view_facilities')
                                        <li class="{{ Request::is('admin/settings/hotels/facilities*') ? 'active' : ''}}">
                                            <a href="{{ route('facilities.index') }}">Facilities</a>
                                        </li>
                                    @endcan
                                    @can('view_hoteltypes')
                                        <li class="{{ Request::is('admin/settings/hotels/hoteltypes*') ? 'active' : ''}}">
                                            <a href="{{ route('hoteltypes.index') }}">Types</a>
                                        </li>
                                    @endcan
                                    @can('view_roomtypes')
                                        <li class="{{ Request::is('admin/settings/hotels/roomtypes*') ? 'active' : ''}}">
                                            <a href="{{ route('roomtypes.index') }}">Room Types</a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                        @endif
                        @if(auth()->user()->can('view_vfeatures'))
                            <li class="menu-item-has-children {{ Request::is('admin/settings/vehicles*') ? 'active' : ''}}">
                                <a href="#">Vehicles</a>
                                <ul class="list-unstyled sub-menu">
                                    @can('view_vfeatures')
                                        <li class="{{ Request::is('admin/settings/vehicles/vfeatures*') ? 'active' : ''}}">
                                            <a href="{{ route('vfeatures.index') }}">features</a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                        @endif
                        @if(auth()->user()->can('view_pfeatures'))
                            <li class="menu-item-has-children {{ Request::is('admin/settings/packages*') ? 'active' : ''}}">
                                <a href="#">Packages</a>
                                <ul class="list-unstyled sub-menu">
                                    @can('view_pfeatures')
                                        <li class="{{ Request::is('admin/settings/packages/pfeatures*') ? 'active' : ''}}">
                                            <a href="{{ route('pfeatures.index') }}">features</a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                        @endif
                        @if(auth()->user()->can('view_countries'))
                            <li class="{{ Request::is('admin/settings/places*') ? 'active' : ''}}">
                                <a href="{{ route('countries.index') }}">Places</a>
                            </li>
                        @elseif(auth()->user()->can('view_provinces'))
                            <li class="{{ Request::is('admin/settings/places*') ? 'active' : ''}}">
                                <a href="{{ route('provinces.index') }}">Places</a>
                            </li>
                        @elseif(auth()->user()->can('view_cities'))
                            <li class="{{ Request::is('admin/settings/places*') ? 'active' : ''}}">
                                <a href="{{ route('cities.index') }}">Places</a>
                            </li>
                        @endif
                        @if(auth()->user()->can('view_admin') || auth()->user()->can('view_client'))
                            <li class="menu-item-has-children {{ Request::is('admin/settings/system*') ? 'active' : ''}}">
                                <a href="#">System</a>
                                <ul class="list-unstyled sub-menu">
                                    @can('view_admin')
                                        <li class="{{ Request::is('admin/settings/system/admin*') ? 'active' : ''}}">
                                            <a href="{{ route('admin.index') }}">Admin</a>
                                        </li>
                                    @endcan
                                    @can('view_client')
                                        <li class="{{ Request::is('admin/settings/system/client*') ? 'active' : ''}}">
                                            <a href="{{ route('client.index') }}">Site</a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
        </ul>
        <!-- /.side-menu -->
    </nav>
    <!-- /.sidebar-nav -->
</aside>