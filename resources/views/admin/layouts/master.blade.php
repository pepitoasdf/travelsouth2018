
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" type="image/x-icon" sizes="16x16" href="{{ asset('favicon.ico') }}">
    <!-- <link rel="stylesheet" href="{{ asset('admin/css/pace.css') }}"> -->

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>
    <!-- CSS -->
    <link href="{{ asset('admin/css/googleapis/google.css?family=Montserrat:200,300,400,500,600%7CRoboto:400') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/feather-icons/feather.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/css/perfect-scrollbar.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/toastr/build/toastr.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/font-awesome/css/animated.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/switchery/dist/switchery.min.css') }}" rel="stylesheet" >
    <link href="{{ asset('plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" >
    <link href="{{ asset('plugins/bootstrap-clockpicker/bootstrap-clockpicker.min.css') }}" rel="stylesheet" >
    <link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" >
    <link href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" >
    @stack('append_css')
    <!-- <link href="{{ asset('plugins/nprogress/nprogress.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/css/custom.css') }}" rel="stylesheet" type="text/css">
    <!-- Head Libs -->
    <script src="{{ asset('admin/js/modernizr.min.js') }}"></script>
    <!-- <script data-pace-options='{ "ajax": false, "selectors": [ "img" ]}' src="{{ asset('admin/js/pace.min.js') }}"></script> -->
</head>

<body class="sidebar-dark sidebar-expand navbar-brand-dark header-light" id="htmlbody">
    <div id="wrapper" class="wrapper">
        <!-- HEADER & TOP NAVIGATION -->
        @include('admin.layouts.menus.navbar')
        <!-- /.navbar -->
        <div class="content-wrapper">
            <!-- SIDEBAR -->
            @include('admin.layouts.menus.sidebar')
            <!-- /.site-sidebar -->
            <main class="main-wrapper clearfix">
                <!-- Page Title Area -->
                @include('admin.layouts.partials.header')
                <!-- /.page-title -->
                <!-- =================================== -->
                <!-- Different data widgets ============ -->
                <!-- =================================== -->
                @yield('content')
                <!-- /.widget-list -->
            </main>
            <!-- /.main-wrappper -->
        </div>
        <!-- /.content-wrapper -->
        <!-- FOOTER -->
        @include('admin.layouts.partials.footer')
    </div>
    <!--/ #wrapper -->
</body>
<!-- Scripts -->
<script src="{{ asset('admin/js/jquery.min.js') }}"></script>
<script src="{{ asset('admin/js/popper.min.js') }}"></script>
<script src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin/js/metisMenu.min.js') }}"></script>
<script src="{{ asset('admin/js/perfect-scrollbar.jquery.js') }}"></script>
<script src="{{ asset('plugins/nprogress/jquery.pjax.js')}}"></script>
<script src="{{ asset('plugins/nprogress/nprogress.js')}}"></script>
<script src="{{ asset('plugins/bootstrap-clockpicker/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}"></script>
<script src="{{ asset('plugins/switchery/dist/switchery.min.js') }}"></script>
<script src="{{ asset('plugins/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('plugins/toastr/build/toastr.min.js') }}"></script>
<script src="{{ asset('admin/js/theme.js') }}"></script>
<script src="{{ asset('admin/js/custom.js') }}"></script>
<script src="{{ asset('admin/js/custom2.js') }}"></script>
@include('partials.alert-toastr')
@stack('append_js')
</html>