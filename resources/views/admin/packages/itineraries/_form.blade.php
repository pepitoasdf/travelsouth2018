{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="day">Day :</label>
    <div class="col-md-7">
        <input required="" class="form-control form {{ $errors->has('day') ? ' is-invalid' : '' }}" id="day" type="text" placeholder="day" name="day" value="{{ isset($itinerary) ? $itinerary->day : old('day') }}">
        @if ($errors->has('day'))
            <span class="help-block">
                <strong>{{ $errors->first('day') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="title">Title :</label>
    <div class="col-md-7">
        <input required="" class="form-control form {{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" type="text" placeholder="title" name="title" value="{{ isset($itinerary) ? $itinerary->title : old('title') }}">
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Description :</label>
    <div class="col-md-7">
        <textarea rows="8" class="form-control form {{ $errors->has('description') ? ' is-invalid' : '' }}" required="" id="description" placeholder="description" name="description">{{ isset($itinerary) ? $itinerary->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

<hr>
<h6 class="form-section">Upload Images</h6>
@include('partials.photo')