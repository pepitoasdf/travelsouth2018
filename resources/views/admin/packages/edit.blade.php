@extends('admin.layouts.master')

@section('title', 'Packages')

@section('header-title')
   <i class="list-icon feather feather-settings"></i> Services
@endsection

@section('header-body')
    <span>Packages</span>
@endsection

@section('content')
<div class="widget-list">
    <div class="row">
        <div class="widget-holder col-md-12">
            <div class="widget-bg">
                <div class="widget-heading widget-heading-border">
                    <h5 class="widget-title">Edit <span class="text-success">( {{  strtoupper($package->name) }} )</span></h5>
                    <div class="widget-actions">
                        @include('partials.add-list',['entity' => 'packages','reference' => 'Packages'])
                    </div>
                    <!-- /.widget-actions -->
                </div>
                <!-- /.widget-heading -->
                <div class="widget-body">
                    <div class="tabs">
                        @include('admin.packages.partials.nav')
                        <!-- /.nav-tabs -->
                        <div class="tab-content">
                            <form class="form-horizontal" data-pjax action="{{route('packages.update',$package->id)}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                {{ method_field('PATCH') }}
                                @include('admin.packages._form')
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label text-right"></label>
                                    <div class="col-md-5">
                                        @can('add_packages')
                                            <button type="submit" name="update" value="update" class="btn btn-success btn-rounded btn-sm"> Update</button>
                                        @endcan
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.widget-body -->
            </div>
            <!-- /.widget-bg -->
        </div>
        <!-- /.widget-holder -->
    </div>
    <!-- /.row -->
</div>
@endsection

