<ul class="nav nav-tabs">
    @can('edit_packages')
        <li class="nav-item">
            <a class="nav-link {{ Request::is('admin/services/packages/'.$package->id.'/edit*') ? 'active' : '' }}" href="{{route('packages.edit',$package->id)}}" aria-expanded="true">
               Profile
            </a>
        </li>
    @endcan
    @can('view_pgalleries')
        <li class="nav-item">
            <a class="nav-link  {{ Request::is('admin/services/packages/'.$package->id.'/pgalleries*') ? 'active' : '' }}" href="{{ route('pgalleries.index',$package->id) }}" aria-expanded="true">Gallery</a>
        </li>
    @endcan
    @can('view_prates')
        <li class="nav-item">
            <a class="nav-link  {{ Request::is('admin/services/packages/'.$package->id.'/prates*') ? 'active' : '' }}" href="{{ route('prates.index',$package->id) }}" aria-expanded="true">Rates</a>
        </li>
    @endcan
    @can('view_pitineraries')
        <li class="nav-item ">
            <a class="nav-link {{ Request::is('admin/services/packages/'.$package->id.'/pitineraries*') ? 'active' : '' }}" href="{{ route('pitineraries.index',$package->id) }}" aria-expanded="true">Itineraries</a>
        </li>
    @endcan
    @can('view_pguides')
        <li class="nav-item ">
            <a class="nav-link {{ Request::is('admin/services/packages/'.$package->id.'/pguides*') ? 'active' : '' }}" href="{{ route('pguides.index',$package->id) }}" aria-expanded="true">Guides</a>
        </li>
    @endcan
    @can('view_pvideos')
        <li class="nav-item ">
            <a class="nav-link {{ Request::is('admin/services/packages/'.$package->id.'/pvideos*') ? 'active' : '' }}" href="{{ route('pvideos.index',$package->id) }}" aria-expanded="true">Videos</a>
        </li>
    @endcan
</ul>