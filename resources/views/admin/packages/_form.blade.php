{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="name">Name :</label>
    <div class="col-md-7">
        <input required="" class="form-control form {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" type="text" placeholder="Name" name="name" value="{{ isset($package) ? $package->name : old('name') }}">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Description :</label>
    <div class="col-md-7">
        <textarea rows="8" class="form-control form {{ $errors->has('description') ? ' is-invalid' : '' }}" required="" id="description" placeholder="description" name="description">{{ isset($package) ? $package->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="pricing">Pricing :</label>
    <div class="col-md-7">
        <input required="" class="form-control form {{ $errors->has('pricing') ? ' is-invalid' : '' }}" id="pricing" type="text" placeholder="Pricing Title" name="pricing" value="{{ isset($package) ? $package->pricing : old('pricing') }}">
        @if ($errors->has('pricing'))
            <span class="help-block">
                <strong>{{ $errors->first('pricing') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="duration">Package Duration :</label>
    <div class="col-md-5">
        <input required="" class="form-control form {{ $errors->has('duration') ? ' is-invalid' : '' }}" id="duration" type="text" placeholder="0.00" name="duration" value="{{ isset($package) ? $package->duration : old('duration') }}">
        @if ($errors->has('duration'))
            <span class="help-block">
                <strong>{{ $errors->first('duration') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-md-2 col-form-label duration_type">Days</label>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="feature_id">Features :</label>
    <div class="col-md-7">
        <?php
            if(isset($package)){
                $class = $package->features()->pluck('feature_id')->toArray();
            }
        ?>
        <div class="pd-t-10 pd-b-10 custom-scroll-content scrollbar-enabled" style="overflow-y: scroll; max-height: 400px;">
            @foreach(App\PackageFeature::all() as $feature)
                @if(isset($package))
                    @if(in_array($feature->id, $class))
                        <div class="checkbox checkbox-primary bw-3 mail-select-checkbox pd-t-0 pd-b-0">
                            <label class="checkbox-checked">
                                <input type="checkbox" checked="" name="feature_id[]" value="{{ $feature->id }}" />
                                <span class="label-text">{{ $feature->description }}</span>
                            </label>
                        </div>
                    @else
                        <div class="checkbox checkbox-primary bw-3 mail-select-checkbox pd-t-0 pd-b-0">
                            <label class="checkbox-checked">
                                <input type="checkbox" name="feature_id[]" value="{{ $feature->id }}" />
                                <span class="label-text">{{ $feature->description }}</span>
                            </label>
                        </div>
                    @endif
                @else
                    <div class="checkbox checkbox-primary bw-3 mail-select-checkbox pd-t-0 pd-b-0">
                        <label class="checkbox-checked">
                            <input type="checkbox" name="feature_id[]" value="{{ $feature->id }}" />
                            <span class="label-text">{{ $feature->description }}</span>
                        </label>
                    </div>
                @endif
            @endforeach
        </div>
        @if ($errors->has('feature_id'))
            <span class="help-block">
                <strong>{{ $errors->first('feature_id') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="max_guest">Maximum Guests :</label>
    <div class="col-md-7">
        <input required="" class="form-control form {{ $errors->has('max_guest') ? ' is-invalid' : '' }}" id="max_guest" type="number" placeholder="0.00" name="max_guest" value="{{ isset($package) ? $package->max_guest : old('max_guest') }}">
        @if ($errors->has('max_guest'))
            <span class="help-block">
                <strong>{{ $errors->first('max_guest') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right"></label>
    <div class="col-md-7">
        <div class="table-responsive">
            <table class="table table-striped table-hover" id="data-table">
                <thead>
                <tr>
                    <th width="35%">Hotels</th>
                    <th width="35%">Rooms</th>
                    <th width="27%">Rates</th>
                    <th width="3%"></th>
                </tr>
                <tr>
                    <th width="35%">
                        <select id="hotel_id" data-placeholder="Hotel" class="form-control " >
                            <option value=""></option>
                            @foreach(App\Hotel::all() as $hotel)
                                <option value="{{ $hotel->id }}">{{ $hotel->name }}</option>
                            @endforeach
                        </select>
                    </th>
                    <th width="35%">
                        <select id="room_id" data-placeholder="Room" class="form-control " >

                        </select>
                    </th>
                    <th width="27%">
                        <input type="text" class="form-control form" id="room_rate" onkeypress="return isNumberKey(event, this);"/>
                    </th>
                    <th width="3%">
                        <a href="#" class="btn btn-success btn-rounded btn-sm add-detail">
                            <i class="list-icon feather feather-plus"></i>
                        </a>
                    </th>
                </tr>
                </thead>
                <tbody class="hotelsrooms">
                    @if(isset($package))
                        @foreach($package->hoteldetails as $detail)
                            <tr>
                                <td style="padding-left:20px">
                                    {{ $detail->hotel->name }}
                                </td>
                                <td style="padding-left:20px">
                                    {{ $detail->room->type->name }}
                                </td>
                                <td style="padding-left:20px">
                                    {{ number_format($detail->room_rate,2) }}
                                </td>
                                <td>
                                    <a href="#" class="btn btn-danger btn-rounded btn-sm remove-package">
                                    <i class="list-icon feather feather-x"></i>
                                    </a>
                                </td>
                                <input type="hidden" name="hotel_id[]" value="{{ $detail->hotel_id }}"/>
                                <input type="hidden" name="room_id[]" value="{{ $detail->room_id }}"/>
                                <input type="hidden" name="room_rate[]" value="{{ $detail->room_rate }}"/>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right"></label>
    <div class="col-md-7">
        <div class="table-responsive">
            <table class="table table-striped table-hover" id="data-table">
                <thead>
                <tr>
                    <th width="70%">Vehicles</th>
                    <th width="27%">Rates</th>
                    <th width="3%"></th>
                </tr>
                <tr>
                    <th>
                        <select id="vehicle_id" data-toggle="select2" data-placeholder="Vehicles" class="form-control">
                            <option value=""></option>
                            @foreach(App\Vehicle::all() as $vehicle)
                                <option value="{{ $vehicle->id }}" data-rate="{{$vehicle->getrate->rate_day}}">{{ $vehicle->name }}</option>
                            @endforeach
                        </select>
                    </th>
                    <th>
                        <input type="text" class="form-control form" id="vehicle_rate"  onkeypress="return isNumberKey(event, this);"/>
                    </th>
                    <th>
                        <a href="#" class="btn btn-success btn-rounded btn-sm add-detail2">
                            <i class="list-icon feather feather-plus"></i>
                        </a>
                    </th>
                </tr>
                </thead>
                <tbody class="vehicles">
                    @if(isset($package))
                        @foreach($package->vehicledetails as $detail)
                            <tr>
                                <td style="padding-left:20px">
                                    {{ $detail->vehicle->name }}
                                </td>
                                <td style="padding-left:20px">
                                    {{ number_format($detail->vehicle_rate, 2) }}
                                </td>
                                <td>
                                    <a href="#" class="btn btn-danger btn-rounded btn-sm remove-package2">
                                    <i class="list-icon feather feather-x"></i>
                                    </a>
                                </td>
                                <input type="hidden" name="vehicle_id[]" value="{{ $detail->vehicle_id }}"/>
                                <input type="hidden" name="vehicle_rate[]" value="{{ $detail->vehicle_rate }}"/>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="rate">Rate :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('rate') ? ' is-invalid' : '' }}" id="rate" type="number" placeholder="0.00" name="rate" value="{{ isset($package->getrate) ? $package->getrate->rate : old('rate') }}" required="">
        @if ($errors->has('rate'))
            <span class="help-block">
                <strong>{{ $errors->first('rate') }}</strong>
            </span>
        @endif
    </div>
    @if(isset($package->getrate))
        <input type="hidden" name="rate_id" value="{{ $package->getrate->id }}">
    @endif
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="effective_date">Effective Date:</label>
    <div class="col-md-7">
        <input class="datepicker form-control form {{ $errors->has('effective_date') ? ' is-invalid' : '' }}" id="effective_date" type="text" name="effective_date" value="{{ isset($package->getrate) ? $package->getrate->effective_date : old('effective_date') }}" required>
        @if ($errors->has('effective_date'))
            <span class="help-block">
                <strong>{{ $errors->first('effective_date') }}</strong>
            </span>
        @endif
    </div>
</div>
@include('partials.status', ['data' => (isset($package) ? $package : null)])

<hr>
<h6 class="form-section">Upload Images</h6>
@include('partials.photo')

@push('append_js')
    <script type="text/javascript">
        $('.add-detail').click(function(e){
            e.preventDefault();
            var hotel_id = $('#hotel_id').val();
            var hotel_text = $('#hotel_id option:selected').text();
            var room_id = $('#room_id').val();
            var room_text = $('#room_id option:selected').text();
            var room_rate = $('#room_rate').val();
            var c = 0;
            if(!hotel_id){
                alert('Hotel is required.');
                $('#hotel_id').focus();
                return false;
            }
            else if(!room_id){
                alert('Room is required.');
                $('#room_id').focus();
                return false;
            }

            $('#room_rate').val(parseFloat(room_rate).toFixed(2));

            $('table.table tbody.hotelsrooms').find('input[name="hotel_id[]"]').each(function(){
                var h_id = $(this).val();
                var r_id = $(this).closest('tr').find('input[name="room_id[]"]').val();
                if(h_id == hotel_id && room_id == r_id){
                    c++;
                }
            });
            if(c > 0){
                alert('Hotel and Room already added.');
                return false;
            }

            $('table.table tbody.hotelsrooms').append('\
                <tr>\
                    <td style="padding-left:20px">\
                        '+hotel_text+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+room_text+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+room_rate+'\
                    </td>\
                    <td>\
                        <a href="#" class="btn btn-danger btn-rounded btn-sm remove-package">\
                        <i class="list-icon feather feather-x"></i>\
                        </a>\
                    </td>\
                    <input type="hidden" name="hotel_id[]" value="'+hotel_id+'"/>\
                    <input type="hidden" name="room_id[]" value="'+room_id+'"/>\
                    <input type="hidden" name="room_rate[]" value="'+room_rate+'"/>\
                </tr>\
            ');
            getTotalRate();
            
            $('#hotel_id').val('');
            $('#room_id').val('');
            $('#room_rate').val('');
        });

        $('.add-detail2').click(function(e){
            e.preventDefault();
            var vehicle_id = $('#vehicle_id').val();
            var vehicle_text = $('#vehicle_id option:selected').text();
            var vehicle_rate = $('#vehicle_rate').val();
            var c = 0;
            if(!vehicle_id){
                alert('Vehicle is required.');
                $('#vehicle_id').focus();
                return false;
            }

            $('#vehicle_rate').val(parseFloat(vehicle_rate).toFixed(2));

            $('table.table tbody.vehicles').find('input[name="vehicle_id[]"]').each(function(){
                var v_id = $(this).val();
                if(v_id == vehicle_id){
                    c++;
                }
            });
            if(c > 0){
                alert('Vehicles already added.');
                return false;
            }

            $('table.table tbody.vehicles').append('\
                <tr>\
                    <td style="padding-left:20px">\
                        '+vehicle_text+'\
                    </td>\
                    <td style="padding-left:20px">\
                        '+vehicle_rate+'\
                    </td>\
                    <td>\
                        <a href="#" class="btn btn-danger btn-rounded btn-sm remove-package2">\
                        <i class="list-icon feather feather-x"></i>\
                        </a>\
                    </td>\
                    <input type="hidden" name="vehicle_id[]" value="'+vehicle_id+'"/>\
                    <input type="hidden" name="vehicle_rate[]" value="'+vehicle_rate+'"/>\
                </tr>\
            ');
            getTotalRate();
            
            $('#vehicle_id').select2('val',null);
            $('#vehicle_rate').val('');
        });

        $(document).on('click','.remove-package',function(e){
            e.preventDefault();
            $(this).closest('tr').remove();
            getTotalRate();
        });

        $(document).on('click','.remove-package2',function(e){
            e.preventDefault();
            $(this).closest('tr').remove();
            getTotalRate();
        });

        $('#hotel_id').change(function(e){
            var id = $(this).val();
            var url = '{{ route("hotelRooms.api") }}';
            $.getJSON(url,{id:id},function(data){
                $('#room_id').empty();
                $('#room_id').append('<option value="">Select</option>');
                $.each(data,function(index, value){
                    $('#room_id').append('<option value="'+value.id+'" data-rate="'+value.getrate.rate+'">'+value.type.name+'</option>');
                });
            });
        });

        $('#room_id').change(function(e){
            var id = $(this).val();
            var rate = $('option:selected',this).data('rate');

            $('#room_rate').val(parseFloat(rate).toFixed(2));
        });

        $('#vehicle_id').change(function(e){
            var id = $(this).val();
            var rate = $('option:selected',this).data('rate');

            $('#vehicle_rate').val(parseFloat(rate).toFixed(2));
        });

        function getTotalRate(){
            var rate = 0;

            $('table.table tbody.hotelsrooms').find('input[name="room_rate[]"]').each(function(){
                var rate2 = $(this).val();
                rate += parseFloat(rate2);
            });

            $('table.table tbody.vehicles').find('input[name="vehicle_rate[]"]').each(function(){
                var rate2 = $(this).val();
                rate += parseFloat(rate2);
            });

            $('#rate').val(parseFloat(rate).toFixed(2));
        }

        function isNumberKey(evt,el) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            // Added to allow decimal, period, or delete
            if (charCode == 110 || charCode == 190 || charCode == 46) 
                return true;
            
            if (charCode > 31 && (charCode < 48 || charCode > 57)) 
                return false;
            
            return true;
        } // isNumberKey

    </script>
@endpush