@extends('admin.layouts.master')

@section('title', 'Packages - Tour Guide')

@section('header-title')
   <i class="list-icon feather feather-command"></i> Services
@endsection

@section('header-body')
    <span>Packages</span>
    <i class="feather feather-chevron-right"></i>
    <span> Tour Guide</span>
@endsection

@section('content')
    <div class="widget-list">
        <div class="row">
            <div class="widget-holder col-md-12">
                <div class="widget-bg">
                    <div class="widget-heading widget-heading-border">
                        <h5 class="widget-title">List <span class="text-success">( {{  strtoupper($package->name) }} )</span></h5>
                        <div class="widget-actions">
                           @include('partials.add-list',['entity' => 'pguides', 'refid' => $package->id,'reference' => 'Tour Guide'])
                        </div>
                        <!-- /.widget-actions -->
                    </div>
                    <!-- /.widget-heading -->
                    <div class="widget-body">
                        <div class="tabs">
                            @include('admin.packages.partials.nav')
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="data-table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            @can('edit_pguides', 'edit_pguides')
                                            <th class="text-center">Actions</th>
                                            @endcan
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($results as $item)
                                            <tr>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->description }}</td>
                                                @can('edit_pvideos','edit_pguides')
                                                <td class="text-center">
                                                   @include('partials.actions', [
                                                        'entity' => 'pguides',
                                                        'ref_id' => $package->id,
                                                        'id' => $item->id
                                                    ])
                                                </td>
                                                @endcan
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{ $results->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
@endsection