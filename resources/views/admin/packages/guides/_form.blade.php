{{ csrf_field() }}
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="name">Name :</label>
    <div class="col-md-7">
        <input required="" class="form-control form {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" type="text" placeholder="name" name="name" value="{{ isset($guide) ? $guide->name : old('name') }}">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="description">Description :</label>
    <div class="col-md-7">
        <textarea rows="8" class="form-control form {{ $errors->has('description') ? ' is-invalid' : '' }}" required="" id="description" placeholder="description" name="description">{{ isset($guide) ? $guide->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="facebook_url">Facebook Url :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('facebook_url') ? ' is-invalid' : '' }}" id="facebook_url" type="text" placeholder="facebook_url" name="facebook_url" value="{{ isset($guide) ? $guide->facebook_url : old('facebook_url') }}">
        @if ($errors->has('facebook_url'))
            <span class="help-block">
                <strong>{{ $errors->first('facebook_url') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="twitter_url">Twitter Url :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('twitter_url') ? ' is-invalid' : '' }}" id="twitter_url" type="text" placeholder="twitter_url" name="twitter_url" value="{{ isset($guide) ? $guide->twitter_url : old('twitter_url') }}">
        @if ($errors->has('twitter_url'))
            <span class="help-block">
                <strong>{{ $errors->first('twitter_url') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="google_url">Google Url :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('google_url') ? ' is-invalid' : '' }}" id="google_url" type="text" placeholder="google_url" name="google_url" value="{{ isset($guide) ? $guide->google_url : old('google_url') }}">
        @if ($errors->has('google_url'))
            <span class="help-block">
                <strong>{{ $errors->first('google_url') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-right" for="linkedin_url">Linkedin Url :</label>
    <div class="col-md-7">
        <input class="form-control form {{ $errors->has('linkedin_url') ? ' is-invalid' : '' }}" id="linkedin_url" type="text" placeholder="linkedin_url" name="linkedin_url" value="{{ isset($guide) ? $guide->linkedin_url : old('linkedin_url') }}">
        @if ($errors->has('linkedin_url'))
            <span class="help-block">
                <strong>{{ $errors->first('linkedin_url') }}</strong>
            </span>
        @endif
    </div>
</div>
<hr>
<h6 class="form-section">Upload Images</h6>
@include('partials.photo')