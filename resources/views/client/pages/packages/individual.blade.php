@extends('client.layouts.master')

@section('title', ucwords($package->name))

@section('content')

    @include('client.pages.partials.header',['data' => $package])

    <section class="section section--pad2">
        <div class="section__inner single-tab">
            <div class="flex reverse-xs">
                <div class="tab__header fluid-md">
                    <a href="" class="tab__links triggered" data-tab="tab1">Description</a>
                    <a href="" class="tab__links" data-tab="tab2">Tour Itinerary</a>
                    <a href="" class="tab__links" data-tab="tab3">Tour Guide</a>
                    @if(count($package->hoteldetails))
                        <a href="" class="tab__links" data-tab="tab4">Hotels</a>
                    @endif
                    @if(count($package->vehicledetails))
                        <a href="" class="tab__links" data-tab="tab5">Vehicles</a>
                    @endif
                    @if(count($package->getvideos))
                        <a href="" class="tab__links" data-tab="tab6">Videos</a>
                    @endif
                </div>
                <div id="depositModal">
                    <a href="" id="show-modal" class="btn btn-green booknow fixed-md">Deposit</a>
                </div>
            </div>
            <div class="main single">
                <div class="content">
                    <div class="tabs ">

                        <div class="tab__body">
                            <div class="tab__content active" id="tab1">
                                <h3>{{ $package->name }}</h3>

                                <p>{{ $package->description }}</p>

                                <br>
                                @foreach($photos->chunk(3) as $chunk)
                                    <div class="gap gap-3 mb20-xs parent-container">
                                        @foreach($chunk as $photo)
                                            <div class="xs-4 photo-gallery">
                                                <div class="zoom">
                                                    <a href="{{ asset($photo->path.'/thumb_570x400/'.$photo->file_name) }}">
                                                        <img src="{{ asset($photo->path.'/thumb_570x400/'.$photo->file_name) }}" alt="package photo">
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                                <br>

                                <h3>Package Features</h3>

                                <ul class="package-list clx">
                                    @foreach($package->featuredetails as $facility)
                                        <li title="{{ $facility->description }}">
                                            <span>
                                                <i class="fas fa-check"></i>{{ str_limit($facility->description,50) }}
                                            </span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            <div class="tab__content " id="tab2">
                                @foreach($package->getitineraries as $itinerary)
                                    <div class="flex-sm ">
                                        <div class="fixed-sm left hotel-info-thumb">
                                            <img src="{{ asset($itinerary->getphoto->path.'/thumb_570x400/'.$itinerary->getphoto->file_name) }}" alt="itinerary photo">
                                        </div>

                                        <div class="fluid-sm">
                                            <h3 class="title">
                                                <strong>{{ $itinerary->day }},</strong> {{ $itinerary->title }}
                                            </h3>
                                            <p>{{ $itinerary->description }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <div class="tab__content " id="tab3">
                                @foreach($package->getguides as $guide)
                                    <div class="flex-sm ">
                                        <div class="fixed-sm left guide-info-thumb">
                                            <img src="{{ asset($guide->getphoto->path.'/thumb_570x400/'.$guide->getphoto->file_name) }}" alt="guide photo">
                                        </div>

                                        <div class="fluid-sm">
                                            <h3 class="title">{{ $guide->name }}</h3>
                                            <p>{{ $guide->description }}</p>

                                            <div class="social">
                                                @if($guide->facebook_url)
                                                    <a href="{{ $guide->facebook_url }}" target="_blank">
                                                        <i class="fab fa-facebook-f"></i>
                                                    </a>
                                                @endif
                                                @if($guide->twitter_url)
                                                    <a href="{{ $guide->twitter_url }}" target="_blank">
                                                        <i class="fab fa-twitter"></i>
                                                    </a>
                                                @endif
                                                @if($guide->google_url)
                                                    <a href="{{ $guide->google_url }}" target="_blank">
                                                        <i class="fab fa-google-plus-g"></i>
                                                    </a>
                                                @endif
                                                @if($guide->linkedin_url)
                                                    <a href="{{ $guide->linkedin_url }}" target="_blank">
                                                        <i class="fab fa-linkedin"></i>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            @if(count($package->hoteldetails))
                                <div class="tab__content" id="tab4">
                                    @foreach($package->hoteldetails as $hotel)
                                        <div class="flex-sm ">
                                            <div class="fixed-sm left hotel-info-thumb">
                                                <img src="{{ asset($hotel->hotel->getphoto->path.'/thumb_570x400/'.$hotel->hotel->getphoto->file_name) }}" alt="">
                                            </div>

                                            <div class="fluid-sm">
                                                <h3 class="title"><a href="{{ route('client.hotels.individual',['id' => $package->id, 'slug' => str_slug($hotel->name,'-')]) }}">{{ $hotel->hotel->name }}</a></h3>
                                                <p>{{ $hotel->hotel->description }}</p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                            @if(count($package->vehicledetails))
                                <div class="tab__content" id="tab4">
                                    @foreach($package->vehicledetails as $vehicle)
                                        <div class="flex-sm ">
                                            <div class="fixed-sm left hotel-info-thumb">
                                                <img src="{{ asset($vehicle->vehicle->getphoto->path.'/thumb_570x400/'.$vehicle->vehicle->getphoto->file_name) }}" alt="">
                                            </div>

                                            <div class="fluid-sm">
                                                <h3 class="title"><a href="{{ route('client.vehicles.individual',['id' => $package->id, 'slug' => str_slug($vehicle->name,'-')]) }}">{{ $vehicle->vehicle->name }}</a></h3>
                                                <p>{{ $vehicle->vehicle->description }}</p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                            @if(count($package->getvideos))
                                <div class="tab__content " id="tab6">
                                    <div class="video-pop-wrapper">
                                        @foreach($package->getvideos as $video)
                                            <div class="video-pop">
                                                <a href="{{ $video->video_url }}">
                                                    <img src="{{ asset($video->getphoto->path.'/thumb_570x400/'.$video->getphoto->file_name) }}" alt="">
                                                </a>
                                                <div class="video-pop__title">{{ $video->title }}</div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
    
                    </div>
                </div>
                <aside class="sidebar">
                    <h3 class="title">Additional Info</h3>
                    <ul class="additional-info">
                        <li>Status :</li>
                        <li><strong>Available</strong> <span class="status available"></span></li>
                        <li>Pricing:</li>
                        <li>{{ $package->pricing }}</li>
                        <li>Duration :</li>
                        <li>{{ $package->duration }} Days</li>
                        <li>Max Guest :</li>
                        <li>{{ $package->max_guest }}</li>
                    </ul>
                    @include('client.pages.partials.help')
                </aside>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="section__inner">

            <header class="section__header">
                <h3 class="section__title t-center">RECOMMEND Packages</h3>
            </header>

            <div class="gap gap-3 ">
                @foreach($recommended as $recommend)
                    <div class="xs-12 md-6 lg-4">
                        <div class="gallery__item">
                            @if(isset($recommend->getphoto))
                                <a href="{{ asset($recommend->getphoto->path.'/thumb_570x400/'.$recommend->getphoto->file_name) }}" alt="">
                                    <img src="{{ asset($recommend->getphoto->path.'/thumb_570x400/'.$recommend->getphoto->file_name) }}" alt="">
                                </a>
                            @else
                                <a href="{{ asset('admin/images/defaults/background/white-bg.jpg') }}" alt="">
                                    <img src="{{ asset('admin/images/defaults/background/white-bg.jpg') }}" alt="">
                                </a>
                            @endif
                            <div class="gallery__inner">
                                <p class="gallery__meta">{{ $recommend->pricing }}</p>
                                <h3 class="gallery__title"><a href="{{ route('client.packages.individual',['id'=>$recommend->id, 'slug' => $slug]) }}">{{ $recommend->name }}</a></h3>
                                <a class="btn btn-sm btn-blue" href="{{ route('client.packages.individual',['id'=>$recommend->id, 'slug' => $slug]) }}">Book Now</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            
        </div>
    </section>

    <section class="section section--pad">
    </section>


    @push('append_js')
        <script type="text/javascript">
            $('.rooms').owlCarousel({
                loop: false,
                margin: 10,
                nav: false,
                items: 1
            });     

            $('.parent-container').magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery:{enabled:true}
            });

            $('.video-pop').magnificPopup({
                delegate: 'a',
                type: 'iframe'
            });
        </script>
    @endpush
@endsection