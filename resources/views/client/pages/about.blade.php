@extends('client.layouts.master')

@section('title', 'About')

@section('content')
    <div class="banner banner-other" @if(isset($about->getphoto)) style="background-image:url({{ asset($about->getphoto->path.'/'.$about->getphoto->file_name) }});background-size: cover;background-position: 50% 50%;" @endif>
        <div class="banner__inner">

            <h2 class="banner__title">About</h2>
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li class="current">About</li>
            </ol>
        </div>
    </div>


    <section class="section section--pad">
        <div class="section__inner">
            
            <header class="section__header">
                <h3 class="section__title t-center">WHO WE ARE?</h3>
                <p class="section__title-sub t-center">
                    @if(isset($about))
                        {{ $about->company }}
                    @endif
                </p>
            </header>
            
            @if(isset($about))
                {!! $about->description !!}
            @endif

        </div>
    </section>

    <section class="section section--pad">
        <div class="section__inner">
            <header class="section__header">
                <h3 class="section__title t-center">SIMPLE AND EFFECTIVE</h3>
                <p class="section__title-sub t-center"></p>
            </header>

            <div class="gap gap-3 copy">
                <div class="xs-12 md-3">
                    <div class="person">
                        <div class="person__thumb"><img src="{{ asset('client/assets/img/volunteer1.jpg') }}" alt=""></div>
                        <h3 class="person__name">Victor Drover</h3>
                        <h4 class="person__title">Managing Director</h4>

                        <p class="person__desc">Vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor.</p>

                        <div class="social">
                            <a href="" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="" target="_blank">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                            <a href="" target="_blank">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="xs-12 md-3">
                    <div class="person">
                        <div class="person__thumb"><img src="{{ asset('client/assets/img/volunteer2.jpg') }}" alt=""></div>
                        <h3 class="person__name">Victor Drover</h3>
                        <h4 class="person__title">Managing Director</h4>

                        <p class="person__desc">Vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor.</p>
                        <div class="social">
                            <a href="" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="" target="_blank">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                            <a href="" target="_blank">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="xs-12 md-3">
                    <div class="person">
                        <div class="person__thumb"><img src="{{ asset('client/assets/img/volunteer3.jpg') }}" alt=""></div>
                        <h3 class="person__name">Victor Drover</h3>
                        <h4 class="person__title">Managing Director</h4>

                        <p class="person__desc">Vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor.</p>
                        <div class="social">
                            <a href="" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="" target="_blank">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                            <a href="" target="_blank">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="xs-12 md-3">
                    <div class="person">
                        <div class="person__thumb"><img src="{{ asset('client/assets/img/volunteer6.jpg') }}" alt=""></div>
                        <h3 class="person__name">Victor Drover</h3>
                        <h4 class="person__title">Managing Director</h4>

                        <p class="person__desc">Vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor.</p>
                        <div class="social">
                            <a href="" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="" target="_blank">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                            <a href="" target="_blank">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

