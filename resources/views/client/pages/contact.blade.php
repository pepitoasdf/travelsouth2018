@extends('client.layouts.master')

@section('title', 'Contact')

@section('content')
    <div class="banner banner-contact">
        <div class="banner__inner">

            <h2 class="banner__title">CONTACT US</h2>
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li class="current">Contact Us</li>
            </ol>
        </div>
    </div>

    <section class="section">
        <div class="section__inner--full">
            <div class="map-wrapper">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d125585.22372703924!2d123.77625473040713!3d10.378756863874981!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33a999258dcd2dfd%3A0x4c34030cdbd33507!2sCebu+City%2C+Cebu!5e0!3m2!1sen!2sph!4v1523682371906" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </section>

    <section class="section section--pad">
        <div class="section__inner">
            
            <div class="gap gap-3">
                <div class="xs-12 md-5">
                    <h3>OUR OFFICE</h3>

                    <div class="copy">
                        @if(isset($contact))
                            <p>
                                <strong>Address</strong> <br>
                                {{ $contact->address }}
                            </p>

                            <p>
                                <strong>Email</strong> <br>
                                {{ $contact->email }}
                            </p>

                            <p>
                                <strong>Phone</strong> <br>
                                {{ $contact->phone }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="xs-12 md-6">
                    <h3>CONTACT FORM</h3>

                    <form action="" class="contact-form">
                        <div class="gap gap-3">
                            <div class="xs-12 md-6">
                                <div class="form__group">
                                    <input type="text" class="form__control" placeholder="Name">
                                </div>
                            </div>
                            <div class="xs-12 md-6">
                                <div class="form__group">
                                    <input type="text" class="form__control" placeholder="Email">
                                </div>
                            </div>
                        </div>
                        <div class="gap gap-3">
                            <div class="xs-12 md-6">
                                <div class="form__group">
                                    <input type="text" class="form__control" placeholder="Subject">
                                </div>
                            </div>
                            <div class="xs-12 md-6">
                                <div class="form__group">
                                    <input type="text" class="form__control" placeholder="Phone">
                                </div>
                            </div>
                        </div>

                        <div class="form__group">
                            <textarea name="" id="" cols="30" rows="5" class="form__control" placeholder="Message"></textarea>
                        </div>

                        <button class="btn btn-blue btn-submit">Send Message</button>
                    </form>
                </div>
            </div>

        </div>
    </section>
@endsection
