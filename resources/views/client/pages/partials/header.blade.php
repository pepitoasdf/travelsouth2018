<section class="section section--banner section--banner-single" style="background-image:url({{ asset($data->getphoto->path.'/'.$data->getphoto->file_name) }});background-repeat:no-repeat;background-size: cover;background-position: 50% 50%;">
    <div class="section__inner">
        <h2 class="banner__title">{{ strtoupper($data->name) }}</h2>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li class="current">{{ $data->name }}</li>
        </ol>
    </div>
</section>