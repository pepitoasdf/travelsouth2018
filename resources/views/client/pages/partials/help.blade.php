<div class="need-help">
    <h3 class="title">Need Help!</h3>
    <p>Rem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>

    <p class="info"><i class="fas fa-phone-square"></i>+123 456 78910</p>
    <p class="info"><i class="fas fa-envelope-square"></i>booknow@travelkit.com </p>
</div>