@extends('client.layouts.master')

@section('title', ucwords($hotel->name))

@section('content')

    @include('client.pages.partials.header',['data' => $hotel])

    <section class="section section--pad2">
        <div class="section__inner single-tab">
            <div class="flex reverse-xs">
                <div class="tab__header fluid-md">
                    <a href="" class="tab__links triggered" data-tab="tab1">Description</a>
                    <a href="" class="tab__links" data-tab="tab2">Room Type</a>
                    @if(count($hotel->getvideos))
                        <a href="" class="tab__links" data-tab="tab3">Videos</a>
                    @endif
                </div>
                <div id="depositModal">
                    <a href="" id="show-modal" class="btn btn-green booknow fixed-md">Deposit</a>
                </div>
            </div>
            <div class="main single">
                <div class="content">
                    <div class="tabs ">

                        <div class="tab__body">
                            <div class="tab__content active" id="tab1">
                                <h3>{{ $hotel->name }}</h3>

                                <p>{{ $hotel->description }}</p>

                                <br>
                                @foreach($photos->chunk(3) as $chunk)
                                    <div class="gap gap-3 mb20-xs parent-container">
                                        @foreach($chunk as $photo)
                                            <div class="xs-4 photo-gallery">
                                                <div class="zoom">
                                                    <a href="{{ asset($photo->path.'/thumb_570x400/'.$photo->file_name) }}">
                                                        <img src="{{ asset($photo->path.'/thumb_570x400/'.$photo->file_name) }}" alt="hotel photo">
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                                <br>

                                <h3>Hotel Facility</h3>

                                <ul class="package-list clx">
                                    @foreach($hotel->facilitydetails as $facility)
                                        <li title="{{ $facility->description }}">
                                            <span>
                                                <i class="fas fa-check"></i>{{ str_limit($facility->description,50) }}
                                            </span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            <div class="tab__content " id="tab2">
                                @foreach($rooms as $room)
                                    <div class="gap gap-2 room-type" style="cursor: pointer;" data-rooms="{{ $room->room_no}}" data-pricing="{{ $room->pricing }}">
                                        <div class="xs-12 sm-12 md-6">
                                            <div class="rooms owl-carousel">
                                                @foreach($room->getphotos as $photo)
                                                    <div class="item">
                                                        <img src="{{ asset($photo->path.'/thumb_570x400/'.$photo->file_name) }}" alt="room photo">
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="xs-12 sm-12 md-6">
                                            <p>
                                                <span class="price" style="font-size: 18px;">
                                                    {{ $room->pricing }}
                                                </span>
                                            </p>
                                            <p>{{ $room->description }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @if(count($hotel->getvideos))
                                <div class="tab__content " id="tab3">
                                    <div class="video-pop-wrapper">
                                        @foreach($hotel->getvideos as $video)
                                            <div class="video-pop">
                                                <a href="{{ $video->video_url }}">
                                                    <img src="{{ asset($video->getphoto->path.'/'.$video->getphoto->file_name) }}" alt="">
                                                </a>
                                                <div class="video-pop__title">{{ $video->title }}</div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
    
                    </div>
                </div>
                <aside class="sidebar">
                    <h3 class="title">Additional Info</h3>
                    <ul class="additional-info">
                        <li>Status :</li>
                        <li><strong>Available</strong> <span class="status available"></span></li>
                        <li class="no-of-rooms" style="display: none;">Total Rooms :</li>
                        <li class="no-of-rooms content" style="display: none;"></li>
                        <li class="pricing" style="display: none;">Pricing:</li>
                        <li class="pricing content" style="display: none;"></li>
                    </ul>
                    @include('client.pages.partials.help')
                </aside>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="section__inner">

            <header class="section__header">
                <h3 class="section__title t-center">RECOMMEND HOTEL</h3>
            </header>

            <div class="gap gap-3 parent-container">
                @foreach($recommended as $recommend)
                    <div class="xs-12 md-6 lg-4">
                        <div class="gallery__item">
                            @if(isset($recommend->getphoto))
                                <a href="{{ asset($recommend->getphoto->path.'/thumb_570x400/'.$recommend->getphoto->file_name) }}" alt="">
                                    <img src="{{ asset($recommend->getphoto->path.'/thumb_570x400/'.$recommend->getphoto->file_name) }}" alt="">
                                </a>
                            @else
                                <a href="{{ asset('admin/images/defaults/background/white-bg.jpg') }}" alt="">
                                    <img src="{{ asset('admin/images/defaults/background/white-bg.jpg') }}" alt="">
                                </a>
                            @endif

                            <div class="gallery__inner">
                                <p class="gallery__meta">{{ $recommend->pricing }}</p>
                                <h3 class="gallery__title">
                                    <a href="{{ route('client.hotels.individual',['id'=>$recommend->id, 'slug' => $slug]) }}">{{ $recommend->name }}</a>
                                </h3>
                                <a class="btn btn-sm btn-blue" href="{{ route('client.hotels.individual',['id'=>$recommend->id, 'slug' => $slug]) }}">Book Now</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="section section--pad">
    </section>


    @push('append_js')
        <script type="text/javascript">
            $('.rooms').owlCarousel({
                loop: false,
                margin: 10,
                nav: false,
                items: 1
            });     

            $('.parent-container').magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery:{enabled:true}
            });

            $('.video-pop').magnificPopup({
                delegate: 'a',
                type: 'iframe'
            });

            $('.room-type').click(function(e){
                var rooms = $(this).data('rooms');
                var pricing = $(this).data('pricing');
                $('ul.additional-info li.no-of-rooms').show('fast');
                $('ul.additional-info li.no-of-rooms.content').html(rooms);

                $('ul.additional-info li.pricing').show('fast');
                $('ul.additional-info li.pricing.content').html(pricing);
            });
        </script>
    @endpush
@endsection