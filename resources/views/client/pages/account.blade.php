@extends('client.layouts.master')

@section('title', 'Travel South - Account')

@section('content')
    <section class="section section--banner">
        <div class="section__inner">
            <h2 class="banner__title">My Account</h2>
            <ol class="breadcrumb">
                <li><a href="">Home</a></li>
                <li class="current">My Account</li>
            </ol>
        </div>
    </section>

    <section class="section section--pad2">
        <div class="section__inner">
            
            <div class="gap">
                <div class="user-profile" id="edit-account">
                    
                    
                    <button @click="editTable = true" >Edit</button>

                    <div v-if="editTable">
                        <a href="">Cancel</a>
                    </div>
                    
                    <account-form :edit-it="editTable"></account-form>  
                        
                </div>
                <div class="fluid">
                    <div class="">
                        <div id="depositModal">
                            <a href="" id="show-modal" @click.prevent="showModal = true" class="btn btn-green booknow fixed-md">Deposit</a>
                            <modal v-if="showModal" @close="showModal = false"></modal>
                        </div>
                    </div>

                    <div class="transaction-history">
                        <table class="table transaction-table">
                            <thead>
                                <tr>
                                    <th>Transaction #</th>
                                    <th>Transaction Name</th>
                                    <th>Date Booked</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>0231</td>
                                    <td>Boracay Summer Escapade</td>
                                    <td>May 10, 1991</td>
                                    <td>Php 15,999.00</td>
                                </tr>
                                <tr>
                                    <td>0231</td>
                                    <td>Boracay Summer Escapade</td>
                                    <td>May 10, 1991</td>
                                    <td>Php 15,999.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
            
        </div>
    </section>
@endsection
