@extends('client.layouts.master')

@section('title', ucwords($vehicle->name))

@section('content')

    @include('client.pages.partials.header',['data' => $vehicle])

    <section class="section section--pad2">
        <div class="section__inner single-tab">
            <div class="flex reverse-xs">
                <div class="tab__header fluid-md">
                    <a href="" class="tab__links triggered" data-tab="tab1">Description</a>
                    <a href="" class="tab__links" data-tab="tab2">Gallery</a>
                    @if(count($vehicle->getvideos))
                        <a href="" class="tab__links" data-tab="tab3">Videos</a>
                    @endif
                    <a href="" class="tab__links" data-tab="tab4">Schedule</a>
                </div>
                <div id="depositModal">
                    <a href="" id="show-modal" class="btn btn-green booknow fixed-md">Deposit</a>
                </div>
            </div>
            <div class="main single">
                <div class="content">
                    <div class="tabs ">

                        <div class="tab__body">
                            <div class="tab__content active" id="tab1">
                                <h3>{{ $vehicle->name }}</h3>

                                <p>{{ $vehicle->description }}</p>

                                <br>
                                @foreach($photos->chunk(3) as $chunk)
                                    <div class="gap gap-3 mb20-xs parent-container">
                                        @foreach($chunk as $photo)
                                            <div class="xs-4 photo-gallery">
                                                <div class="zoom">
                                                    <a href="{{ asset($photo->path.'/thumb_570x400/'.$photo->file_name) }}">
                                                        <img src="{{ asset($photo->path.'/thumb_570x400/'.$photo->file_name) }}" alt="vehicle photo">
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                                <br>

                                <h3>Why you choose this Vehicle ?</h3>

                                <ul class="package-list clx">
                                    @foreach($vehicle->featuredetails as $feature)
                                        <li title="{{ $feature->description }}">
                                            <span>
                                                <i class="fas fa-check"></i>{{ str_limit($feature->description,50) }}
                                            </span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            <div class="tab__content " id="tab2">
                               <div class="vehicle owl-carousel">
                                    @foreach($photos as $photo)
                                        <div class="item">
                                            <img src="{{ asset($photo->path.'/thumb_570x400/'.$photo->file_name) }}" alt="vehicle photo">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            @if(count($vehicle->getvideos))
                                <div class="tab__content " id="tab3">
                                    <div class="video-pop-wrapper">
                                        @foreach($vehicle->getvideos as $video)
                                            <div class="video-pop">
                                                <a href="{{ $video->video_url }}">
                                                    <img src="{{ asset($video->getphoto->path.'/thumb_570x400/'.$video->getphoto->file_name) }}" alt="">
                                                </a>
                                                <div class="video-pop__title">{{ $video->title }}</div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                            <div class="tab__content " id="tab4">
                                <table class="table table-responsive time-schedule">
                                    <thead>
                                        <tr class="t-left">
                                            <th>Pickup Time</th>
                                            <th>Drop Time </th>
                                        </tr>
                                    </thead>
                                    <tbody>                                             
                                        <tr>
                                            <td>2016-12-03 16:00</td>

                                            <td>2016-12-03 18:00</td>
                                        </tr>
                                        <tr>
                                            <td>2016-12-03 12:00</td>
                                            <td>2016-12-03 14:30</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
    
                    </div>
                </div>
                <aside class="sidebar">
                    <h3 class="title">Additional Information</h3>

                    <ul class="additional-info">
                        <li>Status :</li>
                        <li><strong>Available</strong> <span class="status available"></span></li>

                        <li>Vehicle Name :</li>
                        <li>{{ $vehicle->name }}</li>

                        <li>Vehicle Type:</li>
                        <li>Car</li>

                        @if($vehicle->color)
                            <li>Color:</li>
                            <li>{{ $vehicle->color }}</li>
                        @endif
                        
                        @if($vehicle->year_model)
                            <li>Year Model:</li>
                            <li>{{ $vehicle->year_model }}</li>
                        @endif
                        
                        @if($vehicle->doors)
                            <li>Doors:</li>
                            <li>{{ $vehicle->doors }}</li>
                        @endif

                        @if($vehicle->seats)
                            <li>Passenger:</li>
                            <li>{{ $vehicle->seats }}</li>
                        @endif

                        @if($vehicle->transmission)
                            <li>Transmission:</li>
                            <li>{{ $vehicle->transmission }}</li>
                        @endif

                        @if($vehicle->aircon)
                            <li>Aircon:</li>
                            <li>{{ $vehicle->aircon }}</li>
                        @endif

                        @if(isset($vehicle->getrate))
                            <li>Price:</li>
                            <li>{{ $vehicle->getrate->rate_day}} / Per Day</li>
                        @endif
                    </ul>
                    @include('client.pages.partials.help')
                </aside>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="section__inner">

            <header class="section__header">
                <h3 class="section__title t-center">RECOMMEND VEHICLES</h3>
            </header>

            <div class="gap gap-3 ">
                @foreach($recommended as $recommend)
                    <div class="xs-12 md-6 lg-4">
                        <div class="gallery__item">
                            @if(isset($recommend->getphoto))
                                <a href="{{ asset($recommend->getphoto->path.'/thumb_570x400/'.$recommend->getphoto->file_name) }}" alt="">
                                    <img src="{{ asset($recommend->getphoto->path.'/thumb_570x400/'.$recommend->getphoto->file_name) }}" alt="">
                                </a>
                            @else
                                <a href="{{ asset('admin/images/defaults/background/white-bg.jpg') }}" alt="">
                                    <img src="{{ asset('admin/images/defaults/background/white-bg.jpg') }}" alt="">
                                </a>
                            @endif

                            <div class="gallery__inner">
                                <p class="gallery__meta">{{ $recommend->pricing }}</p>
                                <h3 class="gallery__title"><a href="{{ route('client.vehicles.individual',['id'=>$recommend->id, 'slug' => $slug]) }}">{{ $recommend->name }}</a></h3>
                                <a class="btn btn-sm btn-blue" href="{{ route('client.vehicles.individual',['id'=>$recommend->id, 'slug' => $slug]) }}">Book Now</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            
        </div>
    </section>

    <section class="section section--pad">
    </section>


    @push('append_js')
        <script type="text/javascript">
            $('.vehicle').owlCarousel({
                loop: false,
                margin: 10,
                nav: false,
                items: 1
            });     

            $('.parent-container').magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery:{enabled:true}
            });

            $('.video-pop').magnificPopup({
                delegate: 'a',
                type: 'iframe'
            });
        </script>
    @endpush
@endsection