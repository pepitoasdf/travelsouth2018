@extends('client.layouts.master')

@section('title', $option['title'])

@section('content')
    <section class="section section--banner" @if(isset($datas[0]->getphoto)) style="background-image:url({{ asset($datas[0]->getphoto->path.'/'.$datas[0]->getphoto->file_name) }});background-size: cover;background-position: 50% 50%;" @endif>
        <div class="section__inner">
            <h2 class="banner__title">{{ $option['header'] }}</h2>
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li class="current">{{  $option['header'] }}</li>
            </ol>
        </div>
    </section>

    <section class="section section--pad2">
        <div class="section__inner">

            <div class="gap gap-3">
                @foreach($datas as $data)
                    <?php
                        $slug = str_slug($data->name,'-');
                    ?>
                    @if(isset($data->getphoto))
                        <div class="xs-12 md-6 lg-4">
                            <div class="gallery__item">
                                <img src="{{ asset($data->getphoto->path.'/thumb_570x400/'.$data->getphoto->file_name) }}" alt="">

                                <div class="gallery__inner">
                                    <p class="gallery__meta">{{ str_limit($data->pricing,35) }}</p>
                                    @if($option['header'] == "Hotels")
                                        <h3 class="gallery__title">
                                            <a href="{{ route('client.hotels.individual',['id' => $data->id, 'slug' => $slug]) }}">{{ $data->name }}</a>
                                        </h3>
                                        <a class="btn btn-sm btn-blue" href="{{ route('client.hotels.individual',['id' => $data->id, 'slug' => $slug]) }}">Book Now</a>
                                    @elseif($option['header'] == "Vehicles")
                                        <h3 class="gallery__title">
                                            <a href="{{ route('client.vehicles.individual',['id' => $data->id, 'slug' => $slug]) }}">{{ $data->name }}</a>
                                        </h3>
                                        <a class="btn btn-sm btn-blue" href="{{ route('client.vehicles.individual',['id' => $data->id, 'slug' => $slug]) }}">Book Now</a>
                                    @elseif($option['header'] == "Packages")
                                        <h3 class="gallery__title">
                                            <a href="{{ route('client.packages.individual',['id' => $data->id, 'slug' => $slug]) }}">{{ $data->name }}</a>
                                        </h3>
                                        <a class="btn btn-sm btn-blue" href="{{ route('client.packages.individual',['id' => $data->id, 'slug' => $slug]) }}">Book Now</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>

            <div class="t-center">
                {{ $datas->links('vendor.pagination.client') }}
            </div>
        </div>
    </section>
    
    <section class="section section--pad2 section--subs">
        <div class="section__inner">
            <header class="section__header">
                <h3 class="section__title t-center">SUBSCRIBE TO OUR NEWSLETTER</h3>
            </header>

            <div class="subscription">
                <form action="">
                    <input type="text" class="form__control" placeholder="Your email address">
                    <button class="btn btn-blue">Submit</button>
                </form>
            </div>
        </div>
    </section>
@endsection

