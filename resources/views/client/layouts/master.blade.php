<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        @yield('title')
    </title>
    <link rel="shortcut icon" type="image/x-icon" sizes="16x16" href="{{ asset('favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('client/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('client/assets/css/owl.carousel.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Pontano+Sans|Poppins:400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/fontawesome.css" integrity="sha384-q3jl8XQu1OpdLgGFvNRnPdj5VIlCvgsDQTQB6owSOHWlAurxul7f+JpUOVdAiJ5P" crossorigin="anonymous">
    
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css" />

    @stack('append_css')

</head>
<body>

    <div class="wrapper">
        
        @include('client.layouts.partials.header')

        @yield('content')

        @include('client.layouts.partials.footer')
    </div>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>

    <script src="https://unpkg.com/jarallax@1.10/dist/jarallax.min.js"></script>
    <script src="https://unpkg.com/jarallax@1.10/dist/jarallax-video.min.js"></script>
    
    <script src="{{ asset('client/assets/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('client/assets/js/magnific-popup.min.js') }}"></script>
    <script src="{{ asset('client/assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('client/assets/js/main.js') }}"></script>
    @stack('append_js')
</body>
</html>