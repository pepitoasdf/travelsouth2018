<header class="header">
    <div class="header__top">
        <div class="header__inner">
            <div class="info">
                <div class="tel"><span><strong>Call Us: </strong></span>+63 32 474 8301</div>
                <div class="email"><span><strong>Email: </strong></span>info@travelsouth.com.ph</div>
            </div>
        </div>
    </div>
    <div class="header__bot">
        <div class="header__inner header__inner-menu">
            <h1>
                <a href="index.html" class="branding">
                    <img src="{{ asset('client/assets/img/travel-south.png') }}" alt="travel south" class="logo">
                </a>
            </h1>

            <ul class="menu menu-main" id="main-menu">
                <li class="menu__root">
                    <a href="{{ url('/') }}" class="menu__item">Home</a>
                </li>
                <li class="menu__root has-menu-child">
                    <span class="menu__item">Services</span>
                    
                    <ul class="menu-sub">
                        <li class="menu__branch">
                            <a href="{{ route('client.packages') }}" class="menu__branch-item">Package List</a>
                        </li>
                        <li class="menu__branch">
                            <a href="{{ route('client.hotels') }}" class="menu__branch-item">Hotel</a>
                        </li>
                        <li class="menu__branch">
                            <a href="{{ route('client.vehicles') }}" class="menu__branch-item">Vehicle</a>
                        </li>
                    </ul>
                    
                </li>
                <li class="menu__root">
                    <a href="{{ route('client.about') }}" class="menu__item">About</a>
                </li>
                <li class="menu__root">
                    <a href="{{ route('client.contact') }}" class="menu__item">Contact</a>
                </li>
                @if (Auth::check())
                    <li class="menu__root">
                        <a href="{{ route('client.account') }}" class="menu__item">Account</a>
                    </li>
                    <li class="menu__root">
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"
                            class="menu__item">Logout</a>
                    </li>
                @else
                    <li class="menu__root">
                        <a href="{{ route('login') }}" class="menu__item">Login</a>
                    </li>
                    <li class="menu__root">
                        <a href="{{ route('register') }}" class="menu__item">Register</a>
                    </li>
                @endif
            </ul>

            <button class="nav-trigger" data-nav="main-menu">
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
            </button>
        </div>
    </div>
</header>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>