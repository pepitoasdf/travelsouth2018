@extends('client.layouts.master')

@section('title', 'Welcome To Travel South')

@section('content')
    <div class="hero">
        <div class="hero__inner">
            <div class="main-slider owl-carousel">
                @if(count($backgrounds))
                    @foreach($backgrounds as $background)
                        <div class="item">
                            <div class="item__text">
                                <p class="hero__title-sub">{{ $background->subtitle }}</p>
                                <h2 class="hero__title">{{ $background->title }}</h2>
                                <p class="hero__text">
                                    {{ $background->description }}
                                </p>

                                <a href="" class="btn btn-o hero__btn">START TOUR</a>
                            </div>
                            @if(isset($background->getphoto))
                                <img src="{{ asset($background->getphoto->path.'/1920X1080/'.$background->getphoto->file_name) }}" alt="" style="width: 100%">
                            @else
                                <img src="{{ asset('client/assets/img/slider1bg.jpg') }}" alt="" style="width: 100%">
                            @endif
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        <div class="tabs main-tab">
             
            <div class="tab__header">
                <a href="" class="tab__links triggered" data-tab="holiday"><i class="icon-location"></i> <span>Holidays</span></a>
                <a href="" class="tab__links" data-tab="hotel-resort"><i class="icon-hotels"></i><span>Hotels &amp; Resorts</span></a>
                <!-- <a href="" class="tab__links" data-tab="flight">Flights</a> -->
                <a href="" class="tab__links" data-tab="vehicle"><i class="icon-vehicles"></i><span>Vehicles</span></a>
            </div>

            <div class="tab__body">
                <div class="tab__content active" id="holiday">
                    <form action="" class="form form-search">
                        <div class="gap gap-0 search-container">
                            <div class="gap gap-0 flex">
                                <div class="xs-6 sm-3">
                                    <div class="form__group">
                                        <label for="">Search</label>
                                        <input type="text" class="form__control" placeholder="Type keyword">
                                    </div>
                                </div>
                                <div class="xs-6 sm-3">
                                    <div class="form__group">
                                        <label for="">Location</label>
                                        <select name="" id="" class="form__control">
                                            <option value="">Select a Location</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="xs-6 sm-3">
                                    <div class="form__group">
                                        <label for="">Check-in-date</label>
                                        <input type="text" class="form__control" id="check-in"  placeholder="Check-in date">
                                    </div>
                                </div>
                                <div class="xs-6 sm-3">
                                    <div class="form__group">
                                        <label for="">Guest</label>
                                        <input type="number" class="form__control" placeholder="Number of guests">
                                    </div>
                                </div>
                            </div>

                            <button class="btn btn-blue btn-search">Search</button>
                        </div>
                    </form>
                </div>
                <div class="tab__content" id="hotel-resort">
                    <form action="" class="form form-search">
                        <div class="gap gap-0 search-container">
                            <div class="gap gap-0 flex">
                                <div class="xs-6 sm-3">
                                    <div class="form__group">
                                        <label for="">Search</label>
                                        <input type="text" class="form__control" placeholder="Type keyword">
                                    </div>
                                </div>
                                <div class="xs-6 sm-3">
                                    <div class="form__group">
                                        <label for="">Location</label>
                                        <select name="" id="" class="form__control">
                                            <option value="">Select a Location</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="xs-6 sm-3">
                                    <div class="form__group">
                                        <label for="">Hotel Type</label>
                                        <select name="" id="" class="form__control">
                                            <option value="">Hotel Type</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="xs-6 sm-3">
                                    <div class="form__group">
                                        <label for="">Room Type</label>
                                        <select name="" id="" class="form__control">
                                            <option value="">Room Type</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <button class="btn btn-blue btn-search">Search</button>
                        </div>
                    </form>
                </div>
                <div class="tab__content" id="flight">
                    <form action="">
                        <div class="form__group">
                            <input type="text" class="form__control" placeholder="1">
                        </div>
                    </form>
                </div>
                <div class="tab__content" id="vehicle">
                    <form action="" class="form form-search">
                        <div class="gap gap-0 search-container">
                            <div class="gap gap-0 flex">
                                <div class="xs-6 sm-3">
                                    <div class="form__group">
                                        <label for="">Pickup Time</label>
                                        <input type="text" class="form__control" id="pickup" placeholder="Pickup Date & Time">
                                    </div>
                                </div>
                                <div class="xs-6 sm-3">
                                    <div class="form__group">
                                        <label for="">Hours</label>
                                        <input type="number" class="form__control" placeholder="How many hours do you need?">
                                    </div>
                                </div>
                                <div class="xs-6 sm-3">
                                    <div class="form__group">
                                        <label for="">Pickup Location</label>
                                        <input type="text" class="form__control " placeholder="Pickup Location">
                                    </div>
                                </div>
                                <div class="xs-6 sm-3">
                                    <div class="form__group">
                                        <label for="">Drop Location</label>
                                        <input type="text" class="form__control " placeholder="Drop Location">
                                    </div>
                                </div>
                            </div>

                            <button class="btn btn-blue btn-search">Search</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @if(isset($popular))
        <section class="section section--pad2">
            <div class="section__inner">
                <header class="section__header">
                    <h3 class="section__title t-center">
                        {{ strtoupper($popular->title) }}
                    </h3>
                    <p class="section__title-sub t-center">
                        {{ $popular->description }}
                    </p>
                </header>

                <div class="gap gap-3">
                    @foreach($popular->details as $detail)
                        @if($popular->type == "Packages")
                            <div class="xs-12 md-6 lg-4">
                                <div class="gallery__item">
                                    @if(isset($detail->package->getphoto))
                                        <img src="{{ asset($detail->package->getphoto->path.'/thumb_570x400/'.$detail->package->getphoto->file_name) }}" alt="">
                                    @else
                                        <img src="{{ asset('client/assets/img/thumb01.jpg') }}" alt="">
                                    @endif
                                    <div class="gallery__inner">
                                        <p class="gallery__meta">{{ str_limit($detail->package->description,35) }}</p>
                                        <h3 class="gallery__title">
                                            <a href="{{ route('client.packages') }}">{{ $detail->package->name }}</a>
                                        </h3>
                                        <a class="btn btn-sm btn-blue" href="{{ route('client.packages') }}">Book Now</a>
                                    </div>
                                </div>
                            </div>
                        @elseif($popular->type == "Vehicles")
                            <div class="xs-12 md-6 lg-4">
                                <div class="gallery__item">
                                    @if(isset($detail->vehicle->getphoto))
                                        <img src="{{ asset($detail->vehicle->getphoto->path.'/thumb_570x400/'.$detail->vehicle->getphoto->file_name) }}" alt="">
                                    @else
                                        <img src="{{ asset('client/assets/img/thumb01.jpg') }}" alt="">
                                    @endif
                                    <div class="gallery__inner">
                                        <p class="gallery__meta">{{ str_limit($detail->vehicle->description,35) }}</p>
                                        <h3 class="gallery__title">
                                            <a href="{{ route('client.vehicles') }}">{{ $detail->vehicle->name }}</a>
                                        </h3>
                                        <a class="btn btn-sm btn-blue" href="{{ route('client.vehicles') }}">Book Now</a>
                                    </div>
                                </div>
                            </div>
                        @elseif($popular->type == "Hotels")
                            <div class="xs-12 md-6 lg-4">
                                <div class="gallery__item">
                                    @if(isset($detail->hotel->getphoto))
                                        <img src="{{ asset($detail->hotel->getphoto->path.'/thumb_570x400/'.$detail->hotel->getphoto->file_name) }}" alt="">
                                    @else
                                        <img src="{{ asset('client/assets/img/thumb01.jpg') }}" alt="">
                                    @endif
                                    <div class="gallery__inner">
                                        <p class="gallery__meta">{{ str_limit($detail->hotel->description, 35) }}</p>
                                        <h3 class="gallery__title">
                                            <a href="{{ route('client.hotels') }}">{{ $detail->hotel->name }}</a>
                                        </h3>
                                        <a class="btn btn-sm btn-blue" href="{{ route('client.vehicles') }}">Book Now</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </section>
    @endif
    @if(isset($video))
        <section class="section">
            <div class="jarallax video" data-jarallax-video="{{ $video->url }}">
                <h2>{{ $video->title }}</h2>
                <p>
                    {{ $video->description }}
                <p>
                    <a href="" class="btn btn-green btn-lg">BUY NOW</a></p>
            </div>
        </section>
    @endif
    @if(isset($topdestination))
        <section class="section section--pad2">
            <div class="section__inner">
                <header class="section__header">
                    <h3 class="section__title t-center">
                        {{ strtoupper($topdestination->title) }}
                    </h3>
                    <p class="section__title-sub t-center">
                        {{ $topdestination->description }}
                    </p>
                </header>
                <div class="gap gap-3 top-destination">
                    @foreach($topdestination->details as $detail)
                        <div class="xs-6 sm-4 md-2">
                            <a href="">
                                <img src="{{ asset($detail->getphoto->path.'/thumb_570x400/'.$detail->getphoto->file_name) }}" alt="">
                            </a>
                            <h4 class="t6">
                                <a href="">
                                    {{ $detail->city->name }}, 
                                    {{ $detail->province->name }}, 
                                    {{ $detail->country->name }}
                                </a>
                            </h4>
                            <span>{{ $detail->name }}</span>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
    @if(isset($increadibleplace))
        <section class="section">
            <div class="section__inner">
                <header class="section__header">
                    <h3 class="section__title t-center">
                        {{ strtoupper($increadibleplace->title) }}
                    </h3>
                    <p class="section__title-sub t-center">
                        {{ $increadibleplace->description }}
                    </p>
                </header>
                <div class="gap gap-3 parent-container">
                    <div class="xs-12 lg-6 photo-gallery mb20-xs mb0-lg">
                        <div class="zoom">
                            @if(isset($increadibleplace->details[0]))
                                <a href="{{ asset($increadibleplace->details[0]->getphoto->path.'/thumb_570x400/'.$increadibleplace->details[0]->getphoto->file_name) }}">
                                    <img src="{{ asset($increadibleplace->details[0]->getphoto->path.'/thumb_570x400/'.$increadibleplace->details[0]->getphoto->file_name) }}" alt="">
                                </a>
                            @endif
                        </div>
                    </div>
                    <div class="xs-12 lg-6">
                        <div class="gap gap-3 mb20-xs">
                            <div class="xs-6  photo-gallery">
                                <div class="zoom">
                                    @if(isset($increadibleplace->details[1]))
                                        <a href="{{ asset($increadibleplace->details[1]->getphoto->path.'/thumb_570x400/'.$increadibleplace->details[1]->getphoto->file_name) }}">
                                            <img src="{{ asset($increadibleplace->details[1]->getphoto->path.'/thumb_570x400/'.$increadibleplace->details[1]->getphoto->file_name) }}" alt="">
                                        </a>
                                    @endif
                                </div>
                                <div class="gallery__inner">
                                    <h3 class="gallery__title">
                                        <a href="">{{ $increadibleplace->name }}</a>
                                </div>
                            </div>
                            <div class="xs-6 photo-gallery">
                                <div class="zoom">
                                    @if(isset($increadibleplace->details[2]))
                                        <a href="{{ asset($increadibleplace->details[2]->getphoto->path.'/thumb_570x400/'.$increadibleplace->details[2]->getphoto->file_name) }}">
                                            <img src="{{ asset($increadibleplace->details[2]->getphoto->path.'/thumb_570x400/'.$increadibleplace->details[2]->getphoto->file_name) }}" alt="">
                                        </a>
                                    @endif
                                </div>
                                <div class="gallery__inner">
                                    <h3 class="gallery__title">
                                        <a href="">{{ $increadibleplace->name }}</a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="gap gap-3 mb20-xs">
                            <div class="xs-6  photo-gallery">
                                <div class="zoom">
                                    @if(isset($increadibleplace->details[3]))
                                        <a href="{{ asset($increadibleplace->details[3]->getphoto->path.'/thumb_570x400/'.$increadibleplace->details[3]->getphoto->file_name) }}">
                                            <img src="{{ asset($increadibleplace->details[3]->getphoto->path.'/thumb_570x400/'.$increadibleplace->details[3]->getphoto->file_name) }}" alt="">
                                        </a>
                                    @endif
                                </div>
                                <div class="gallery__inner">
                                    <h3 class="gallery__title">
                                        <a href="">{{ $increadibleplace->name }}</a>
                                </div>
                            </div>
                            <div class="xs-6 photo-gallery">
                                <div class="zoom">
                                    @if(isset($increadibleplace->details[4]))
                                        <a href="{{ asset($increadibleplace->details[4]->getphoto->path.'/'.$increadibleplace->details[4]->getphoto->file_name) }}">
                                            <img src="{{ asset($increadibleplace->details[4]->getphoto->path.'/'.$increadibleplace->details[4]->getphoto->file_name) }}" alt="">
                                        </a>
                                    @endif
                                </div>
                                <div class="gallery__inner">
                                    <h3 class="gallery__title">
                                        <a href="">{{ $increadibleplace->name }}</a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    @if(isset($partners))
        <section class="section section--pad2">
            <div class="section__inner">
                <header class="section__header">
                    <h3 class="section__title t-center">OUR PARTNER</h3>
                </header>
                <div class="our-partners owl-carousel">
                    @foreach($partners as $partner)
                        <div class="item">
                            <a href="{{ $partner->url }}">
                                <img src="{{ asset($partner->getphoto->path.'/'.$partner->getphoto->file_name) }}" alt="">
                            </a>
                        </div>
                    @endforeach                                             
                </div>
            </div>
        </section>
    @endif
    <section class="section section--pad2 section--subs">
        <div class="section__inner">
            <header class="section__header">
                <h3 class="section__title t-center">SUBSCRIBE TO OUR NEWSLETTER</h3>
            </header>

            <div class="subscription">
                <form action="">
                    <input type="text" class="form__control" placeholder="Your email address">
                    <button class="btn btn-blue">Submit</button>
                </form>
            </div>
        </div>
    </section>
    @push('append_js')
        <script>
            $( function() {
                $( "#check-in" ).datepicker({
                  dayNamesMin: [ "S", "M", "T", "W", "T", "F", "S" ],
                  dateFormat: "yy-mm-dd"
                });
                $( "#pickup" ).datepicker({
                  dayNamesMin: [ "S", "M", "T", "W", "T", "F", "S" ],
                  dateFormat: "yy-mm-dd"
                });
            } );
            $('.our-partners').owlCarousel({
                loop:true,
                margin: 10,
                nav: false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:5,
                        margin: 140
                    }
                }
            });

            jarallax(document.querySelectorAll('.jarallax'), {
                speed: 0.2
            });

            $('.main-slider').owlCarousel({
                animateOut: 'fadeOut',
                autoplay: true,
                loop:true,
                dots: false,
                navText: '',
                nav: true,
                items: 1
            });

            $('.parent-container').magnificPopup({
              delegate: 'a',
              type: 'image',
              gallery:{enabled:true}
            });
        </script>
    @endpush
@endsection

