<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageVehicleDetail extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'package_id', 'vehicle_id', 'vehicle_rate'
    ];

    public function vehicle(){
    	return $this->belongsTo(Vehicle::class,'vehicle_id');
    }

    public function package(){
    	return $this->belongsTo(Package::class, 'package_id');
    }
}
