<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelDetailFacility extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'hotel_id', 'facility_id'
    ];
}
