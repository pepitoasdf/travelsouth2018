<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'title', 'description', 'logo', 'background'
    ];
}
