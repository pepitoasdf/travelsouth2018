<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelRoom extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'pricing', 'description', 'hotel_id', 'room_type_id', 'status', 'room_no'
    ];

    public function type(){
    	return $this->belongsTo(HotelRoomType::class,'room_type_id');
    }

    public function getrate(){
        $getrate =  HotelRoomRate::where('effective_date', '<=', date('Y-m-d'))->orderBy('effective_date','desc')->first();
        if(count($getrate) > 0){
    	   return $this->hasOne(HotelRoomRate::class,'room_id')->where('effective_date', '<=', date('Y-m-d'))->orderBy('effective_date','desc');
        }
        else{
            return $this->hasOne(HotelRoomRate::class,'room_id')->orderBy('effective_date','desc');
        }
    }

    public function rates(){
        return $this->hasMany(HotelRoomRate::class,'room_id');
    }

    public static function getName($id = null){
        if($id){
            return static::where('id',$id)->first()->name;
        }
        return '';
    }

    public function getphotos(){
        return $this->hasMany(Photo::class, 'ref_id')->where('model_type','rooms');
    }
}
