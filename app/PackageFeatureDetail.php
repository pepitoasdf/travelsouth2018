<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageFeatureDetail extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'package_id', 'feature_id'
    ];
}
