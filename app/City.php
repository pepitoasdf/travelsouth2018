<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'country', 'province', 'name'
    ];

    public function getcountry(){
    	return $this->belongsTo(Country::class,'country');
    }

    public function getprovince(){
    	return $this->belongsTo(Province::class,'province');
    }

    public function hotels(){
        return $this->hasMany(Hotel::class, 'city_id');
    }
}
