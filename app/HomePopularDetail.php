<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePopularDetail extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'header_id', 'ref_id'
    ];

    public function vehicle(){
    	return $this->belongsTo(Vehicle::class, 'ref_id');
    }
    public function hotel(){
    	return $this->belongsTo(Hotel::class, 'ref_id');
    }
    public function package(){
    	return $this->belongsTo(Package::class, 'ref_id');
    }
}
