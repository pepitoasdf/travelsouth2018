<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePopular extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'title', 'description', 'status', 'type'
    ];

    public function details(){
    	return $this->hasMany(HomePopularDetail::class, 'header_id');
    }
}
