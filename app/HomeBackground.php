<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeBackground extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'title', 'subtitle', 'description', 'status'
    ];

    public function getphoto(){
    	return $this->hasOne(Photo::class,'ref_id')->where('model_type','background')->orderBy('created_at','desc');
    }

}
