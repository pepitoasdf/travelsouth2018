<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'name', 'email', 'address', 'phone','currency_id', 'remarks', 'date', 'service_type'
    ];

    protected $hidden = [
        'request_id'
    ];

    public static function validateRules(){
    	return [
            'name' => 'required|min:5',
            'email' => 'required|email|min:1',
            'address' => 'required|min:10',
            'phone' => 'required|min:1',
            'currency_id' => 'required|min:1',
            'service_type' => 'required',
        ];
    }

    public static function validateMessages(){
    	return [
            'country_id.required' => 'The country field is required.',
            'service_type.required' => 'The service type field is required.',
        ];
    }
}
