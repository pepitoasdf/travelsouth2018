<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelRoomType extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'name', 'description'
    ];
}
