<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageRate extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'package_id', 'rate', 'effective_date'
    ];
}
