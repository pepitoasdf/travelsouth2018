<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'country', 'name'
    ];

    public function getcountry(){
    	return $this->belongsTo(Country::class, 'country');
    }

    public function hotels(){
        return $this->hasMany(Hotel::class, 'province_id');
    }

}
