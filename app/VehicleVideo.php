<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleVideo extends Model
{
     protected $guarded = ['id'];

    protected $fillable = [
        'vehicle_id', 'video_url', 'title', 'description','status'
    ];

    public function getphoto(){
        return $this->hasOne(Photo::class,'ref_id')->where('model_type','vehicles')->orderBy('created_at','desc');
    }
}
