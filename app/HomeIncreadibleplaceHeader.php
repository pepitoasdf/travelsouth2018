<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeIncreadibleplaceHeader extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'title', 'description', 'status'
    ];

   	public function details(){
   		return $this->hasMany(HomeIncreadibleplaceDetail::class,'header_id');
   	}
}