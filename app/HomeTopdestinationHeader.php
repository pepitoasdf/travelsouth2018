<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeTopdestinationHeader extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'title', 'description', 'status'
    ];

   	public function details(){
   		return $this->hasMany(HomeTopdestinationDetail::class,'header_id');
   	}
}
