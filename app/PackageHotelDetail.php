<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageHotelDetail extends Model
{
	protected $guarded = ['id'];

    protected $fillable = [
        'package_id', 'hotel_id', 'room_id', 'room_rate'
    ];

    public function hotel(){
    	return $this->belongsTo(Hotel::class,'hotel_id');
    }

    public function room(){
    	return $this->belongsTo(HotelRoom::class,'room_id');
    }
    public function package(){
    	return $this->belongsTo(Package::class, 'package_id');
    }
}
