<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingDetail extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'service_type', 'hotel_id', 'room_id', 'package_id','vehicle_id', 'rate_type', 'duration', 'start_date', 'end_date',
        'amount','balance'
    ];

    protected $hidden = [
        'header_id'
    ];
}
