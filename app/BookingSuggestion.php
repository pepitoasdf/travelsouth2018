<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingSuggestion extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'remarks', 'service_id', 'duration', 'duration_type','start_date', 'end_date', 'amount'
    ];

    protected $hidden = [
        'request_id'
    ];
}
