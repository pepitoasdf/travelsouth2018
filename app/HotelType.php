<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelType extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'name'
    ];

    public function hotels(){
    	return $this->hasMany(Hotel::class,'type_id');
    }
}
