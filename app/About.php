<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'company', 'description', 'address', 'email','phone','status'
    ];

     public function getphoto(){
        return $this->hasOne(Photo::class,'ref_id')->where('model_type','abouts')->orderBy('created_at','desc');
    }
}
