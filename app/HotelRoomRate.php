<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelRoomRate extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'room_id', 'rate', 'effective_date'
    ];
}
