<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageGuide extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'package_id', 'name', 'description','twitter_url','facebook_url','google_url','linkedin_url'
    ];
    public function getphoto(){
        return $this->hasOne(Photo::class,'ref_id')->where('model_type','package-guides')->orderBy('created_at','desc');
    }
}
