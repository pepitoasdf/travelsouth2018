<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'name', 'email', 'phone', 'address','status'
    ];
}
