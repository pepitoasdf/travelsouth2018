<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageFeature extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'description'
    ];
}
