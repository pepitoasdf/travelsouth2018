<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'name', 'description', 'color', 'plate_no', 'year_model', 'mv_file_no', 'engine_no', 'no_of_wheel', 'transmission', 'seats', 'doors', 'aircon','status', 'pricing'
    ];

    public static function insertData($request){
        $vehicle = static::create($request->all());
        
        if($vehicle){
            for ($i=0; $i < count($request->input('feature_id')); $i++) { 
                static::insertFeatures($vehicle->id, $request->input('feature_id')[$i]);
            }

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'vehicles',$vehicle->id);
            }
        }

        return $vehicle;
    }

    public static function insertFeatures($vehicle_id, $input){
        VehicleFeatureDetail::create([
            'vehicle_id' => $vehicle_id,
            'feature_id' => $input
        ]);
    }

    public function getrate(){
        $getrate =  VehicleRate::where('effective_date', '<=', date('Y-m-d'))->orderBy('effective_date','desc')->first();
        if(count($getrate) > 0){
            return $this->hasOne(VehicleRate::class,'vehicle_id')->where('effective_date', '<=', date('Y-m-d'))->orderBy('effective_date','desc');
        }
        else{
            return $this->hasOne(VehicleRate::class,'vehicle_id')->orderBy('effective_date','desc');
        }
    }

    public function drivergetrate(){
        $getrate =  VehicleDriverRate::where('effective_date', '<=', date('Y-m-d'))->orderBy('effective_date','desc')->first();
        if(count($getrate) > 0){
            return $this->hasOne(VehicleDriverRate::class,'vehicle_id')->where('effective_date', '<=', date('Y-m-d'))->orderBy('effective_date','desc');
        }
        else{
            return $this->hasOne(VehicleDriverRate::class,'vehicle_id')->orderBy('effective_date','desc');
        }
    }

    public function featuredetails(){
        return $this->belongsToMany(VehicleFeature::class,'vehicle_feature_details', 'vehicle_id', 'feature_id');
    }

    public function rates(){
        return $this->hasMany(VehicleRate::class,'vehicle_id');
    }

    public function driverrates(){
        return $this->hasMany(VehicleDriverRate::class,'vehicle_id');
    }

    public function features(){
        return $this->hasMany(VehicleFeatureDetail::class,'vehicle_id');
    }

    public static function getName($id = null){
        if($id){
            return static::where('id',$id)->first()->name;
        }
        return '';
    }
    public function getphoto(){
        return $this->hasOne(Photo::class,'ref_id')->where('model_type','vehicles')->orderBy('created_at','desc');
    }

    public function getvideos(){
        return $this->hasMany(VehicleVideo::class, 'vehicle_id')->where('status','Published');
    }
}
