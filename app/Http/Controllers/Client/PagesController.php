<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HomeBackground;
use App\HomePopular;
use App\HomeVideo;
use App\HomeTopdestinationHeader;
use App\HomeTopdestinationDetail;
use App\HomeIncreadibleplaceHeader;
use App\HomeIncreadibleplaceDetail;
use App\HomePartner;
use App\Photo;
use App\Package;
use App\Hotel;
use App\HotelRoom;
use App\Vehicle;
use App\About;
use App\PackageItinerary;
use App\PackageGuide;
use App\Contact;
class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => 'account']);
    }
    public function index()
    {
        $popular = HomePopular::where('status','Published')
                    ->with(['details' => function($q){
                        $q->orderBy('updated_at', 'desc')
                        ->limit(6);
                    }])
                    ->orderBy('updated_at','desc')
                    ->first();
        $backgrounds = HomeBackground::where('status','Published')
                    ->orderBy('updated_at','desc')
                    ->limit(10)
                    ->get();

        $video = HomeVideo::where('status','Published')
                ->orderBy('updated_at','desc')
                ->first();

        $topdestination = HomeTopdestinationHeader::where('status','Published')
                ->with(['details' => function($q){
                    $q->orderBy('updated_at', 'desc')
                    ->limit(6);
                }])
                ->orderBy('updated_at','desc')
                ->first();

        $increadibleplace = HomeIncreadibleplaceHeader::where('status','Published')
                ->with(['details' => function($q){
                    $q->orderBy('updated_at', 'desc')
                    ->limit(5);
                }])
                ->orderBy('updated_at','desc')
                ->first();

        $partners  = HomePartner::where('status','Published')
                    ->orderBy('updated_at','desc')
                    ->get();

        return view('welcome',compact('backgrounds','popular','video','increadibleplace','partners','topdestination'));
    }
    public function packages()
    {
        $datas = Package::where('status','Published')
                ->orderBy('updated_at','desc')
                ->paginate(9);

        $option = ['title'=> "Packages List", 'header' => "Packages"];

        return view('client.pages.lists', compact('datas','option'));
    }
    public function hotels()
    {
        $datas = Hotel::where('status','Published')
                ->orderBy('updated_at','desc')
                ->paginate(9);

        $option = ['title'=> "Hotels List", 'header' => "Hotels"];
        return view('client.pages.lists', compact('datas','option'));
    }
    public function vehicles()
    {
        $datas = Vehicle::where('status','Published')
                ->orderBy('updated_at','desc')
                ->paginate(9);

        $option = ['title'=> "Vehicles List", 'header' => "Vehicles"];
        return view('client.pages.lists', compact('datas','option'));
    }
    public function hotelIndividual($id, $slug){
        $hotel = Hotel::findOrFail($id);
        $photos = Photo::where('ref_id',$id)
                    ->where('model_type','hotels')
                    ->get();

        $rooms = HotelRoom::where('hotel_id',$id)
                ->where('status','Published')
                ->groupBy('room_type_id')
                ->get();

        $recommended = Hotel::where('id', '!=', $id)->where('status','Published')
                ->limit(9)
                ->inRandomOrder()
                ->get();

        return view('client.pages.hotels.individual', compact('hotel','photos','rooms','slug','recommended'));
    }

    public function vehicleIndividual($id, $slug){
        $vehicle = Vehicle::findOrFail($id);
        $photos = Photo::where('ref_id',$id)
                    ->where('model_type','vehicles')
                    ->get();

        $recommended = Vehicle::where('id', '!=', $id)->where('status','Published')
                ->limit(9)
                ->inRandomOrder()
                ->get();

        return view('client.pages.vehicles.individual', compact('vehicle','photos','slug','recommended'));
    }

    public function packageIndividual($id, $slug){
        $package = Package::findOrFail($id);
        $photos = Photo::where('ref_id',$id)
                    ->where('model_type','packages')
                    ->get();

        $recommended = Package::where('id', '!=', $id)->where('status','Published')
                ->limit(9)
                ->inRandomOrder()
                ->get();

        return view('client.pages.packages.individual', compact('package','photos','slug','recommended'));
    }

    public function about()
    {
        $about = About::where('status','Published')
                ->orderBy('updated_at','desc')
                ->first();

        return view('client.pages.about', compact('about'));
    }
    public function contact()
    {
        $contact = Contact::where('status','Published')
                ->orderBy('updated_at','desc')
                ->first();

        return view('client.pages.contact', compact('contact'));
    }
    public function account()
    {
        return view('client.pages.account');
    }
}
