<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HomeVideo;
use App\Authorizable;
class HomeVideoController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = HomeVideo::orderBy('status','asc')->orderBy('updated_at','desc')->paginate($this->page_num);
        return view('admin.pages.home.videos.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.home.videos.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:1',
            'url' => 'required|url|min:1',
            'description' => 'required|min:1',
            'status' => 'required|min:1'
        ]);

        $fill = $request->all();

        if ( $video = HomeVideo::create($fill)) {

            $request->session()->flash('toast-alert-success', 'Video url successfully added.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create video url.');
        }

        return redirect()->route('videos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = HomeVideo::findOrFail($id);
        return view('admin.pages.home.videos.edit', compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|min:1',
            'url' => 'required|url|min:1',
            'description' => 'required|min:1',
            'status' => 'required|min:1'
        ]);

        $video = HomeVideo::findOrFail($id);

        $fill = $request->all();

        $video->fill($fill);

        $video->save();

        $request->session()->flash('toast-alert-success', 'video url successfully updated.');

        return redirect()->route('videos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = HomeVideo::findOrFail($id);

        if( $video->delete() ) {
            $request->session()->flash('toast-alert-success', 'Video url successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Video url not deleted.');
        }

        return redirect()->back();
    }
}
