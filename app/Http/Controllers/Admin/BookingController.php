<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Authorizable;
use App\Booking;
use App\Country;
class BookingController extends Controller
{
    use Authorizable;
    
    public function __construct(){
        $this->page_num = 10;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Booking::orderBy('status','asc')->orderBy('id','desc')->paginate($this->page_num);
        return view('admin.booking.bookings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country = Country::where('currency_code','!=','')->get();
        return view('admin.booking.bookings.new', compact('country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            Booking::validateRules(),
            Booking::validateMessages()
        );

        $fill = $request->except('status','type');
        $fill['status'] = "New";
        $fill['type'] = "Booked";

        $booking = Booking::create($fill);
        if($booking){
            $request->session()->flash('toast-alert-success', 'Booking Charges successfully created.');
        }
        else{
            $request->session()->flash('toast-alert-danger', 'Unable to create booking charges.');
        }
        return redirect()->route('bookings.edit',$booking->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $booking = Booking::where('id',$id)->firstOrFail();
                
        return view('admin.booking.bookings.edit', compact('booking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $booking = Booking::findOrFail($id);

        $this->validate($request,
            Booking::validateRules(),
            Booking::validateMessages()
        );

        $fill = $request->except('status','type');

        if($request->input('update')){
            $fill['status'] = "New";
            $booking->fill($fill);
            $booking->save();
            $request->session()->flash('toast-alert-success', 'Booking Charges successfully updated.');
        }
        elseif($request->input('finalize')){
            $booking->status = "Finalized";
            $booking->save();
            $request->session()->flash('toast-alert-success', 'Booking Charges successfully finalized.');
        }
        elseif($request->input('approve')){
            $booking->status = "Approved";
            $booking->save();
            $request->session()->flash('toast-alert-success', 'Booking Charges successfully approved.');
        }
        elseif($request->input('void')){
            $booking->status = "Void";
            $booking->save();
            $request->session()->flash('toast-alert-success', 'Booking Charges successfully voided.');
        }
        return redirect()->route('bookings.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $booking = Booking::findOrFail($id);

        if( $booking->delete() ) {
            $request->session()->flash('toast-alert-success', 'Booking successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Booking not deleted.');
        }

        return redirect()->back();
    }
}
