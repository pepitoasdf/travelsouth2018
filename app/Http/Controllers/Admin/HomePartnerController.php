<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HomePartner;
use App\Authorizable;
use App\Photo;
class HomePartnerController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = HomePartner::orderBy('status','asc')->orderBy('updated_at','desc')->paginate($this->page_num);
        return view('admin.pages.home.partners.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.home.partners.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'url' => 'required|url|min:1',
            'status' => 'required|min:1'
        ]);

        $fill = $request->all();

        if ( $partner = HomePartner::create($fill)) {

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'partners',$partner->id);
            }

            $request->session()->flash('toast-alert-success', 'Partner successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create partner.');
        }

        return redirect()->route('partners.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partner = HomePartner::findOrFail($id);
        $photos = Photo::where('ref_id',$partner->id)
                ->where('model_type','partners')
                ->get();
        return view('admin.pages.home.partners.edit', compact('partner','photos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'url' => 'required|url|min:1',
            'status' => 'required|min:1',
        ]);

        $partner = HomePartner::findOrFail($id);

        $fill = $request->all();

        $partner->fill($fill);

        $partner->save();
        
        if($request->hasFile('images')){
            Photo::upload($request->file('images'),'partners',$partner->id);
        }

        if(count($request->input('removephotoid')) && count($request->input('removephotoname'))){
            Photo::removeFile($request->input('removephotoid'));
        }
        $request->session()->flash('toast-alert-success', 'Partner successfully updated.');

        return redirect()->route('partners.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $partner = HomePartner::findOrFail($id);

        if( $partner->delete() ) {
            Photo::removeByReference($id,'partners');
            $request->session()->flash('toast-alert-success', 'Partner successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Partner not deleted.');
        }

        return redirect()->back();
    }
}
