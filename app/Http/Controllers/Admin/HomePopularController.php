<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HomePopular;
use App\Authorizable;
use App\Vehicle;
use App\Hotel;
use App\Package;
use App\HomePopularDetail;
use DB;
class HomePopularController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = HomePopular::orderBy('updated_at','desc')->paginate($this->page_num);
        return view('admin.pages.home.popular.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.home.popular.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vkey = $this->vkey($request);


        $this->validate($request, [
            'title' => 'required|min:1',
            'type' => 'required|min:1',
            'description' => 'required|min:1',
            'status' => 'required|min:1',
            $vkey => 'required|array|min:1'
        ]);

        $fill = $request->all();

        if ( $popular = HomePopular::create($fill)) {
            for ($i=0; $i < count($request->input($vkey)); $i++) { 
                HomePopularDetail::create([
                    'header_id' => $popular->id,
                    'ref_id' => $request->input($vkey)[$i]
                ]);
            }

            $request->session()->flash('toast-alert-success', 'Popular successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create popular.');
        }

        return redirect()->route('popular.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $popular = HomePopular::findOrFail($id);
        return view('admin.pages.home.popular.edit', compact('popular'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vkey = $this->vkey($request);

        $this->validate($request, [
            'title' => 'required|min:1',
            'type' => 'required|min:1',
            'description' => 'required|min:1',
            'status' => 'required|min:1',
            $vkey => 'required|array|min:1'
        ]);

        $popular = HomePopular::findOrFail($id);

        $fill = $request->all();

        $popular->fill($fill);

        $popular->save();
        if($popular){
            HomePopularDetail::where('header_id',$popular->id)->delete();
            for ($i=0; $i < count($request->input($vkey)); $i++) { 
                HomePopularDetail::create([
                    'header_id' => $popular->id,
                    'ref_id' => $request->input($vkey)[$i]
                ]);
            }

            $request->session()->flash('toast-alert-success', 'Popular successfully updated.');
        }
        return redirect()->route('popular.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $popular = HomePopular::findOrFail($id);

        if( $popular->delete() ) {
            HomePopularDetail::where('header_id',$id)->delete();
            $request->session()->flash('toast-alert-success', 'Popular successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Popular not deleted.');
        }

        return redirect()->back();
    }


    private function vkey($request){
        if($request->input('type') == "Packages"){
            $vkey = 'service_packages';
        }
        else if($request->input('type') == "Vehicles"){
            $vkey = 'service_vehicles';
        }
        else if($request->input('type') == "Hotels"){
            $vkey = 'service_hotels';
        }
        return $vkey;
    }
}
