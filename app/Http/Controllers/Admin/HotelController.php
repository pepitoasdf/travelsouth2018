<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Authorizable;
use App\HotelRoomType;
use App\HotelType;
use App\HotelFacility;
use App\Hotel;
use App\HotelDetailFacility;
use Image;
use App\Photo;
use File;
class HotelController extends Controller
{
    use Authorizable;
    
    public function __construct(){
        $this->page_num = 10;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Hotel::orderBy('id','desc')->paginate($this->page_num);
        return view('admin.hotels.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.hotels.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'description' => 'required|min:1',
            'country_id' => 'required|min:1',
            'province_id' => 'required|min:1',
        ],
        [
            'country_id.required' => 'The country field is required.',
            'province_id.required' => 'The province field is required.',
        ]
        );

        $success = Hotel::insertData($request);
        if($success){
            $request->session()->flash('toast-alert-success', 'Hotel successfully created.');
        }
        else{
            $request->session()->flash('toast-alert-danger', 'Unable to create hotel.');
        }
        return redirect()->route('hotels.edit',$success->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hotel = Hotel::where('id',$id)->with('facilities')->firstOrFail();
        $photos = Photo::where('ref_id',$hotel->id)
                ->where('model_type','hotels')
                ->get();
                
        return view('admin.hotels.edit', compact('hotel','photos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'description' => 'required|min:1',
            'country_id' => 'required|min:1',
            'province_id' => 'required|min:1',
        ],
        [
            'country_id.required' => 'The country field is required.',
            'province_id.required' => 'The province field is required.',
        ]
        );

        $hotel = Hotel::findOrFail($id);

        $hotel->fill($request->except('facility_id'));
        $hotel->save();
        if($hotel){
            $hotel->facilities()->delete();
            for ($i=0; $i < count($request->input('facility_id')); $i++) { 
                Hotel::insertFacilities($hotel->id, $request->input('facility_id')[$i]);
            }

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'hotels',$hotel->id);
            }

            if(count($request->input('removephotoid')) && count($request->input('removephotoname'))){
                Photo::removeFile($request->input('removephotoid'));
            }

            $request->session()->flash('toast-alert-success', 'Hotel successfully updated.');
        }
        else{
             $request->session()->flash('toast-alert-danger', 'Something went wrong. Try again.');
        }
        return redirect()->route('hotels.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $hotel = Hotel::findOrFail($id);

        if( $hotel->delete() ) {
            Photo::removeByReference($id,'hotels');
            $request->session()->flash('toast-alert-success', 'Hotel successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Hotel not deleted.');
        }

        return redirect()->back();
    }
}
