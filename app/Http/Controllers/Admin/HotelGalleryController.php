<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Hotel;
use App\HotelRoom;
use App\Photo;
class HotelGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $hotel = Hotel::findOrFail($id);
        $rooms = HotelRoom::where('hotel_id',$id)->get();
        $hotelPhotos = Photo::where('ref_id',$id)
                        ->where('model_type','hotels')
                        ->get();
        return view('admin.hotels.galleries.index',compact('hotel','rooms','hotelPhotos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $hotel = Hotel::findOrFail($id);
        return view('admin.hotels.galleries.new',compact('hotel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if($request->hasFile('images')){
            Photo::upload($request->file('images'),'hotels',$id);
            $request->session()->flash('toast-alert-success', 'Photos successfully added.');
        }

        return redirect()->route('hgalleries.index', $id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }

    public function roomIndex($id,$gallery){
        $hotel = Hotel::findOrFail($id);
        $room = HotelRoom::findOrFail($gallery);
        $roomPhotos = Photo::where('ref_id',$room->id)
                        ->where('model_type','rooms')
                        ->get();
        return view('admin.hotels.galleries.room-index',compact('hotel','room','roomPhotos'));

    }

    public function roomCreate($id, $gallery){
        $hotel = Hotel::findOrFail($id);
        $room = HotelRoom::findOrFail($gallery);
        return view('admin.hotels.galleries.room-new',compact('hotel','room'));
    }

    public function roomStore(Request $request, $id, $gallery){
        if($request->hasFile('images')){
            Photo::upload($request->file('images'),'rooms',$gallery);
            $request->session()->flash('toast-alert-success', 'Photos successfully added.');
        }

        return redirect()->route('hgalleries.roomIndex',['id' => $id, 'hgallery' => $gallery]);
    }
}
