<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Authorizable;
class ContactController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Contact::orderBy('updated_at','desc')->paginate($this->page_num);
        return view('admin.pages.contacts.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.contacts.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'email' => 'required|string|email|max:255|unique:contacts',
            'phone' => 'required|min:1',
            'address' => 'required|min:6',
        ]);

        if ( $contact = Contact::create($request->all()) ) {

            $request->session()->flash('toast-alert-success', 'Contact successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create contact.');
        }

        return redirect()->route('contacts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = Contact::findOrFail($id);

        return view('admin.pages.contacts.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'email' => 'required|string|email|max:255|unique:contacts',
            'phone' => 'required|min:1',
            'address' => 'required|min:6',
        ]);

        $contact = Contact::findOrFail($id);

        $contact->fill($request->all());

        if($contact->isDirty()){
            $contact->save();
            $request->session()->flash('toast-alert-success', 'Contact successfully updated.');
        }
        else{
            $request->session()->flash('toast-alert-info', 'Nothing to update.');
        }

        return redirect()->route('contacts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $contact = Contact::findOrFail($id);

        if( $contact->delete() ) {
            $request->session()->flash('toast-alert-success', 'Contact successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Contact not deleted.');
        }

        return redirect()->back();
    }
}
