<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HotelRoom;
use App\Hotel;
use App\Photo;
use File;
use Image;
use App\HotelRoomRate;
class HotelRoomRateController extends Controller
{
    //use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $hotel = Hotel::findOrFail($id);
        $results = HotelRoom::where('hotel_id',$id)->orderBy('room_type_id')->paginate($this->page_num);
        return view('admin.hotels.rates.index', compact('results','hotel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$room)
    {
        $hotel = Hotel::findOrFail($id);

        $room = HotelRoom::findOrFail($room);

        return view('admin.hotels.rates.edit', compact('room','hotel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $room)
    {  //return dd($request->input());
        $room = HotelRoom::findOrFail($room);
        if($room){
            for ($x=0; $x < count($request->input('remove_id')); $x++) { 
                $check = HotelRoomRate::find($request->input('remove_id')[$x]);
                if($check){
                    $check->delete();
                }
            }
            for ($i=0; $i < count($request->input('rate_id')); $i++) { 
                $rate = HotelRoomRate::where('id',$request->input('rate_id')[$i])
                        ->where('room_id',$room->id)
                        ->first();
                if($rate){
                    $rate->rate = $request->input('rate')[$i];
                    $rate->effective_date = date('Y-m-d',strtotime($request->input('effective_date')[$i]));
                    $rate->save();
                }
                else{
                    HotelRoomRate::create([
                        'room_id' => $room->id,
                        'rate' => $request->input('rate')[$i],
                        'effective_date' => date('Y-m-d',strtotime($request->input('effective_date')[$i]))
                    ]);
                }
            }
            $request->session()->flash('toast-alert-success', 'Room rates  successfully updated.');
        }

        return redirect()->route('roomrates.edit',['id' => $id, 'roomrate' => $room]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
