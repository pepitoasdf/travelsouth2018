<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HomeTopdestinationDetail;
use App\HomeTopdestinationHeader;
use App\Authorizable;
use DB;
use App\Photo;
use Session;
class HomeTopdestinationController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = HomeTopdestinationHeader::orderBy('status','asc')->orderBy('updated_at','desc')->paginate($this->page_num);
        return view('admin.pages.home.topdestinations.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.pages.home.topdestinations.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:1',
            'description' => 'required|min:1',
            'status' => 'required|min:1'
        ]);

        $fill = $request->all();
        if ( $topdestination = HomeTopdestinationHeader::create($fill)) {

            $request->session()->flash('toast-alert-success', 'Top Destination Header successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create Top Destination Header.');
        }

        return redirect()->route('topdestinations.edit',$topdestination);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topdestination = HomeTopDestinationHeader::findOrFail($id);
        return view('admin.pages.home.topdestinations.edit', compact('topdestination','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $topdestination = HomeTopdestinationHeader::findOrFail($id);

        if($request->input('title') && $request->input('description') && $request->input('status')){
            $this->validate($request, [
                'title' => 'required|min:1',
                'description' => 'required|min:1',
                'status' => 'required|min:1'
            ]);

            $fill = $request->all();
            $topdestination->fill($fill);
            $topdestination->save();

            $request->session()->flash('toast-alert-success', 'Top Destination successfully updated.');

        }
        else if($request->input('name') && $request->input('location') && $request->hasFile('image')){
            $this->validate($request, [
                'name' => 'required|min:1'
            ]);
            
            $place = explode('|', $request->input('location'));

            $fill = $request->all();
            $fill['header_id'] = $id;
            $fill['city_id'] = $place[2];
            $fill['province_id'] = $place[1];
            $fill['country_id'] = $place[0];

            if($request->input('detail_id')){
                $detail = HomeTopdestinationDetail::findOrFail($detail->id);
                $detail->fill($fill);
                $detail->save();

                $photo  = Photo::where('model_type','topdestinations')
                        ->where('ref_id',$detail->id)
                        ->first();
                if($photo){
                    Photo::removeFile($photo->id);
                }

                if($request->hasFile('image')){
                    Photo::upload($request->file('image'),'topdestinations',$detail->id);
                }
                $request->session()->flash('toast-alert-success', 'Top Destination Details successfully updated.');
            }
            else{
                $detail = HomeTopdestinationDetail::create($fill);
                if($detail){
                    Photo::upload($request->file('image'), 'topdestinations', $detail->id);
                }

                $request->session()->flash('toast-alert-success', 'Top Destination Details successfully added.');
            }
        }
        else{
            $request->session()->flash('toast-alert-danger', 'Something went wrong. Try again.');
        }
        return redirect()->route('topdestinations.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $topdestination = HomeTopdestinationHeader::findOrFail($id);
        if($request->input('detal_id')){
            $detail = HomeTopdestinationDetail::find($request->input('detal_id'));
            if($detail){
                Photo::removeByReference($detail->id,'topdestinations');
                $detail->delete();
            }
        }
        else{
            if( $topdestination->delete() ) {
                $details = HomeTopdestinationDetail::where('header_id',$id)->get();
                foreach ($details as $detail) {
                    Photo::removeByReference($detail->id,'topdestinations');
                }
                
                HomeTopdestinationDetail::where('header_id',$id)->delete();

                $request->session()->flash('toast-alert-success', 'Top Destination successfully deleted.');
            } else {
                $request->session()->flash('toast-alert-warning', 'Top Destination not deleted.');
            }
        }

        return redirect()->back();
    }
}
