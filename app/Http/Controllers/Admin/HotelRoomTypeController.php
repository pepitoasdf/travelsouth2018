<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Authorizable;
use App\HotelRoomType;
Use App\Hotel;
class HotelRoomTypeController extends Controller
{
    use Authorizable;
    
    public function __construct(){
        $this->page_num = 10;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = HotelRoomType::latest()->paginate($this->page_num);
        return view('admin.settings.hotels.roomtypes.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.settings.hotels.roomtypes.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:1'
        ]);

        if ( $roomtype = HotelRoomType::create($request->all()) ) {

            $request->session()->flash('toast-alert-success', 'Room Type successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create room type.');
        }

        return redirect()->route('roomtypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roomtype = HotelRoomType::findOrFail($id);

        return view('admin.settings.hotels.roomtypes.edit', compact('roomtype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:1'
        ]);

        $roomtype = HotelRoomType::findOrFail($id);

        // Update roomtype
        $roomtype->fill($request->all());

        if($roomtype->isDirty()){
            $roomtype->save();
            $request->session()->flash('toast-alert-success', 'Room type successfully updated.');
        }
        else{
            $request->session()->flash('toast-alert-info', 'Nothing to update.');
        }

        return redirect()->route('roomtypes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $roomtype = HotelRoomType::findOrFail($id);
        
        if( $roomtype->delete() ) {
            $request->session()->flash('toast-alert-success', 'Room type successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Room type not deleted.');
        }

        return redirect()->back();
    }
}
