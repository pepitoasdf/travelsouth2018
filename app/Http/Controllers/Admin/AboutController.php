<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Authorizable;
use App\About;
use App\Photo;
class AboutController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = About::orderBy('updated_at','desc')->paginate($this->page_num);
        return view('admin.pages.abouts.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.abouts.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company' => 'required|min:1',
            'description' => 'required|min:15',
            'address' => 'required|min:1',
            'email' => 'required|string|email|max:255|unique:abouts',
            'phone' => 'required|min:6'
        ]);

        if ( $about = About::create($request->all()) ) {

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'abouts',$about->id);
            }

            $request->session()->flash('toast-alert-success', 'About successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create about.');
        }

        return redirect()->route('abouts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = About::findOrFail($id);

        return view('admin.pages.abouts.edit', compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'company' => 'required|min:1',
            'description' => 'required|min:15',
            'address' => 'required|min:1',
            'email' => 'required|string|email|max:255|unique:abouts',
            'phone' => 'required|min:6'
        ]);

        $about = About::findOrFail($id);

        $about->fill($request->all());

        $about->save();
        
        if($request->hasFile('images')){
            Photo::upload($request->file('images'),'abouts',$about->id);
        }

        if(count($request->input('removephotoid')) && count($request->input('removephotoname'))){
            Photo::removeFile($request->input('removephotoid'));
        }
        $request->session()->flash('toast-alert-success', 'About successfully updated.');

        return redirect()->route('abouts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $about = About::findOrFail($id);
        if( $about->delete() ) {
            Photo::removeByReference($id,'abouts');
            $request->session()->flash('toast-alert-success', 'About successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'About not deleted.');
        }

        return redirect()->back();
    }
}
