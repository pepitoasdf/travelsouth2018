<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Authorizable;
class DashboardController extends Controller
{
	use Authorizable;
	
    public function index(){
    	return view('admin.dashboard.index');
    }

    public function admin(){
    	return redirect()->route('dashboard.index');          
    }
}
