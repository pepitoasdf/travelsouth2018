<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Authorizable;
use App\HotelRoom;
use App\Hotel;
use App\Photo;
use File;
use Image;
use App\HotelRoomRate;
class HotelRoomController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $hotel = Hotel::findOrFail($id);
        $results = HotelRoom::where('hotel_id',$id)->orderBy('room_type_id')->paginate($this->page_num);
        return view('admin.hotels.rooms.index', compact('results','hotel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $hotel = Hotel::findOrFail($id);
        return view('admin.hotels.rooms.new',compact('hotel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $hotel = Hotel::findOrFail($id);

        $this->validate($request, [
            'pricing' => 'required|min:1',
            'description' => 'required|min:1',
            'room_type_id' => 'required|min:1',
            'rate' => 'required|min:1|numeric',
            'room_no' => 'required|min:1|numeric',
        ],
        [
            'room_type_id.required' => 'The room type field is required.',
            'rate.required' => 'The rate per day field is required.',
            'room_no.required' => 'The number of room field is required.',
            'room_no.numeric' => 'The number of room field must be numeric.',
        ]);

        $fill = $request->except('hotel_id','rate','effective_date');
        $fill['hotel_id'] = $hotel->id;

        if ( $room = HotelRoom::create($fill) ) {
            HotelRoomRate::create([
                'room_id' => $room->id,
                'rate' => $request->input('rate'),
                'effective_date' => date('Y-m-d',strtotime($request->input('effective_date')))
            ]);

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'rooms',$room->id);
            }

            $request->session()->flash('toast-alert-success', 'Room successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create room.');
        }

        return redirect()->route('rooms.index',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$room)
    {
        $hotel = Hotel::findOrFail($id);

        $room = HotelRoom::findOrFail($room);

        $photos = Photo::where('ref_id',$room->id)
                ->where('model_type','rooms')
                ->get();
        return view('admin.hotels.rooms.edit', compact('room','hotel','photos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $room)
    {
        $this->validate($request, [
            'pricing' => 'required|min:1',
            'description' => 'required|min:1',
            'room_type_id' => 'required|min:1',
            'rate' => 'required|min:1|numeric',
            'room_no' => 'required|min:1|numeric',
        ],
        [
            'room_type_id.required' => 'The room type field is required.',
            'rate.required' => 'The rate per day field is required.',
            'room_no.required' => 'The number of room field is required.',
            'room_no.numeric' => 'The number of room field must be numeric.',
        ]);

        $room = HotelRoom::findOrFail($room);
        if($room){
            $fill = $request->except('hotel_id');
            $fill['hotel_id'] = $id;
            $room->fill($fill);
            $room->save();

            $rate = HotelRoomRate::where('id',$request->input('rate_id'))
                    ->where('room_id',$room->id)
                    ->where('effective_date',$request->input('effective_date'))
                    ->first();
            if($rate){
                $rate->rate = $request->input('rate');
                $rate->effective_date = date('Y-m-d',strtotime($request->input('effective_date')));
                $rate->save();
            }
            else{
                HotelRoomRate::create([
                    'room_id' => $room->id,
                    'rate' => $request->input('rate'),
                    'effective_date' => date('Y-m-d',strtotime($request->input('effective_date')))
                ]);
            }

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'rooms',$room->id);
            }

            if(count($request->input('removephotoid')) && count($request->input('removephotoname'))){
                Photo::removeFile($request->input('removephotoid'));
            }
            
            $request->session()->flash('toast-alert-success', 'Room  successfully updated.');
        }

        return redirect()->route('rooms.index',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id, $room)
    {
        $room = HotelRoom::findOrFail($room);

        if( $room->delete() ) {
            Photo::removeByReference($room,'rooms');
            $request->session()->flash('toast-alert-success', 'Room successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Room not deleted.');
        }

        return redirect()->back();
    }
}
