<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HotelVideo;
use App\Hotel;
use App\Authorizable;
use App\Photo;
class HotelVideoController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $hotel = Hotel::findOrFail($id);
        $results = HotelVideo::where('hotel_id',$id)->orderBy('id')->paginate($this->page_num);
        return view('admin.hotels.videos.index', compact('results','hotel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $hotel = Hotel::findOrFail($id);
        return view('admin.hotels.videos.new',compact('hotel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $hotel = Hotel::findOrFail($id);

        $this->validate($request, [
            'title' => 'required|min:1',
            'description' => 'required|min:1',
            'video_url' => 'required|url|min:1'
        ]);

        $fill = $request->all();
        $fill['hotel_id'] = $hotel->id;

        if ( $video = HotelVideo::create($fill) ) {

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'hotel-videos',$video->id);
            }

            $request->session()->flash('toast-alert-success', 'Video successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create video.');
        }

        return redirect()->route('hvideos.index',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $video)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $video)
    {
        $hotel = Hotel::findOrFail($id);

        $video = HotelVideo::findOrFail($video);

        $photos = Photo::where('ref_id',$video->id)
                ->where('model_type','hotel-videos')
                ->get();
        return view('admin.hotels.videos.edit', compact('video','hotel','photos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $video)
    {
        $hotel = Hotel::findOrFail($id);
        $video = HotelVideo::findOrFail($video);

        $this->validate($request, [
            'title' => 'required|min:1',
            'description' => 'required|min:1',
            'video_url' => 'required|url|min:1'
        ]);

        if($video){
            $fill = $request->except('hotel_id');
            $fill['hotel_id'] = $id;
            $video->fill($fill);
            $video->save();

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'hotel-videos',$video->id);
            }

            if(count($request->input('removephotoid')) && count($request->input('removephotoname'))){
                Photo::removeFile($request->input('removephotoid'));
            }
            
            $request->session()->flash('toast-alert-success', 'Video  successfully updated.');
        }

        return redirect()->route('hvideos.index',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $video)
    {
        $getvideo = HotelVideo::findOrFail($video);

        if( $getvideo->delete() ) {
            Photo::removeByReference($video,'hotel-videos');
            $request->session()->flash('toast-alert-success', 'Video successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Video not deleted.');
        }

        return redirect()->back();
    }
}
