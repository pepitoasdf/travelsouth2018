<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Authorizable;
use File;
class SystemAdminController extends Controller
{
    use Authorizable;
    
    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = Admin::orderBy('id','desc')->first();
        return view('admin.settings.system.admin.index', compact('admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:1',
            'description' => 'required|min:1'
        ]);
        $admin = Admin::first();
        
        $destinationPath = 'admin/images/uploads/system';
        
        if($request->hasFile('logo')){
            $filename_logo = uniqid().'_'.time() . '.' . $request->logo->getClientOriginalExtension();
            $request->logo->move($destinationPath, $filename_logo);

            $fill['logo'] = $destinationPath.'/'.$filename_logo;
            if($admin){
                File::delete($admin->logo);
            }
        }

        if($request->hasFile('background')){
            $filename_background = uniqid().'_'.time() . '.' . $request->background->getClientOriginalExtension();
            $request->background->move($destinationPath, $filename_background);

            $fill['background'] = $destinationPath.'/'.$filename_background;
            if($admin){
                File::delete($admin->background);
            }
        }

        $fill['title'] = $request->input('title');
        $fill['description'] = $request->input('description');

        if($admin){
            $admin->fill($fill);
            $admin->save();
            if($admin){
                
                $request->session()->flash('toast-alert-success', 'Admin Setting successfully updated.');
            }
        }
        else{
            $admin = Admin::create($fill);
            if($admin){
                $request->session()->flash('toast-alert-success', 'Admin Setting successfully created.');
            }
            else{
                $request->session()->flash('toast-alert-danger', 'Unable to create Admin Setting.');
            }
        }

        return redirect()->route('admin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }
}
