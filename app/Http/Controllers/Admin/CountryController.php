<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Authorizable;
use App\Country;
class CountryController extends Controller
{
    use Authorizable;
    
    public function __construct(){
        $this->page_num = 10;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Country::latest()->paginate($this->page_num);
        return view('admin.settings.places.countries.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.places.countries.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'currency_name' => 'required|min:1'

        ]);

        if ( $country = Country::create($request->all()) ) {

            $request->session()->flash('toast-alert-success', 'Country successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create country.');
        }

        return redirect()->route('countries.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::findOrFail($id);

        return view('admin.settings.places.countries.edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'currency_name' => 'required|min:1'

        ]);

        $country = Country::findOrFail($id);

        $country->fill($request->all());

        if($country->isDirty()){
            $country->save();
            $request->session()->flash('toast-alert-success', 'Country successfully updated.');
        }
        else{
            $request->session()->flash('toast-alert-info', 'Nothing to update.');
        }

        return redirect()->route('countries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $country = Country::findOrFail($id);
        if($country->hotels->count()){
            $request->session()->flash('toast-alert-danger', 'Country already in used.');
            return redirect()->back();
        }
        if( $country->delete() ) {
            $request->session()->flash('toast-alert-success', 'Country successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Country not deleted.');
        }

        return redirect()->back();
    }
}
