<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Authorizable;
use App\PackageFeature;
class PackageFeatureController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = PackageFeature::latest()->paginate($this->page_num);
        return view('admin.settings.packages.features.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.packages.features.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:1',
        ]);

        if ( $feature = PackageFeature::create($request->all()) ) {

            $request->session()->flash('toast-alert-success', 'Feature successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create feature.');
        }

        return redirect()->route('pfeatures.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feature = PackageFeature::findOrFail($id);

        return view('admin.settings.packages.features.edit', compact('feature'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'description' => 'required|min:1'
        ]);

        $feature = PackageFeature::findOrFail($id);

        $feature->fill($request->all());

        if($feature->isDirty()){
            $feature->save();
            $request->session()->flash('toast-alert-success', 'Feature successfully updated.');
        }
        else{
            $request->session()->flash('toast-alert-info', 'Nothing to update.');
        }

        return redirect()->route('pfeatures.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $feature = PackageFeature::findOrFail($id);

        if( $feature->delete() ) {
            $request->session()->flash('toast-alert-success', 'Feature successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Feature not deleted.');
        }

        return redirect()->back();
    }
}
