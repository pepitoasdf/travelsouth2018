<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Authorizable;
use App\HotelType;
Use App\Hotel;
class HotelTypeController extends Controller
{
    use Authorizable;
    
    public function __construct(){
        $this->page_num = 10;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = HotelType::latest()->paginate($this->page_num);
        return view('admin.settings.hotels.types.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.hotels.types.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
        ]);

        if ( $type = HotelType::create($request->all()) ) {

            $request->session()->flash('toast-alert-success', 'Type successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create type.');
        }

        return redirect()->route('hoteltypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = HotelType::findOrFail($id);

        return view('admin.settings.hotels.types.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:1'
        ]);

        $type = HotelType::findOrFail($id);

        $type->fill($request->all());

        if($type->isDirty()){
            $type->save();
            $request->session()->flash('toast-alert-success', 'Type successfully updated.');
        }
        else{
            $request->session()->flash('toast-alert-info', 'Nothing to update.');
        }

        return redirect()->route('hoteltypes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $type = HotelType::findOrFail($id);
        if($type->hotels->count()){
            $request->session()->flash('toast-alert-danger', 'Type already in used.');
            return redirect()->back();
        }
        if( $type->delete() ) {
            $request->session()->flash('toast-alert-success', 'Type successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Type not deleted.');
        }

        return redirect()->back();
    }
}
