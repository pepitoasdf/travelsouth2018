<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PackageItinerary;
use App\Package;
use App\Authorizable;
use App\Photo;
class PackageItineraryController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $package = Package::findOrFail($id);
        $results = PackageItinerary::where('package_id',$id)->orderBy('id')->paginate($this->page_num);
        return view('admin.packages.itineraries.index', compact('results','package'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $package = Package::findOrFail($id);
        return view('admin.packages.itineraries.new',compact('package'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $package = Package::findOrFail($id);

        $this->validate($request, [
            'day' => 'required|min:1',
            'title' => 'required|min:1',
            'description' => 'required|min:1'
        ]);

        $fill = $request->all();
        $fill['package_id'] = $package->id;

        if ( $itinerary = PackageItinerary::create($fill) ) {

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'package-itinerary',$itinerary->id);
            }

            $request->session()->flash('toast-alert-success', 'Itinerary successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create itinerary.');
        }

        return redirect()->route('pitineraries.index',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $itinerary)
    {
        $package = Package::findOrFail($id);

        $itinerary = PackageItinerary::findOrFail($itinerary);

        $photos = Photo::where('ref_id',$itinerary->id)
                ->where('model_type','package-itinerary')
                ->get();
        return view('admin.packages.itineraries.edit', compact('itinerary','package','photos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $itinerary)
    {
        $package = Package::findOrFail($id);
        $itinerary = PackageItinerary::findOrFail($itinerary);
        
        $this->validate($request, [
            'day' => 'required|min:1',
            'title' => 'required|min:1',
            'description' => 'required|min:1'
        ]);

        if($itinerary){
            $fill = $request->except('package_id');
            $fill['package_id'] = $id;
            $itinerary->fill($fill);
            $itinerary->save();

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'package-itinerary',$itinerary->id);
            }

            if(count($request->input('removephotoid')) && count($request->input('removephotoname'))){
                Photo::removeFile($request->input('removephotoid'));
            }
            
            $request->session()->flash('toast-alert-success', 'Itinerary  successfully updated.');
        }

        return redirect()->route('pitineraries.index',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $itinerary)
    {
        $getitinerary = PackageItinerary::findOrFail($itinerary);

        if( $getitinerary->delete() ) {
            Photo::removeByReference($itinerary,'package-itinerary');
            $request->session()->flash('toast-alert-success', 'Itinerary successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Itinerary not deleted.');
        }

        return redirect()->back();
    }
}
