<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;
use App\Authorizable;
use App\PackageRate;
use App\Photo;
use App\PackageHotelDetail;
use App\PackageVehicleDetail;
use App\PackageItinerary;
class PackageController extends Controller
{
    use Authorizable;
    
    public function __construct(){
        $this->page_num = 10;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Package::orderBy('id','desc')->paginate($this->page_num);
        return view('admin.packages.index',compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.packages.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'duration' => 'required|min:1|max:10|numeric',
            'rate' => 'required|numeric',
            'effective_date' => 'required'
        ]);
        $success = Package::insertData($request);
        if ($success) {

            $this->saveDetails($request, $success->id);

            PackageRate::create([
                'package_id' => $success->id,
                'rate' => $request->input('rate'),
                'effective_date' => date('Y-m-d',strtotime($request->input('effective_date')))
            ]);
            $request->session()->flash('toast-alert-success', 'Package successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create package.');
        }

        return redirect()->route('packages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Package::findOrFail($id);
        $photos = Photo::where('ref_id',$package->id)
                ->where('model_type','packages')
                ->get();
        return view('admin.packages.edit', compact('package','photos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'duration' => 'required|min:1',
            'rate' => 'required|numeric',
            'effective_date' => 'required'
        ]);

        $package = Package::findOrFail($id);

        $package->fill($request->all());
        $package->save();
        if($package){
            
            $this->saveDetails($request,$id);
            
            $rate = PackageRate::where('id',$request->input('rate_id'))
                    ->where('package_id',$package->id)
                    ->where('effective_date',$request->input('effective_date'))
                    ->first();
            if($rate){
                $rate->rate = $request->input('rate');
                $rate->effective_date = date('Y-m-d',strtotime($request->input('effective_date')));
                $rate->save();
            }
            else{
                PackageRate::create([
                    'package_id' => $package->id,
                    'rate' => $request->input('rate'),
                    'effective_date' => date('Y-m-d',strtotime($request->input('effective_date')))
                ]);
            }

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'packages',$package->id);
            }

            if(count($request->input('removephotoid')) && count($request->input('removephotoname'))){
                Photo::removeFile($request->input('removephotoid'));
            }

            $request->session()->flash('toast-alert-success', 'Package successfully updated.');
        }
        else{
             $request->session()->flash('toast-alert-danger', 'Something went wrong. Try again.');
        }
        return redirect()->route('packages.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $package = Package::findOrFail($id);
        if( $package->delete() ) {
            PackageDetail::where('package_id',$id)->delete();
            PackageFeatureDetail::where('package_id',$id)->delete();
            PackageRate::where('package_id',$id)->delete();
            Photo::removeByReference($id,'packages');
            $request->session()->flash('toast-alert-success', 'Package successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Package not deleted.');
        }

        return redirect()->back();
    }

     private function saveDetails($request, $id){
        PackageHotelDetail::where('package_id', $id)->delete();
        PackageVehicleDetail::where('package_id', $id)->delete();

        for ($i=0; $i < count($request->input('hotel_id')); $i++) { 
            PackageHotelDetail::create([
                'package_id' => $id,
                'hotel_id' => $request->input('hotel_id')[$i],
                'room_id' => $request->input('room_id')[$i],
                'room_rate' => $request->input('room_rate')[$i]
            ]);
        }

        for ($i=0; $i < count($request->input('vehicle_id')); $i++) { 
            PackageVehicleDetail::create([
                'package_id' => $id,
                'vehicle_id' => $request->input('vehicle_id')[$i],
                'vehicle_rate' => $request->input('vehicle_rate')[$i]
            ]);
        }
    }
}
