<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Authorizable;
use App\Country;
use App\Province;
use App\City;
class CityController extends Controller
{
    use Authorizable;
    
    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = City::latest()->paginate($this->page_num);
        return view('admin.settings.places.cities.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.places.cities.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'country' => 'required|min:1',
            'province' => 'required|min:1'
        ]);

        if ( $city = City::create($request->all()) ) {

            $request->session()->flash('toast-alert-success', 'City successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create city.');
        }

        return redirect()->route('cities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::findOrFail($id);

        return view('admin.settings.places.cities.edit', compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'country' => 'required|min:1',
            'province' => 'required|min:1'
        ]);

        $city = City::findOrFail($id);

        $city->fill($request->all());

        if($city->isDirty()){
            $city->save();
            $request->session()->flash('toast-alert-success', 'City successfully updated.');
        }
        else{
            $request->session()->flash('toast-alert-info', 'Nothing to update.');
        }

        return redirect()->route('cities.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $city = City::findOrFail($id);
        if($city->hotels->count()){
            $request->session()->flash('toast-alert-danger', 'City already in used.');
            return redirect()->back();
        }
        if( $city->delete() ) {
            $request->session()->flash('toast-alert-success', 'City successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'City not deleted.');
        }

        return redirect()->back();
    }
}
