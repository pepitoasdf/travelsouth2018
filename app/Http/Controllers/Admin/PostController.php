<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Photo;
use App\Post;
use App\Authorizable;
class PostController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Post::orderBy('updated_at','desc')->paginate($this->page_num);
        return view('admin.pages.blog.posts.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.blog.posts.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:1',
            'body' => 'required|min:15',
            'author_id' => 'required|min:1',
            'status' => 'required|min:1'
        ]);

        $fill = $request->all();
        $fill['slug'] = Post::setSlug($request->input('title'));

        if ( $post = Post::create($fill)) {

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'posts',$post->id);
            }

            $request->session()->flash('toast-alert-success', 'Post successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create post.');
        }

        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $photos = Photo::where('ref_id',$post->id)
                ->where('model_type','posts')
                ->get();
        return view('admin.pages.blog.posts.edit', compact('post','photos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|min:1',
            'body' => 'required|min:15',
            'author_id' => 'required|min:1',
            'status' => 'required|min:1'
        ]);

        $post = Post::findOrFail($id);

        $fill = $request->all();
        $fill['slug'] = Post::setSlug($request->input('title'));

        $post->fill($fill);

        $post->save();
        
        if($request->hasFile('images')){
            Photo::upload($request->file('images'),'posts',$post->id);
        }

        if(count($request->input('removephotoid')) && count($request->input('removephotoname'))){
            Photo::removeFile($request->input('removephotoid'));
        }
        $request->session()->flash('toast-alert-success', 'Post successfully updated.');

        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $post = Post::findOrFail($id);

        if( $post->delete() ) {
            Photo::removeByReference($id,'posts');
            $request->session()->flash('toast-alert-success', 'Post successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Post not deleted.');
        }

        return redirect()->back();
    }
}
