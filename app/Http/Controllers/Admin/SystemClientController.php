<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use App\Authorizable;
use File;
class SystemClientController extends Controller
{
    use Authorizable;
    
    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = Client::orderBy('id','desc')->first();
        return view('admin.settings.system.client.index', compact('client'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:1',
            'description' => 'required|min:1'
        ]);
        $client = Client::first();
        
        $destinationPath = 'admin/images/uploads/system';
        
        if($request->hasFile('logo')){
            $filename_logo = uniqid().'_'.time() . '.' . $request->logo->getClientOriginalExtension();
            $request->logo->move($destinationPath, $filename_logo);

            $fill['logo'] = $destinationPath.'/'.$filename_logo;
            if($client){
                File::delete($client->logo);
            }
        }

        if($request->hasFile('background')){
            $filename_background = uniqid().'_'.time() . '.' . $request->background->getClientOriginalExtension();
            $request->background->move($destinationPath, $filename_background);

            $fill['background'] = $destinationPath.'/'.$filename_background;
            if($client){
                File::delete($client->background);
            }
        }

        $fill['title'] = $request->input('title');
        $fill['description'] = $request->input('description');

        if($client){
            $client->fill($fill);
            $client->save();
            if($client){
                
                $request->session()->flash('toast-alert-success', 'Client Setting successfully updated.');
            }
        }
        else{
            $client = Client::create($fill);
            if($client){
                $request->session()->flash('toast-alert-success', 'Client Setting successfully created.');
            }
            else{
                $request->session()->flash('toast-alert-danger', 'Unable to create Client Setting.');
            }
        }

        return redirect()->route('client.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         abort(404);
    }
}
