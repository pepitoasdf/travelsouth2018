<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vehicle;
use App\Authorizable;
use App\Photo;
use File;
use Image;
use App\VehicleRate;
use App\VehicleDriverRate;
class VehicleRateController extends Controller
{
    //use Authorizable;
    
    public function __construct(){
        $this->page_num = 10;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $vehicle = Vehicle::findorFail($id);
        $rate = VehicleRate::where('vehicle_id',$id)->orderBy('effective_date')->get();
        return view('admin.vehicles.rates.index',compact('vehicle','rate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $vehicle = Vehicle::findOrFail($id);
        if($vehicle){
            for ($x=0; $x < count($request->input('remove_id')); $x++) { 
                $check = VehicleRate::find($request->input('remove_id')[$x]);
                if($check){
                    $check->delete();
                }
            }
            for ($i=0; $i < count($request->input('rate_id')); $i++) { 
                $rate = VehicleRate::where('id',$request->input('rate_id')[$i])
                        ->where('vehicle_id',$vehicle->id)
                        ->first();
                if($rate){
                    $rate->rate_day = $request->input('rate_day')[$i];
                    $rate->rate_hour = $request->input('rate_hour')[$i];
                    $rate->effective_date = date('Y-m-d',strtotime($request->input('effective_date')[$i]));
                    $rate->save();
                }
                else{
                    VehicleRate::create([
                        'vehicle_id' => $vehicle->id,
                        'rate_day' => $request->input('rate_day')[$i],
                        'rate_hour' => $request->input('rate_hour')[$i],
                        'effective_date' => date('Y-m-d',strtotime($request->input('effective_date')[$i]))
                    ]);
                }
            }

            for ($x=0; $x < count($request->input('remove_id2')); $x++) { 
                $check = VehicleDriverRate::find($request->input('remove_id2')[$x]);
                if($check){
                    $check->delete();
                }
            }
            for ($i=0; $i < count($request->input('rate_id2')); $i++) { 
                $rate = VehicleDriverRate::where('id',$request->input('rate_id2')[$i])
                        ->where('vehicle_id',$vehicle->id)
                        ->first();
                if($rate){
                    $rate->rate_day = $request->input('rate_day2')[$i];
                    $rate->rate_hour = $request->input('rate_hour2')[$i];
                    $rate->effective_date = date('Y-m-d',strtotime($request->input('effective_date2')[$i]));
                    $rate->save();
                }
                else{
                    VehicleDriverRate::create([
                        'vehicle_id' => $vehicle->id,
                        'rate_day' => $request->input('rate_day2')[$i],
                        'rate_hour' => $request->input('rate_hour2')[$i],
                        'effective_date' => date('Y-m-d',strtotime($request->input('effective_date2')[$i]))
                    ]);
                }
            }
            $request->session()->flash('toast-alert-success', 'Vehicle  rates  successfully updated.');
        }

        return redirect()->route('vrates.index',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $rate)
    {
        $vehicle = Vehicle::where('id',$id)->firstOrFail();
        $rate = VehicleRate::where('id',$rate)->firstOrFail();

        return view('admin.vehicles.rates.edit', compact('vehicle','rate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $rate)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }
}
