<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PackageGuide;
use App\Package;
use App\Authorizable;
use App\Photo;
class PackageGuideController extends Controller
{
    use Authorizable;
    
    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $package = Package::findOrFail($id);
        $results = PackageGuide::where('package_id',$id)->orderBy('id')->paginate($this->page_num);
        return view('admin.packages.guides.index', compact('results','package'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $package = Package::findOrFail($id);
        return view('admin.packages.guides.new',compact('package'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $package = Package::findOrFail($id);

        $this->validate($request, [
            'name' => 'required|min:1',
            'description' => 'required|min:1'
        ]);

        $fill = $request->all();
        $fill['package_id'] = $package->id;

        if ( $guide = PackageGuide::create($fill) ) {

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'package-guides',$guide->id);
            }

            $request->session()->flash('toast-alert-success', 'Tour Guide successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create guide.');
        }

        return redirect()->route('pguides.index',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $guide)
    {
        $package = Package::findOrFail($id);

        $guide = PackageGuide::findOrFail($guide);

        $photos = Photo::where('ref_id',$guide->id)
                ->where('model_type','package-guides')
                ->get();
        return view('admin.packages.guides.edit', compact('guide','package','photos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $guide)
    {
        $package = Package::findOrFail($id);
        $guide = PackageGuide::findOrFail($guide);
        
        $this->validate($request, [
            'name' => 'required|min:1',
            'description' => 'required|min:1'
        ]);

        if($guide){
            $fill = $request->except('package_id');
            $fill['package_id'] = $id;
            $guide->fill($fill);
            $guide->save();

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'package-guides',$guide->id);
            }

            if(count($request->input('removephotoid')) && count($request->input('removephotoname'))){
                Photo::removeFile($request->input('removephotoid'));
            }
            
            $request->session()->flash('toast-alert-success', 'Tour Guide  successfully updated.');
        }

        return redirect()->route('pguides.index',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $guide)
    {
        $getguide = PackageGuide::findOrFail($guide);

        if( $getguide->delete() ) {
            Photo::removeByReference($guide,'package-guides');
            $request->session()->flash('toast-alert-success', 'Tour Guide successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Tour Guide not deleted.');
        }

        return redirect()->back();
    }
}
