<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;
use App\PackageRate;
class PackageRateController extends Controller
{
    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $package = Package::findorFail($id);
        $rate = PackageRate::where('package_id',$id)->orderBy('effective_date')->get();
        return view('admin.packages.rates.index',compact('package','rate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $package = Package::findOrFail($id);
        if($package){
            for ($x=0; $x < count($request->input('remove_id')); $x++) { 
                $check = PackageRate::find($request->input('remove_id')[$x]);
                if($check){
                    $check->delete();
                }
            }
            for ($i=0; $i < count($request->input('rate_id')); $i++) { 
                $rate = PackageRate::where('id',$request->input('rate_id')[$i])
                        ->where('package_id',$package->id)
                        ->first();
                if($rate){
                    $rate->rate = $request->input('rate')[$i];
                    $rate->effective_date = date('Y-m-d',strtotime($request->input('effective_date')[$i]));
                    $rate->save();
                }
                else{
                    PackageRate::create([
                        'package_id' => $package->id,
                        'rate' => $request->input('rate')[$i],
                        'effective_date' => date('Y-m-d',strtotime($request->input('effective_date')[$i]))
                    ]);
                }
            }
            $request->session()->flash('toast-alert-success', 'Package  rates  successfully updated.');
        }

        return redirect()->route('prates.index',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $rate)
    {
        $package = Package::where('id',$id)->firstOrFail();
        $rate = PackageRate::where('id',$rate)->firstOrFail();

        return view('admin.packages.rates.edit', compact('package','rate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }
}
