<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\VehicleVideo;
use App\Vehicle;
use App\Authorizable;
use App\Photo;
class VehicleVideoController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $vehicle = Vehicle::findOrFail($id);
        $results = VehicleVideo::where('vehicle_id',$id)->orderBy('id')->paginate($this->page_num);
        return view('admin.vehicles.videos.index', compact('results','vehicle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $vehicle = Vehicle::findOrFail($id);
        return view('admin.vehicles.videos.new',compact('vehicle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $vehicle = Vehicle::findOrFail($id);

        $this->validate($request, [
            'title' => 'required|min:1',
            'description' => 'required|min:1',
            'video_url' => 'required|url|min:1'
        ]);

        $fill = $request->all();
        $fill['vehicle_id'] = $vehicle->id;

        if ( $video = VehicleVideo::create($fill) ) {

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'vehicle-videos',$video->id);
            }

            $request->session()->flash('toast-alert-success', 'Video successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create video.');
        }

        return redirect()->route('vvideos.index',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $video)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $video)
    {
        $vehicle = Vehicle::findOrFail($id);

        $video = VehicleVideo::findOrFail($video);

        $photos = Photo::where('ref_id',$video->id)
                ->where('model_type','vehicle-videos')
                ->get();
        return view('admin.vehicles.videos.edit', compact('video','vehicle','photos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $video)
    {
        $vehicle = Vehicle::findOrFail($id);
        $video = VehicleVideo::findOrFail($video);

        $this->validate($request, [
            'title' => 'required|min:1',
            'description' => 'required|min:1',
            'video_url' => 'required|url|min:1'
        ]);

        if($video){
            $fill = $request->except('vehicle_id');
            $fill['vehicle_id'] = $id;
            $video->fill($fill);
            $video->save();

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'vehicle-videos',$video->id);
            }

            if(count($request->input('removephotoid')) && count($request->input('removephotoname'))){
                Photo::removeFile($request->input('removephotoid'));
            }
            
            $request->session()->flash('toast-alert-success', 'Video  successfully updated.');
        }

        return redirect()->route('vvideos.index',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $video)
    {
        $getvideo = VehicleVideo::findOrFail($video);

        if( $getvideo->delete() ) {
            Photo::removeByReference($video,'vehicle-videos');
            $request->session()->flash('toast-alert-success', 'Video successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Video not deleted.');
        }

        return redirect()->back();
    }
}
