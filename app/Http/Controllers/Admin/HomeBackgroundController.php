<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HomeBackground;
use App\Authorizable;
use App\Photo;
class HomeBackgroundController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = HomeBackground::orderBy('status','asc')->orderBy('updated_at','desc')->paginate($this->page_num);
        return view('admin.pages.home.background.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.home.background.new');
    }

    /**
     * Store a newly created resource in storage/database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:1',
            'subtitle' => 'required|min:1',
            'description' => 'required|min:1',
            'status' => 'required|min:1'
        ]);

        $fill = $request->all();

        if ( $background = HomeBackground::create($fill)) {

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'background',$background->id);
            }

            $request->session()->flash('toast-alert-success', 'Background successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create background.');
        }

        return redirect()->route('background.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $background = HomeBackground::findOrFail($id);
        $photos = Photo::where('ref_id',$background->id)
                ->where('model_type','background')
                ->get();
        return view('admin.pages.home.background.edit', compact('background','photos'));
    }

    /**
     * Update the specified resource in storage/database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|min:1',
            'subtitle' => 'required|min:1',
            'description' => 'required|min:1',
            'status' => 'required|min:1'
        ]);

        $background = HomeBackground::findOrFail($id);

        $fill = $request->all();

        $background->fill($fill);

        $background->save();
        
        if($request->hasFile('images')){
            Photo::upload($request->file('images'),'background',$background->id);
        }

        if(count($request->input('removephotoid')) && count($request->input('removephotoname'))){
            Photo::removeFile($request->input('removephotoid'));
        }
        $request->session()->flash('toast-alert-success', 'Background successfully updated.');

        return redirect()->route('background.index');
    }

    /**
     * Remove the specified resource from storage/database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $background = HomeBackground::findOrFail($id);

        if( $background->delete() ) {
            Photo::removeByReference($id,'background');
            $request->session()->flash('toast-alert-success', 'Background successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Background not deleted.');
        }

        return redirect()->back();
    }
}
