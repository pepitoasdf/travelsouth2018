<?php

namespace App\Http\Controllers\Admin\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use App\Hotel;
use App\HotelRoom;
use App\Vehicle;
use App\User;
use DB;
use App\Role;
use App\TabMenu;
use App\Package;
use App\Country;
use App\Province;
use App\City;
use App\HomeTopdestinationDetail;
use App\Photo;
use App\HomeIncreadibleplaceDetail;
class AppAjaxController extends Controller
{
    public function countries(Request $request){
        $name = $request->input('name');
        $countries = Country::where('name','like','%'.$name.'%')->select('id','name','name as text')->get();

        return Response::json($countries, 200, array(), JSON_PRETTY_PRINT);
    }

    public function provinces(Request $request){
        $name = $request->input('name');
        $query = Province::where('name','like','%'.$name.'%');
        if($request->input('country')){
            $query->where('country',$request->input('country'));
        }
        $provinces = $query->select('id','name','name as text')->get();

        return Response::json($provinces, 200, array(), JSON_PRETTY_PRINT);
    }

    public function cities(Request $request){
        $name = $request->input('name');

        $query = City::where('name','like','%'.$name.'%');
        if($request->input('country')){
            $query->where('country',$request->input('country'));
        }
        if($request->input('province')){
            $query->where('province', $request->input('province'));
        }

       $cities = $query->select('id','name','name as text')->get();

        return Response::json($cities, 200, array(), JSON_PRETTY_PRINT);
    }

    public function hotels(Request $request){
    	$name = $request->input('name');
    	$hotels = Hotel::where('name','like','%'.$name.'%')
                ->select('id','name','name as text')
                ->get();

    	return Response::json($hotels, 200, array(), JSON_PRETTY_PRINT);
    }

    public function rooms(Request $request){
    	$name = $request->input('name');
    	$rooms = HotelRoom::where('name','like','%'.$name.'%')->select('id','name','name as text')->get();

    	return Response::json($rooms, 200, array(), JSON_PRETTY_PRINT);
    }

    public function hotelRooms(Request $request){
        $hotelId = $request->input('id');
        $rooms = HotelRoom::where('hotel_id',$hotelId)
                ->with('getrate','type')
                ->get();

        return Response::json($rooms, 200, array(), JSON_PRETTY_PRINT);
    }

    public function vehicles(Request $request){
    	$name = $request->input('name');
    	$vehicle = Vehicle::where('name','like','%'.$name.'%')->select('id','name','name as text')->get();

    	return Response::json($vehicle, 200, array(), JSON_PRETTY_PRINT);
    }

    public function users(Request $request){
        $name = $request->input('name');
        $user = User::where(DB::raw("CONCAT(firstname,' ',lastname,' ','lastname', ' ', firstname)"),'like','%'.$name.'%')
                ->select(DB::raw("CONCAT(firstname,' ',lastname) as name, CONCAT(firstname,' ',lastname) as text, id"))
                ->get();

        return Response::json($user, 200, array(), JSON_PRETTY_PRINT);
    }

    public function getrole(Request $request){
        $id = $request->input('id');

        $role = Role::find($id);
        $menus = TabMenu::orderBy('name','asc')->get();

        return view('admin.roles.partials.role-permission', compact('role', 'menus'));
    }
    public function packages(Request $request){
        $name = $request->input('name');
        $packages = Package::where('name','like','%'.$name.'%')->select('id','name','name as text')->get();

        return Response::json($packages, 200, array(), JSON_PRETTY_PRINT);
    }
    public function hotelLocations(Request $request){
        $name = $request->input('name');

        $locations = DB::select("
                        SELECT * FROM (
                            SELECT 
                                CONCAT(c.name, ', ', d.name, ', ', b.name) as `text`,
                                CONCAT(b.id, '|', d.id, '|', c.id) as id
                            FROM hotels as a 
                            LEFT JOIN countries as b on a.country_id = b.id
                            LEFT JOIN cities as c on a.city_id = c.id
                            LEFT JOIN provinces as d on a.province_id = d.id
                            WHERE NOT EXISTS(
                                select * from home_topdestination_details  as e
                                where a.country_id = e.country_id and a.city_id = e.city_id
                                        and a.province_id = e.province_id
                            )
                        ) as t
                        WHERE t.text LIKE ?
                        GROUP BY t.text
                    ",['%'.$name.'%']);

         return Response::json($locations, 200, array(), JSON_PRETTY_PRINT);              
    }

    public function topdestinationUpdate(Request $request){
        $id = $request->input('id');
        $detail = HomeTopdestinationDetail::find($id);
        $photo = Photo::where('model_type','topdestinations')
                ->where('ref_id',$detail->id)
                ->first();
        return view('admin.pages.home.topdestinations._form-modal',compact('detail','photo'));
    }

    public function increadibleplaceUpdate(Request $request){
        $id = $request->input('id');
        $detail = HomeIncreadibleplaceDetail::find($id);
        $photo = Photo::where('model_type','increadibleplaces')
                ->where('ref_id',$detail->id)
                ->first();
        return view('admin.pages.home.increadibleplaces._form-modal',compact('detail','photo'));
    }
}
