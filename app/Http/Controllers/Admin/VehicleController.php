<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vehicle;
use App\Authorizable;
use App\Photo;
use File;
use Image;
use App\VehicleRate;
class VehicleController extends Controller
{
    use Authorizable;
    
    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Vehicle::orderBy('name')->paginate($this->page_num);
        return view('admin.vehicles.index',compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.vehicles.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'description' => 'required|min:10',
            'color' => 'required|min:1',
            'year_model' => 'required|min:1',
            'mv_file_no' => 'required|min:1',
            'no_of_wheel' => 'numeric',
            'seats' => 'numeric',
            'doors' => 'numeric'
        ]);

        $success = Vehicle::insertData($request);
        if($success){
            VehicleRate::create([
                'vehicle_id' => $success->id,
                'rate_day' => $request->input('rate_day'),
                'rate_hour' => $request->input('rate_hour'),
                'effective_date' => date('Y-m-d',strtotime($request->input('effective_date')))
            ]);
            $request->session()->flash('toast-alert-success', 'Vehicle successfully created.');
        }
        else{
            $request->session()->flash('toast-alert-danger', 'Unable to create vehicle.');
        }
        return redirect()->route('vehicles.edit',$success->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicle = Vehicle::where('id',$id)->firstOrFail();
        $photos = Photo::where('ref_id',$vehicle->id)
                ->where('model_type','vehicles')
                ->get();
                
        return view('admin.vehicles.edit', compact('vehicle','photos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'description' => 'required|min:10',
            'color' => 'required|min:1',
            'year_model' => 'required|min:1',
            'mv_file_no' => 'required|min:1',
            'no_of_wheel' => 'numeric',
            'seats' => 'numeric',
            'doors' => 'numeric'
        ]);

        $vehicle = Vehicle::findOrFail($id);

        $vehicle->fill($request->all());
        $vehicle->save();
        if($vehicle){
            $rate = VehicleRate::where('id',$request->input('rate_id'))
                    ->where('vehicle_id',$vehicle->id)
                    ->where('effective_date',$request->input('effective_date'))
                    ->first();
            if($rate){
                $rate->rate_day = $request->input('rate_day');
                $rate->rate_hour = $request->input('rate_hour');
                $rate->effective_date = date('Y-m-d',strtotime($request->input('effective_date')));
                $rate->save();
            }
            else{
                VehicleRate::create([
                    'vehicle_id' => $vehicle->id,
                    'rate_day' => $request->input('rate_day'),
                    'rate_hour' => $request->input('rate_hour'),
                    'effective_date' => date('Y-m-d',strtotime($request->input('effective_date')))
                ]);
            }

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'vehicles',$vehicle->id);
            }

            if(count($request->input('removephotoid')) && count($request->input('removephotoname'))){
                Photo::removeFile($request->input('removephotoid'));
            }

            $request->session()->flash('toast-alert-success', 'Vehicle successfully updated.');
        }
        else{
             $request->session()->flash('toast-alert-danger', 'Something went wrong. Try again.');
        }
        return redirect()->route('vehicles.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $vehicle = Vehicle::findOrFail($id);

        if( $vehicle->delete() ) {
            Photo::removeByReference($id,'vehicles');
            $request->session()->flash('toast-alert-success', 'Vehicle successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Vehicle not deleted.');
        }

        return redirect()->back();
    }
}
