<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Authorizable;
use App\Country;
use App\Province;
class ProvinceController extends Controller
{
    use Authorizable;
    
    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Province::latest()->paginate($this->page_num);
        return view('admin.settings.places.provinces.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.places.provinces.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'country' => 'required|min:1',
            'name' => 'required|min:1'
        ]);

        if ( $city = Province::create($request->all()) ) {

            $request->session()->flash('toast-alert-success', 'Province successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create province.');
        }

        return redirect()->route('provinces.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $province = Province::findOrFail($id);

        return view('admin.settings.places.provinces.edit', compact('province'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:1',
            'country' => 'required|min:1'
        ]);

        $province = Province::findOrFail($id);

        $province->fill($request->all());

        if($province->isDirty()){
            $province->save();
            $request->session()->flash('toast-alert-success', 'Province successfully updated.');
        }
        else{
            $request->session()->flash('toast-alert-info', 'Nothing to update.');
        }

        return redirect()->route('provinces.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $province = Province::findOrFail($id);
        if($province->hotels->count()){
            $request->session()->flash('toast-alert-danger', 'Province already in used.');
            return redirect()->back();
        }
        if( $province->delete() ) {
            $request->session()->flash('toast-alert-success', 'Province successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Province not deleted.');
        }

        return redirect()->back();
    }
}
