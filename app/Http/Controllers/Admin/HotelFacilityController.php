<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Authorizable;
use App\Hotel;
use App\HotelFacility;
class HotelFacilityController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = HotelFacility::latest()->paginate($this->page_num);
        return view('admin.settings.hotels.facilities.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.hotels.facilities.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:1',
        ]);

        if ( $facility = HotelFacility::create($request->all()) ) {

            $request->session()->flash('toast-alert-success', 'Facility successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create facility.');
        }

        return redirect()->route('facilities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $facility = HotelFacility::findOrFail($id);

        return view('admin.settings.hotels.facilities.edit', compact('facility'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'description' => 'required|min:1'
        ]);

        $facility = HotelFacility::findOrFail($id);

        $facility->fill($request->all());

        if($facility->isDirty()){
            $facility->save();
            $request->session()->flash('toast-alert-success', 'Facility successfully updated.');
        }
        else{
            $request->session()->flash('toast-alert-info', 'Nothing to update.');
        }

        return redirect()->route('facilities.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $facility = HotelFacility::findOrFail($id);

        if( $facility->delete() ) {
            $request->session()->flash('toast-alert-success', 'Facility successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Facility not deleted.');
        }

        return redirect()->back();
    }
}
