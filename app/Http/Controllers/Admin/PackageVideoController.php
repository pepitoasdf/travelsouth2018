<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PackageVideo;
use App\Package;
use App\Authorizable;
use App\Photo;
class PackageVideoController extends Controller
{
    use Authorizable;

    public function __construct(){
        $this->page_num = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $package = Package::findOrFail($id);
        $results = PackageVideo::where('package_id',$id)->orderBy('id')->paginate($this->page_num);
        return view('admin.packages.videos.index', compact('results','package'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $package = Package::findOrFail($id);
        return view('admin.packages.videos.new',compact('package'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $package = Package::findOrFail($id);

        $this->validate($request, [
            'title' => 'required|min:1',
            'description' => 'required|min:1',
            'video_url' => 'required|url|min:1'
        ]);

        $fill = $request->all();
        $fill['package_id'] = $package->id;

        if ( $video = PackageVideo::create($fill) ) {

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'package-videos',$video->id);
            }

            $request->session()->flash('toast-alert-success', 'Video successfully created.');

        } else {
            $request->session()->flash('toast-alert-danger', 'Unable to create video.');
        }

        return redirect()->route('pvideos.index',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $video)
    {
        $package = Package::findOrFail($id);

        $video = PackageVideo::findOrFail($video);

        $photos = Photo::where('ref_id',$video->id)
                ->where('model_type','package-videos')
                ->get();
        return view('admin.packages.videos.edit', compact('video','package','photos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $video)
    {
        $package = Package::findOrFail($id);
        $video = PackageVideo::findOrFail($video);

        $this->validate($request, [
            'title' => 'required|min:1',
            'description' => 'required|min:1',
            'video_url' => 'required|url|min:1'
        ]);

        if($video){
            $fill = $request->except('package_id');
            $fill['package_id'] = $id;
            $video->fill($fill);
            $video->save();

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'package-videos',$video->id);
            }

            if(count($request->input('removephotoid')) && count($request->input('removephotoname'))){
                Photo::removeFile($request->input('removephotoid'));
            }
            
            $request->session()->flash('toast-alert-success', 'Video  successfully updated.');
        }

        return redirect()->route('pvideos.index',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $video)
    {
        $getvideo = PackageVideo::findOrFail($video);

        if( $getvideo->delete() ) {
            Photo::removeByReference($video,'package-videos');
            $request->session()->flash('toast-alert-success', 'Video successfully deleted.');
        } else {
            $request->session()->flash('toast-alert-warning', 'Video not deleted.');
        }

        return redirect()->back();
    }
}
