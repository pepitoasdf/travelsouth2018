<?php

namespace App\Http\Controllers\Admin;

use App\Authorizable;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use App\TabMenu;
use Auth;
use App\Http\Controllers\Controller;
class RoleController extends Controller
{
    use Authorizable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        $menus = TabMenu::orderBy('name','asc')->get();
        
        return view('admin.roles.index', compact('roles', 'menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|unique:roles']);

        if( Role::create($request->only('name')) ) {
            $request->session()->flash('toast-alert-success', 'Role Added.');
            return redirect()->route('roles.index');
        }

        return redirect()->back();
    }

    public function edit($id){
        if($id == 1){
            abort(404);
        }
        $role = Role::findOrFail($id);
        $menus = TabMenu::orderBy('name','asc')->get();

        return view('admin.roles.edit', compact('role', 'menus'));
    }

    public function show($id){
        return $this->edit($id);
    }

    public function create(){
        return view('admin.roles.new');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($id == 1){
            abort(404);
        }
        if($role = Role::findOrFail($id)) {
            // admin role has everything
            if($role->name === 'Admin') {
                $role->syncPermissions(Permission::all());
                return redirect()->route('roles.index');
            }

            $permissions = $request->get('permissions', []);

            $role->syncPermissions($permissions);

            $request->session()->flash('toast-alert-success', $role->name . ' permissions successfully updated.');
        } else {
            $request->session()->flash('toast-alert-danger', 'Role with id '. $id .' note found.');
        }
        return redirect()->route('roles.index');
    }
}
