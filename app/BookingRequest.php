<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingRequest extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'name', 'email', 'address', 'phone','currency_id', 'remarks', 'date', 'type', 'service_type',
        'service_id','duration', 'duration_type', 'start_date', 'end_date','amount'
    ];

}
