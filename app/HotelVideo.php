<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelVideo extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'hotel_id', 'video_url', 'title', 'description','status'
    ];

    public function getphoto(){
        return $this->hasOne(Photo::class,'ref_id')->where('model_type','hotel-videos')->orderBy('created_at','desc');
    }
}
