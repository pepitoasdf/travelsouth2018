<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PackageFeatureDetail;
class Package extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'pricing', 'name', 'description', 'duration', 'status', 'max_guest'
    ];

    public function hoteldetails(){
        return $this->hasMany(PackageHotelDetail::class, 'package_id');
    }

    public function vehicledetails(){
        return $this->hasMany(PackageVehicleDetail::class, 'package_id');
    }

    public static function insertData($request){
        $package = static::create($request->all());
        
        if($package){
            for ($i=0; $i < count($request->input('feature_id')); $i++) { 
                static::insertFeatures($package->id, $request->input('feature_id')[$i]);
            }

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'packages',$package->id);
            }
        }

        return $package;
    }

    public static function insertFeatures($package_id, $input){
        PackageFeatureDetail::create([
            'package_id' => $package_id,
            'feature_id' => $input
        ]);
    }

    public static function getName($id = null){
        if($id){
            return static::where('id',$id)->first()->name;
        }
        return '';
    }

    public function getrate(){

        $getrate =  PackageRate::where('effective_date', '<=', date('Y-m-d'))->orderBy('effective_date','desc')->first();
        if(count($getrate) > 0){
            return $this->hasOne(PackageRate::class,'package_id')->where('effective_date', '<=', date('Y-m-d'))->orderBy('effective_date','desc');
        }
        else{
            return $this->hasOne(PackageRate::class,'package_id')->orderBy('effective_date','desc');
        }
        
    }

    public function rates(){
        return $this->hasMany(PackageRate::class,'package_id');
    }

    public function features(){
        return $this->hasMany(PackageFeatureDetail::class,'package_id');
    }
    public function featuredetails(){
        return $this->belongsToMany(PackageFeature::class,'package_feature_details', 'package_id', 'feature_id');
    }
    public function getphoto(){
        return $this->hasOne(Photo::class,'ref_id')->where('model_type','packages')->orderBy('created_at','desc');
    }
    public function getvideos(){
        return $this->hasMany(PackageVideo::class, 'package_id')->where('status','Published');
    }

    public function getitineraries(){
        return $this->hasMany(PackageItinerary::class,'package_id');
    }

    public function getguides(){
        return $this->hasMany(PackageGuide::class,'package_id');
    }
}
