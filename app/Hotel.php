<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'name', 'description', 'pricing', 'city_id', 'province_id', 'country_id', 'zip_code', 'type_id', 'locality','status'
    ];

    public function country(){
    	return $this->belongsTo(Country::class,'country_id');
    }
    public function province(){
        return $this->belongsTo(Province::class,'province_id');
    }
    public function city(){
        return $this->belongsTo(City::class,'city_id');
    }

    public function type(){
    	return $this->belongsTo(HotelType::class,'type_id');
    }

    public function facilities(){
    	return $this->hasMany(HotelDetailFacility::class,'hotel_id');
    }

    public function facilitydetails(){
        return $this->belongsToMany(HotelFacility::class,'hotel_detail_facilities', 'hotel_id', 'facility_id');
    }

    public static function insertData($request){
        $hotel = static::create($request->except('facility_id'));
        
        if($hotel){
            for ($i=0; $i < count($request->input('facility_id')); $i++) { 
                static::insertFacilities($hotel->id, $request->input('facility_id')[$i]);
            }

            if($request->hasFile('images')){
                Photo::upload($request->file('images'),'hotels',$hotel->id);
            }
        }

        return $hotel;
    }


    public static function insertFacilities($hotel_id, $input){
        HotelDetailFacility::create([
            'hotel_id' => $hotel_id,
            'facility_id' => $input
        ]);
    }

    public static function getName($id = null){
        if($id){
            return static::where('id',$id)->first()->name;
        }
        return '';
    }
    public function getphoto(){
        return $this->hasOne(Photo::class,'ref_id')->where('model_type','hotels')->orderBy('created_at','desc');
    }

    public function getvideos(){
        return $this->hasMany(HotelVideo::class, 'hotel_id')->where('status','Published');;
    }
}
