<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeVideo extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'title', 'description', 'url', 'status'
    ];
}
