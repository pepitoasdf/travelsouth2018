<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleFeature extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'description'
    ];
}
