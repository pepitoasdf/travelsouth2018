<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TabMenu;
class Permission extends Model
{
    public static function defaultPermissions()
    {
        return [
            'view_users',
            'add_users',
            'edit_users',
            'delete_users',

            'view_roles',
            'add_roles',
            'edit_roles',
            'delete_roles',

            'view_vfeatures',
            'add_vfeatures',
            'edit_vfeatures',
            'delete_vfeatures',

            'view_vehicles',
            'add_vehicles',
            'edit_vehicles',
            'delete_vehicles',

            'view_hotels',
            'add_hotels',
            'edit_hotels',
            'delete_hotels',

            'view_facilities',
            'add_facilities',
            'edit_facilities',
            'delete_facilities',

            'view_hoteltypes',
            'add_hoteltypes',
            'edit_hoteltypes',
            'delete_hoteltypes',

            'view_rooms',
            'add_rooms',
            'edit_rooms',
            'delete_rooms',

            'view_roomrates',
            'add_roomrates',
            'edit_roomrates',
            'delete_roomrates',

            'view_roomtypes',
            'add_roomtypes',
            'edit_roomtypes',
            'delete_roomtypes',

            'view_countries',
            'add_countries',
            'edit_countries',
            'delete_countries',

            'view_packages',
            'add_packages',
            'edit_packages',
            'delete_packages',

            'view_vrates',
            'add_vrates',
            'edit_vrates',
            'delete_vrates',

            'view_pfeatures',
            'add_pfeatures',
            'edit_pfeatures',
            'delete_pfeatures',

            'view_prates',
            'add_prates',
            'edit_prates',
            'delete_prates',

            'view_hgalleries',
            'add_hgalleries',

            'view_vgalleries',
            'add_vgalleries',

            'view_pgalleries',
            'add_pgalleries',

            'view_contacts',
            'add_contacts',
            'edit_contacts',
            'delete_contacts',

            'view_abouts',
            'add_abouts',
            'edit_abouts',
            'delete_abouts',

            'view_bookings',
            'add_bookings',
            'edit_bookings',
            'delete_bookings',

            'view_posts',
            'add_posts',
            'edit_posts',
            'delete_posts',

            'view_dashboard',

            'view_background',
            'add_background',
            'edit_background',
            'delete_background',

            'view_increadibleplaces',
            'add_increadibleplaces',
            'edit_increadibleplaces',
            'delete_increadibleplaces',

            'view_partners',
            'add_partners',
            'edit_partners',
            'delete_partners',

            'view_popular',
            'add_popular',
            'edit_popular',
            'delete_popular',

            'view_topdestinations',
            'add_topdestinations',
            'edit_topdestinations',
            'delete_topdestinations',

            'view_videos',
            'add_videos',
            'edit_videos',
            'delete_videos',

            'view_provinces',
            'add_provinces',
            'edit_provinces',
            'delete_provinces',

            'view_cities',
            'add_cities',
            'edit_cities',
            'delete_cities',

            'view_pitineraries',
            'add_pitineraries',
            'edit_pitineraries',
            'delete_pitineraries',

            'view_pguides',
            'add_pguides',
            'edit_pguides',
            'delete_pguides',

            'view_pvideos',
            'add_pvideos',
            'edit_pvideos',
            'delete_pvideos',

            'view_hvideos',
            'add_hvideos',
            'edit_hvideos',
            'delete_hvideos',

            'view_vvideos',
            'add_vvideos',
            'edit_vvideos',
            'delete_vvideos',

            'view_admin',
            'add_admin',
            'edit_admin',
            'delete_admin',

            'view_client',
            'add_client',
            'edit_client',
            'delete_client'
        ];
    }
}
