<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleFeatureDetail extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'vehicle_id', 'feature_id'
    ];
}
