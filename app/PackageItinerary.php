<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageItinerary extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'package_id', 'day', 'title', 'description'
    ];

    public function getphoto(){
        return $this->hasOne(Photo::class,'ref_id')->where('model_type','package-itinerary')->orderBy('created_at','desc');
    }
}
