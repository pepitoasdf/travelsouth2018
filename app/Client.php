<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'title', 'description', 'logo', 'background'
    ];

     public function getphoto(){
        return $this->hasOne(Photo::class,'ref_id')->where('model_type','system-client')->orderBy('created_at','desc');
    }
}
