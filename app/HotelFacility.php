<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelFacility extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'description'
    ];
}
