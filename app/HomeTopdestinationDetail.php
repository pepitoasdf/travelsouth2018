<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeTopdestinationDetail extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'name', 'country_id','city_id', 'province_id', 'header_id'
    ];

    public function city(){
    	return $this->belongsTo(City::class,'city_id');
    }

    public function country(){
    	return $this->belongsTo(Country::class,'country_id');
    }

    public function province(){
    	return $this->belongsTo(Province::class,'province_id');
    }
    public function getphoto(){
        return $this->hasOne(Photo::class,'ref_id')->where('model_type','topdestinations')->orderBy('created_at','desc');
    }
}
