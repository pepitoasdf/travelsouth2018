<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Post extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'title', 'body', 'author_id', 'slug', 'status'
    ];

    public static function findBySlug($slug){
    	return static::where('slug',$slug)->firstOrFail();
    }

    public static function setSlug($text){
    	$text = str_slug($text,'-');
    	
    	$count = static::where('slug','like','%'.$text.'%')->count();
    	return ($count > 0) ? $text . '-' . ($count + 1) : $text;
    }

    public function author(){
        return $this->belongsTo(User::class,'author_id');
    }
}
