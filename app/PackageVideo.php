<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageVideo extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'package_id', 'video_url', 'title', 'description','status'
    ];
}
