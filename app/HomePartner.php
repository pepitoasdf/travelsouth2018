<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePartner extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'name', 'url', 'status'
    ];
    public function getphoto(){
        return $this->hasOne(Photo::class,'ref_id')->where('model_type','partners')->orderBy('created_at','desc');
    }
}
