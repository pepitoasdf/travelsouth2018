<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;
use File;
class Photo extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'ref_id', 'model_type', 'file_name', 'path'
    ];

    public static function upload($images, $path, $ref_id){
    	if(is_array($images)){
    		for ($i=0; $i < count($images); $i++) { 
    			static::manageUpload($images[$i], $path, $ref_id);
    		}
    	}
    	else{
    		static::manageUpload($images, $path, $ref_id);
    	}
    }

    public static function manageUpload($image, $path, $ref_id){
        $filename = uniqid().'_'.time() . '.' . $image->getClientOriginalExtension();

        $destinationPath = 'admin/images/uploads/'.$path;
    	$destinationResize100 = $destinationPath.'/thumb_100';
    	$destinationResize570x400 = $destinationPath.'/thumb_570x400';
    	$destinationResizeSquare1920X1080 = $destinationPath.'/1920X1080';

    	if(!File::exists($destinationPath)) {
		    File::makeDirectory($destinationPath);
		}
		if(!File::exists($destinationResize100)) {
		    File::makeDirectory($destinationResize100);
		}
		if(!File::exists($destinationResize570x400)) {
		    File::makeDirectory($destinationResize570x400);
		}
		if(!File::exists($destinationResizeSquare1920X1080)) {
		    File::makeDirectory($destinationResizeSquare1920X1080);
		}

        $resize = Image::make($image);
        $resize->resize(null, 100, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationResize100.'/'.$filename);

        $resize = Image::make($image);
        $resize->resize(570, 400)->save($destinationResize570x400.'/'.$filename);

        $resize = Image::make($image);
        $resize->resize(1920, 1080)->save($destinationResizeSquare1920X1080.'/'.$filename);

        $image->move($destinationPath, $filename);

        static::savePhoto($ref_id, $path, $filename, $destinationPath);
    }
    public static function savePhoto($ref_id = null, $path = null, $filename, $destinationPath){
    	Photo::create([
            'ref_id' => $ref_id,
            'model_type' => $path,
            'file_name' => $filename,
            'path' => $destinationPath
        ]);
    }

    public static function removeFile($images){
        if(count($images)){
            if(is_array($images)){
                for ($i=0; $i < count($images); $i++) {
                    $photo = static::find($images[$i]);

                    File::delete($photo->path.'/'.$photo->file_name);
                    File::delete($photo->path.'/thumb_100/'.$photo->file_name);
                    File::delete($photo->path.'/thumb_570x400/'.$photo->file_name);
                    File::delete($photo->path.'/1920X1080/'.$photo->file_name);
                    
                    $photo->delete();
                }
            }
            else{
                $photo = static::find($images);

                File::delete($photo->path.'/'.$photo->file_name);
                File::delete($photo->path.'/thumb_100/'.$photo->file_name);
                File::delete($photo->path.'/thumb_570x400/'.$photo->file_name);
                File::delete($photo->path.'/1920X1080/'.$photo->file_name);
                
                $photo->delete();
            }
        }
    }

    public static function removeByReference($id, $model){
        $photos = static::where('ref_id',$id)->where('model_type', $model)->get();
        if($photos){
            foreach ($photos as $photo) {
                $deletePhoto = static::find($photo->id);

                File::delete($deletePhoto->path.'/'.$deletePhoto->file_name);
                File::delete($deletePhoto->path.'/thumb_100/'.$deletePhoto->file_name);
                File::delete($deletePhoto->path.'/thumb_570x400/'.$deletePhoto->file_name);
                File::delete($deletePhoto->path.'/1920X1080/'.$deletePhoto->file_name);
                
                $deletePhoto->delete();
            }
        }
    }
}
