<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleRate extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'rate_day', 'rate_hour', 'effective_date','vehicle_id'
    ];
}
